#include <glm/mat4x4.hpp>

void initOpenAL();
void shutdownOpenAL();
void setListenerTransform(glm::mat4 transform);
