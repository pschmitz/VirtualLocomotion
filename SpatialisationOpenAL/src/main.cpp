#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cmath>
#include <string>
#include <iostream>
#include <sstream>

#include <glm/gtx/io.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>

#define BUFLEN 512
#define NPACK 10
#define PORT 1338

#include <openal.hpp>


struct sockaddr_in sockAddr;
int readSocket;

void bindUDPServerSocket() {
    readSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(readSocket == -1)
        std::cerr << "error creating socket" << std::endl;

    memset((char *) &sockAddr, 0, sizeof(sockAddr));
    sockAddr.sin_family = AF_INET;
    sockAddr.sin_port = htons(PORT);
    sockAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(readSocket, (struct sockaddr *) &sockAddr, sizeof(sockAddr))==-1)
        std::cerr << "error binding socket" << std::endl;
}

glm::mat4 retreiveListenerTransformUDP() {
    glm::mat4 transform;

    if(read(readSocket, glm::value_ptr(transform), 64) != 64)
        std::cerr << "failed to read 64 bytes from socket" << std::endl;

    std::cout << transform << std::endl;

    return transform;
}

int main(int argc, char **argv)
{
    initOpenAL();
    bindUDPServerSocket();

    glm::mat4 listenerTransform = glm::mat4(1);
    do {
        listenerTransform = retreiveListenerTransformUDP();
        setListenerTransform(listenerTransform);
    } while (true);

    close(readSocket);
    shutdownOpenAL();

    return 0;
}
