#include <cstring>
#include <string>
#include <iostream>
#include <vector>

#include <AL/al.h>
#include <AL/alext.h>
#include <AL/alc.h>
#include <AL/alut.h>

#include <glm/glm.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

//#define FILENAME "resources/background-music-cut-mono.wav"
#define FILENAME "resources/background-music-cut.wav"
//#define FILENAME "resources/typewriter-mono.wav"
// #define FILENAME "typewriter.wav"

ALCdevice * device;
ALCcontext * context;

ALuint buffer, source;
ALint state;

void checkError(std::string context)
{
    ALenum error = alGetError();

    switch(error) {
    case AL_INVALID_NAME:
        std::cerr << "OpenAL error: " << context
                  << ": AL_INVALID_NAME" << std::endl;
        break;
    case AL_INVALID_ENUM:
        std::cerr << "OpenAL error: " << context
                  << ": AL_INVALID_ENUM" << std::endl;
        break;
    case AL_INVALID_VALUE:
        std::cerr << "OpenAL error: " << context
                  << ": AL_INVALID_VALUE" << std::endl;
        break;
    case AL_INVALID_OPERATION:
        std::cerr << "OpenAL error: " << context
                  << ": AL_INVALID_OPERATION" << std::endl;
        break;
    case AL_OUT_OF_MEMORY:
        std::cerr << "OpenAL error: " << context
                  << ": AL_OUT_OF_MEMORY" << std::endl;
        break;
    case AL_NO_ERROR:
        break;
    default:
        std::cerr << "OpenAL error: " << context
                  << ": unknown error code "
                  << error << std::endl;
        break;
    }
}

// devices contains the device names, separated by NULL
// and terminated by two consecutive NULLs.
std::vector<std::string> getDeviceNames(const ALCchar *devices) {
    std::vector<std::string> deviceNames;

    ALCchar buffer[128];

    bool hasReadZero = false;
    bool abort = false;

    const ALCchar * begin = devices;
    const ALCchar * p = devices;

    while(!abort) {
        if(*p == 0) {
            if(hasReadZero) {
                break;
            }
            hasReadZero = true;

            memcpy(buffer, begin, p - begin + 1);
            deviceNames.push_back(std::string(buffer));

            begin = p + 1;
        }
        else {
            hasReadZero = false;
        }
        ++p;
    }

    return deviceNames;
}

void initOpenAL() {
    const ALCchar *devices;
    const ALCchar *defaultDeviceName;

    // Pass in NULL device handle to get list of devices
    devices = alcGetString(NULL, ALC_ALL_DEVICES_SPECIFIER);
    std::vector<std::string> deviceNames = getDeviceNames(devices);

    std::cout << "Available devices: " << std::endl;
    for(auto name : deviceNames) {
        std::cout << name << std::endl;
    }

    defaultDeviceName = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);
    std::cout << "Default device: " << defaultDeviceName << std::endl;

//    device = alcOpenDevice(defaultDeviceName);
    device = alcOpenDevice("ALSA Default");
    if(!device) {
        std::cerr << "Error opening default device!" << std::endl;
    }

    ALCint attrs[] = {
        ALC_HRTF_SOFT, ALC_TRUE, /* request HRTF */
        0 /* end of list */
    };

    context = alcCreateContext(device, attrs);
    alcMakeContextCurrent(context);

    ALCint hrtf;
    alcGetIntegerv(device, ALC_HRTF_SOFT, 1, &hrtf);
    if(hrtf == ALC_TRUE) {
        std::cout << "HRTF: enabled" << std::endl;
    }
    else {
        std::cout << "HRTF: disabled" << std::endl;
    }

    // Initialize the environment
    alutInitWithoutContext(0, NULL);
    checkError("alutInitWithoutContext");

    // Load pcm data into buffer
    buffer = alutCreateBufferFromFile(FILENAME);
    alGetError();

    ALint info;
    alGetBufferi(buffer, AL_FREQUENCY, &info);
    std::cout << "frequency: " << info << std::endl;

    alGetBufferi(buffer, AL_CHANNELS, &info);
    std::cout << "channels: " << info << std::endl;

    // Create sound source (use buffer to fill source)
    alGenSources(1, &source);
    alSourcei(source, AL_BUFFER, buffer);
    alSourcei(source, AL_LOOPING, 1);

    // Set listener position
    ALfloat listenerPos[] = {0.0, 1.8, 0.0};
    alListenerfv(AL_POSITION, listenerPos);

    // Orientation as AT and UP vectors
    // ALfloat listenerOri[]={0.0,0.0,-1.0, 0.0,1.0,0.0};
    // alListenerfv(AL_ORIENTATION,listenerOri);

//    ALfloat sourcePos[] = {-1.0, 1.8, 0.0};
    ALfloat sourcePos[] = {7.27685, 0.88, -1.85};
    alSourcefv(source, AL_POSITION, sourcePos);
    alSourcef(source, AL_GAIN, 0.5f);

    // Play
    alSourcePlay(source);
}

void shutdownOpenAL() {
    // Clean up sources and buffers
    alDeleteSources(1, &source);
    alDeleteBuffers(1, &buffer);

    // Exit everything
    alutExit();

}

void setListenerTransform(glm::mat4 transform) {
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;

    glm::decompose(transform,
                   scale, rotation, translation, skew, perspective);

    rotation = glm::conjugate(rotation);

    glm::vec3 at = glm::vec3(0, 0, -1);
    glm::vec3 up = glm::vec3(0, 1, 0);

    at = glm::rotate(rotation, at);
    up = glm::rotate(rotation, up);

    std::cout << "at: " << at << std::endl;
    std::cout << "up: " << up << std::endl;

    float orientationVectors[] = {
        at[0], at[1], at[2],
        up[0], up[1], up[2]
    };

    alListenerfv(AL_POSITION, glm::value_ptr(translation));
    alListenerfv(AL_ORIENTATION, orientationVectors);
}
