package graphicsrwth.gearvrapp;

public interface INetworkPackageHandler
{
    void handlePackage(byte[] data);
}
