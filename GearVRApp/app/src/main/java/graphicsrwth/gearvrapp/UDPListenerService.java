package graphicsrwth.gearvrapp;


import java.net.DatagramPacket;
import java.net.DatagramSocket;

//import org.apache.http.util.ExceptionUtils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


/*
 * Linux command to send UDP:
 * #socat - UDP-DATAGRAM:192.168.1.255:11111,broadcast,sp=11111
 */
public class UDPListenerService extends Service {
    static String UDP_BROADCAST = "UDPBroadcast";

    //Boolean shouldListenForUDPBroadcast = false;
    DatagramSocket socket;

    INetworkPackageHandler packageHandler;

    public UDPListenerService(INetworkPackageHandler packageHandler)
    {
        this.packageHandler = packageHandler;
    }

    private void listenAndWaitAndThrowIntent(/*InetAddress broadcastIP, */Integer port) throws Exception {
        byte[] recvBuf = new byte[15000];
        if (socket == null || socket.isClosed()) {
            //socket = new DatagramSocket(port, broadcastIP);
            //socket.setBroadcast(true);
            socket = new DatagramSocket(port);
        }
        //socket.setSoTimeout(1000);
        DatagramPacket packet = new DatagramPacket(recvBuf, recvBuf.length);
        Log.d("UDP", "Waiting for UDP broadcast");
        socket.receive(packet);

       /* String senderIP = packet.getAddress().getHostAddress();
        String message = new String(packet.getData()).trim();

        Log.d("UDP", "Got UDB broadcast from " + senderIP + ", message: " + message);*/

        packageHandler.handlePackage(packet.getData());

        //broadcastIntent(senderIP, message);
        socket.close();
    }

    private void broadcastIntent(String senderIP, String message)
    {
        Intent intent = new Intent(UDPListenerService.UDP_BROADCAST);
        intent.putExtra("sender", senderIP);
        intent.putExtra("message", message);
        sendBroadcast(intent);
    }

    Thread UDPBroadcastThread;

    public long testHandle;

    void startListenForUDPBroadcast()
    {
        UDPBroadcastThread = new Thread(new Runnable() {
            public void run() {
                try
                {
                    //Log.d("Udp:", "Try to start Udp Listening");
                    //InetAddress broadcastIP = InetAddress.getByName("137.226.115.228"); //172.16.238.42 //192.168.1.255
                    Integer port = 1235;
                    while (shouldRestartSocketListen)
                    {
                        //GL2JNILib.testFunction(testHandle, 0);

                        int tries = 0;
                        while(true)
                        {
                            try
                            {
                                listenAndWaitAndThrowIntent(/*broadcastIP, */port);
                                //GL2JNIActivity.setDebugText("Working");
                                break;
                            }
                            catch(Exception e)
                            {
                                Log.d("UDP", "listenAndWaitAndThrowIntent failed.");
                                Thread.sleep(100);

                                if(tries == 4)
                                {
                                    throw e;
                                }
                            }
                        }
                    }
                    //if (!shouldListenForUDPBroadcast) throw new ThreadDeath();
                }
                catch (Exception e)
                {
                    Log.i("UDP", "no longer listening for UDP broadcasts cause of error " + e.getMessage());
                    //GL2JNIActivity.setDebugText(e.getMessage());
                }
            }
        });
        UDPBroadcastThread.start();
    }

    private Boolean shouldRestartSocketListen=true;

    void stopListen() {
        shouldRestartSocketListen = false;
        socket.close();
    }

    @Override
    public void onCreate() {

    };

    @Override
    public void onDestroy() {
        stopListen();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        shouldRestartSocketListen = true;
        startListenForUDPBroadcast();
        Log.i("UDP", "Service started");
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
