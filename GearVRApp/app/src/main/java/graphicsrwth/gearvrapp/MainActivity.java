package graphicsrwth.gearvrapp;

import android.Manifest;
import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity implements SurfaceHolder.Callback, INetworkPackageHandler
{
    static
    {
        //Load native JNI lib
        System.loadLibrary("native-lib");
    }

    private static final String TAG = "GearVrAppMainActivity";

    private TextureLoader textureLoader;
    private FileReader fileReader;
    private NativeToJava nativeToJava;

    private SurfaceView mView;
    private SurfaceHolder mSurfaceHolder;
    private boolean initialized = false;

    private Thread thread;
    private RenderThreadRunnable renderThreadRunnable;

    private boolean activityIsResumed = false;
    private boolean surfaceIsReady = false;

    //We are only allowed to enter vr mode if the activity is resumed and the surface is valid (surfaceChanged)
    //The order of surface and activity states is not guaranteed.
    //For more details take a look into "VrApi.h"
    private void checkAndAllowVr()
    {
        if(activityIsResumed && surfaceIsReady)
        {
            NativeInterface.ApplicationAllowVrMode(true);
        }
    }

    private void checkAndForbidVr()
    {
        NativeInterface.ApplicationAllowVrMode(false);
    }

    @Override
    public void onCreate(Bundle bundle)
    {
        // Request permission to read SDCard
        ActivityCompat.requestPermissions(
                this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1
        );

        //Fullscreen configuration
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Test if native interface is working
        Log.d("NATIVETEST", NativeInterface.NativeTest());

        super.onCreate( bundle );

        mView = new SurfaceView( this );
        setContentView( mView );
        mView.getHolder().addCallback( this );

        // Force the screen to stay on, rather than letting it dim and shut off
        // while the user is watching a movie.
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );

        // Force screen brightness to stay at maximum
        setScreenBrightness(1.0f);

        initializeNativeToJava();
        NativeInterface.initializeApplication(this, nativeToJava);
        initializeAndStartRenderThread();
        initialized = true;
    }

    private void setScreenBrightness(float value)
    {
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.screenBrightness = value;
        getWindow().setAttributes( params );
    }

    private void initializeAndStartRenderThread()
    {
        //NativeInterface.onCreate(this);
        renderThreadRunnable = new RenderThreadRunnable(this, nativeToJava);
        thread = new Thread(renderThreadRunnable);
        thread.start();
    }

    private void initializeNativeToJava()
    {
        fileReader = new FileReader(this.getAssets());
        textureLoader = new TextureLoader(this);
        nativeToJava = new NativeToJava(textureLoader, fileReader);
    }

    @Override protected void onStart()
    {
        Log.v( TAG, "MainActivity::onStart()" );
        super.onStart();
        //NativeInterface.onStart();
    }

    @Override protected void onResume()
    {
        hideSystemUI();
        Log.v( TAG, "MainActivity::onResume()" );
        super.onResume();
        activityIsResumed = true;
        checkAndAllowVr();
        //NativeInterface.onResume();
    }

    @Override protected void onPause()
    {
        activityIsResumed = false;
        checkAndForbidVr();

        showSystemUI();
        Log.v( TAG, "MainActivity::onPause()" );
        //NativeInterface.onPause();
        super.onPause();
    }

    @Override protected void onStop()
    {
        Log.v( TAG, "MainActivity::onStop()" );
        //NativeInterface.onStop();
        super.onStop();
    }

    @Override protected void onDestroy()
    {
        Log.v( TAG, "MainActivity::onDestroy()" );
        if ( mSurfaceHolder != null )
        {

        }

        super.onDestroy();
        initialized = false;
    }

    @Override public void surfaceCreated( SurfaceHolder holder )
    {
        Log.v( TAG, "MainActivity::surfaceCreated()" );
        if (initialized)
        {
            mSurfaceHolder = holder;
            NativeInterface.loadApplication(mSurfaceHolder.getSurface());
        }
    }

    @Override public void surfaceChanged( SurfaceHolder holder, int format, int width, int height )
    {
        Log.v( TAG, "MainActivity::surfaceChanged()" );
        if (initialized)
        {
            //NativeInterface.onSurfaceChanged(holder.getSurface());
            mSurfaceHolder = holder;
            surfaceIsReady = true;
            checkAndAllowVr();
        }
    }

    @Override public void surfaceDestroyed( SurfaceHolder holder )
    {
        surfaceIsReady = false;
        checkAndForbidVr();

        Log.v( TAG, "MainActivity::surfaceDestroyed()" );
        if (initialized)
        {
            NativeInterface.unloadApplication();
            mSurfaceHolder = null;
        }
    }

    @Override public boolean dispatchKeyEvent( KeyEvent event )
    {
        if (initialized)
        {
            int keyCode = event.getKeyCode();
            int action = event.getAction();
            if ( action != KeyEvent.ACTION_DOWN && action != KeyEvent.ACTION_UP )
            {
                return super.dispatchKeyEvent( event );
            }
            if ( keyCode == KeyEvent.KEYCODE_VOLUME_UP )
            {
                //adjustVolume( 1 );
                return true;
            }
            if ( keyCode == KeyEvent.KEYCODE_VOLUME_DOWN )
            {
                //adjustVolume( -1 );
                return true;
            }
            if ( action == KeyEvent.ACTION_UP )
            {
                Log.v( TAG, "MainActivity::dispatchKeyEvent( " + keyCode + ", " + action + " )" );
            }
            //NativeInterface.onKeyEvent(keyCode, action);
            return true;
        }
        return false;
    }

    @Override public boolean dispatchTouchEvent( MotionEvent event )
    {
        if (initialized)
        {
            int action = event.getAction();
            float x = event.getRawX();
            float y = event.getRawY();
            if ( action == MotionEvent.ACTION_UP )
            {
                Log.v( TAG, "MainActivity::dispatchTouchEvent( " + action + ", " + x + ", " + y + " )" );
            }
            NativeInterface.touchEvent(action, x, y );
        }
        return true;
    }

    // This snippet hides the system bars.
    private void hideSystemUI()
    {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private void showSystemUI()
    {
        mView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    public void handlePackage(byte[] data)
    {
        Log.d("UDP", "Received package.");
        NativeInterface.udpPackage(data, data.length);
    }
}
