package graphicsrwth.gearvrapp;

import android.app.Activity;
import android.view.Surface;

public class NativeInterface
{
    public static native String NativeTest();

    //Is called only once per app start and initialize all fields that do not change until the app is destroyed
    public static native void initializeApplication(Activity activity, NativeToJava nativeToJava);

    //Is called when we have a valid surface which allows us to create a egl context and OpenGL resources.
    public static native void loadApplication(Surface s);

    public static native void ApplicationAllowVrMode(boolean value);

    //Is called once per frame. Frametime is defined in seconds.
    public static native void renderThreadFrame(double frameTime);

    //Is called when the current surface is destroyed and we have to destroy all graphics resources.
    public static native void unloadApplication();

    //Is called if the app is going to close and all resources need to be destroyed.
    public static native void destroyApplication();

    //
    public static native void udpPackage(byte[] data, int length);

    //
    public static native void touchEvent(int action, float x, float y);


    /*
    // Render thread
    public static native void renderThreadStart(Activity activity, NativeToJava nativeToJava);
    public static native void renderThreadStop();

    // Activity lifecycle
    public static native void onCreate( Activity obj );
    public static native void onStart();
    public static native void onResume();
    public static native void onPause();
    public static native void onStop();
    public static native void onDestroy();

    // Surface lifecycle
    public static native void onSurfaceCreated(Surface s);
    public static native void onSurfaceChanged(Surface s);
    public static native void onSurfaceDestroyed();

    // Input
    public static native void onKeyEvent(int keyCode, int action );
    public static native void onTouchEvent(int action, float x, float y );
    */
}
