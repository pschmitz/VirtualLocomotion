package graphicsrwth.gearvrapp;

import android.content.res.AssetManager;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ByteArrayOutputStream;

public class FileReader
{
    private AssetManager assetManager;
    public FileReader(AssetManager assetManager) { this.assetManager = assetManager; }

    public String LoadTextFromAssets(String filename) throws IOException
    {
        StringBuilder stringBuffer = new StringBuilder();
        InputStream stream = assetManager.open(filename);
        BufferedReader in = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
        String result;

        while ((result = in.readLine()) != null)
        {
            stringBuffer.append(result);
            stringBuffer.append("\n");
        }

        in.close();
        return stringBuffer.toString();
    }


    public byte[] LoadRawBufferFromAssets(String filename) throws IOException
    {
        InputStream stream = assetManager.open(filename);

        try (ByteArrayOutputStream os = new ByteArrayOutputStream();)
        {
            byte[] buffer = new byte[0xFFFF];

            for (int len; (len = stream.read(buffer)) != -1;)
                os.write(buffer, 0, len);

            os.flush();

            stream.close();
            return os.toByteArray();
        } catch (IOException e) {
            return new byte[0];
        }
    }
}
