package graphicsrwth.gearvrapp;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Created by Benni on 05.12.2016.
 */

public class TextureLoader
{
    Context context;

    public TextureLoader(Context context)
    {
        this.context = context;
    }

    public TextureData LoadTextureDataFromAssets(String filename) throws IOException
    {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;   // No pre-scaling

        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;

        istr = assetManager.open(filename);
        return LoadTextureData(istr);
    }

    public TextureData LoadTextureFromFile(String filePath) throws IOException
    {
        FileInputStream fileInputStream = new FileInputStream(new File(filePath));
        return LoadTextureData(fileInputStream);
    }

    public TextureData LoadTextureData(InputStream stream)
    {
        Bitmap bitmap = BitmapFactory.decodeStream(stream);

        // Read in the resource
        //final Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fd);
        //final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

        TextureData textureData = new TextureData();
        int bytesize = bitmap.getRowBytes() * bitmap.getHeight(); // or bitmap.ByteCount if target is  api 12 or later
        ByteBuffer buffer = ByteBuffer.allocate(bytesize);
        bitmap.copyPixelsToBuffer(buffer);
        buffer.rewind();
        textureData.Bytes = new byte[bytesize];
        buffer.get(textureData.Bytes);

        textureData.Width = bitmap.getWidth();
        textureData.Height = bitmap.getHeight();

        // Recycle the bitmap, since its data has been loaded into OpenGL.
        bitmap.recycle();
        return textureData;
    }
}
