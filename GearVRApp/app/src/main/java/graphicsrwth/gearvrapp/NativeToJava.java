package graphicsrwth.gearvrapp;

import android.os.Environment;

import java.io.IOException;

/**
 * Created by Benni on 05.12.2016.
 */

public class NativeToJava
{
    FileReader fileReader;
    TextureLoader textureLoader;

    public NativeToJava(TextureLoader textureLoader, FileReader fileReader)
    {
        this.textureLoader = textureLoader;
        this.fileReader = fileReader;
    }

    public TextureData LoadTextureDataFromAssets(String filename) throws IOException
    {
        return textureLoader.LoadTextureDataFromAssets(filename);
    }

    public TextureData LoadTextureDataFromFile(String filename) throws IOException
    {
        return textureLoader.LoadTextureFromFile(filename);
    }

    public String LoadTextFromAssets(String filename) throws IOException
    {
        return fileReader.LoadTextFromAssets(filename);
    }

    public byte[] LoadRawBufferFromAssets(String filename) throws IOException
    {
        return fileReader.LoadRawBufferFromAssets(filename);
    }

    public String GetExternalStorageFolder() {
        return Environment.getExternalStorageDirectory().toString();
    }
}
