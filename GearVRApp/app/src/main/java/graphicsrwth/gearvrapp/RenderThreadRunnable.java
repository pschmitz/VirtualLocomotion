package graphicsrwth.gearvrapp;

import android.app.Activity;
import android.util.Log;

/**
 * Created by Benjamin Roth on 08.12.2016.
 */

public class RenderThreadRunnable  implements Runnable
{
    private boolean running = false;

    private Activity activity;
    private NativeToJava nativeToJava;

    RenderThreadRunnable(Activity activity, NativeToJava nativeToJava)
    {
        this.activity = activity;
        this.nativeToJava = nativeToJava;
    }

    public void run()
    {
        running = true;
        long lastTime = System.nanoTime();

        while(running)
        {
            long current = System.nanoTime();
            double tempFrameTime = (current - lastTime) / (1000.0 * 1000.0);
            lastTime = current;
            NativeInterface.renderThreadFrame(tempFrameTime);
            if(tempFrameTime > 16.6 + 0.1)
            {
                //Log.d("FRAMETIME", tempFrameTime + "");
            }
        }
        running = false;
    }

    public void stop()
    {
        running = false;
    }
}
