#include "extensions.hh"
#include <EGL/egl.h>
#include <glow/common/log.hh>

#define LOAD_PTR_AND_DEBUG(basename, vendor) \
do { \
    gl ## basename = (basename ## Ptr) eglGetProcAddress("gl" #basename #vendor); \
    if (gl ## basename) { \
        glow::info() << "Successfully loaded extension function gl" #basename "."; \
    } else { \
        glow::warning() << "Could not load extension function gl" #basename "."; \
    } \
} while(0);


namespace glow {
    namespace ext {
        DebugMessageCallbackPtr glDebugMessageCallback = 0;
        PopDebugGroupPtr glPopDebugGroup = 0;
        PushDebugGroupPtr glPushDebugGroup = 0;

        RenderbufferStorageMultisamplePtr glRenderbufferStorageMultisample = 0;
        FramebufferTexture2DMultisamplePtr glFramebufferTexture2DMultisample = 0;

        void loadAll() {
            glow::info() << "GLES-Extensions on this device " << glGetString(GL_EXTENSIONS);


            LOAD_PTR_AND_DEBUG(DebugMessageCallback, KHR);
            LOAD_PTR_AND_DEBUG(PushDebugGroup, KHR);
            LOAD_PTR_AND_DEBUG(PopDebugGroup, KHR);

            LOAD_PTR_AND_DEBUG(RenderbufferStorageMultisample, EXT);
            LOAD_PTR_AND_DEBUG(FramebufferTexture2DMultisample, EXT);
        }
    }
}