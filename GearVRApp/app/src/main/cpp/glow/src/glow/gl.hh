#pragma once

#include <GLES3/gl31.h>

namespace glow
{
/**
 * @brief restores various openGL states to their default setting
 *
 * This is not necessarily comprehensive, so better check the implementation to be sure
 *
 * Includes at least:
 *   * all glEnable/Disable states
 *   * their derived calls (glBlendFunc, ...)
 */
void restoreDefaultOpenGLState();
}
