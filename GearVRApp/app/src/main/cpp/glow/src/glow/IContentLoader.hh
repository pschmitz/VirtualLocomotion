//
// Created by Benni on 13.12.2016.
//

#ifndef GEARVRAPP_ICONTENTLOADER_H
#define GEARVRAPP_ICONTENTLOADER_H

#include <vector>
#include <string>
#include <ios>
#include <fstream>

#include "../../../Framework/TextureData.h"
#include "../../../Framework/Log.h"

using std::string;

/*interface*/ class IContentLoader
{
public:
    virtual ~IContentLoader() { }

    virtual string LoadTextFromAssets(string filename) = 0;
    virtual TextureData LoadTextureFromAssets(string filename) = 0;

    virtual string GetExternalStorageFolder() = 0;
    virtual TextureData LoadTextureFromFile(string filename) = 0;

    std::vector<unsigned char> LoadRawBufferFromFile(string filename)
    {
        filename = GetExternalStorageFolder() + filename;
        Log("LoadRawBufferFromFile(\"" + filename + "\")");
        // open the file:
        std::streampos fileSize;
        std::ifstream file(filename, std::ios::binary);

        // get its size:
        file.seekg(0, std::ios::end);
        fileSize = file.tellg();
        file.seekg(0, std::ios::beg);

        // read the data:
        int _fileSize = (int)fileSize;
        std::vector<unsigned char> fileData((int)fileSize);
        file.read((char*) &fileData[0], fileSize);
        return fileData;
    }

    virtual std::vector<unsigned char>
        LoadRawBufferFromAssets(string filename) = 0;

};

#endif //GEARVRAPP_ICONTENTLOADER_H
