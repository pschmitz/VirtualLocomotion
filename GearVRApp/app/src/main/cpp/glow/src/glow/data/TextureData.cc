#include "TextureData.hh"

#include "SurfaceData.hh"

#include <glow/common/profiling.hh>
#include <glow/common/log.hh>
#include <glow/common/str_utils.hh>
#include <glow/IContentLoader.hh>
#include <glow/glow.hh>

using namespace glow;

glow::TextureData::LoadFromFileFunc glow::TextureData::sLoadFunc = nullptr;

glow::TextureData::TextureData()
{
}

void glow::TextureData::addSurface(const SharedSurfaceData &surface)
{
    mSurfaces.push_back(surface);
}

SharedTextureData glow::TextureData::createFromFile(const std::string &filename, ColorSpace colorSpace)
{
    GLOW_ACTION();

    auto ending = util::toLower(util::fileEndingOf(filename));
    return loadFromAPK(filename, ending, colorSpace);
}

SharedTextureData glow::TextureData::loadFromAPK(std::string const &filename, std::string const &ending,
                                           ColorSpace colorSpace) {

    GLOW_ACTION();

    auto data = glow::getLoader()->LoadTextureFromAssets(filename);

    auto tex = std::make_shared<glow::TextureData>();
    auto surface = std::make_shared<SurfaceData>();

    surface->setData(std::vector<char>(data.Data, data.Data + data.DataLength));

    surface->setType(GL_UNSIGNED_BYTE);
    surface->setMipmapLevel(0);
    surface->setWidth(data.Width);
    surface->setHeight(data.Height);
    surface->setFormat(GL_RGBA);

    switch (colorSpace)
    {
        case ColorSpace::Linear:
            tex->setPreferredInternalFormat(GL_RGBA);
            break;
        case ColorSpace::sRGB:
        case ColorSpace::AutoDetect:
            //tex->setPreferredInternalFormat(GL_SRGB_ALPHA);
            break;
    }
    tex->setWidth(data.Width);
    tex->setHeight(data.Height);
    tex->addSurface(surface);

    return tex;
}

SharedTextureData glow::TextureData::createFromFileCube(const std::string &fpx,
                                                  const std::string &fnx,
                                                  const std::string &fpy,
                                                  const std::string &fny,
                                                  const std::string &fpz,
                                                  const std::string &fnz,
                                                  ColorSpace colorSpace)
{
    GLOW_ACTION();

    auto tpx = createFromFile(fpx, colorSpace);
    auto tnx = createFromFile(fnx, colorSpace);
    auto tpy = createFromFile(fpy, colorSpace);
    auto tny = createFromFile(fny, colorSpace);
    auto tpz = createFromFile(fpz, colorSpace);
    auto tnz = createFromFile(fnz, colorSpace);

    SharedTextureData ts[] = {tpx, tnx, tpy, tny, tpz, tnz};
    for (auto i = 0; i < 6; ++i)
        if (!ts[i])
            return nullptr;

    for (auto i = 1; i < 6; ++i)
        if (ts[0]->getWidth() != ts[i]->getWidth() || ts[0]->getHeight() != ts[i]->getHeight())
        {
            std::string files[] = {fpx, fnx, fpy, fny, fpz, fnz};
            error() << "CubeMaps require same size for every texture: ";
            error() << "  " << ts[0]->getWidth() << "x" << ts[0]->getHeight() << ", " << files[0];
            error() << "  " << ts[i]->getWidth() << "x" << ts[i]->getHeight() << ", " << files[i];
            return nullptr;
        }

    auto tex = tpx;
    tex->setTarget(GL_TEXTURE_CUBE_MAP);
    for (auto const &s : tex->getSurfaces())
        s->setTarget(GL_TEXTURE_CUBE_MAP_POSITIVE_X);
    for (auto i = 1; i < 6; ++i)
        for (auto const &s : ts[i]->getSurfaces())
        {
            s->setTarget(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i);
            tex->addSurface(s);
        }
    return tex;
}

void glow::TextureData::setLoadFunction(const glow::TextureData::LoadFromFileFunc &func)
{
    sLoadFunc = func;
}
