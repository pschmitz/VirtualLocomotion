#include "Shader.hh"

#include <fstream>

#include "glow/glow.hh"
#include "glow/common/file_utils.hh"
#include "glow/common/log.hh"
#include "glow/common/stream_readall.hh"
#include "glow/common/shader_endings.hh"
#include "glow/common/str_utils.hh"
#include "glow/common/profiling.hh"
#include "glow/IContentLoader.hh"

using namespace glow;

Shader::Shader(GLenum shaderType) : mType(shaderType)
{
    mObjectName = glCreateShader(mType);
}

Shader::~Shader()
{
    glDeleteShader(mObjectName);
}

void Shader::compile()
{
    GLOW_ACTION();
    glCompileShader(mObjectName);

    // check error log
    GLint logLength = 0;
    glGetShaderiv(mObjectName, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 1)
    {
        std::vector<GLchar> log;
        log.resize(logLength + 1);
        glGetShaderInfoLog(mObjectName, logLength + 1, nullptr, log.data());

        error() << "Log for " << (mFileName.empty() ? "[Non-file shader]" : mFileName);
        error() << "Shader compiler: " << log.data();
        mHasErrors = true;
        mCompiled = false; // is this true?
    }
    else
    {
        mHasErrors = false;
        mCompiled = true;
    }
}

void Shader::reload()
{
    // Does not make much sense since shader files come from the apk
    /*
    GLOW_ACTION();

    // reload source
    if (!mFileName.empty())
    {
        auto loader = glow::getLoader();
        auto source = loader->LoadTextFromAssets(mFileName);

        std::ifstream file(mFileName);
        if (!file.good())
        {
            warning() << "Skipping reload for " << mFileName << ", file not readable";
            return;
        }
        setSource(util::readall(file));
        mLastModification = util::fileModificationTime(mFileName);
    }

    // compile
    compile();
    */
}

void Shader::setSource(const std::string &source)
{
    setSource(std::vector<std::string>({source}));
}

void Shader::setSource(const std::vector<std::string> &sources)
{
    mFileDependencies.clear();
    mSources = sources;

    auto parsedSources = sources;

    std::vector<const GLchar *> srcs;
    srcs.resize(parsedSources.size());
    for (auto i = 0u; i < parsedSources.size(); ++i)
        srcs[i] = parsedSources[i].c_str();
    glShaderSource(mObjectName, srcs.size(), srcs.data(), nullptr);
}

SharedShader Shader::createFromSource(GLenum shaderType, const std::string &source)
{
    GLOW_ACTION();

    auto shader = std::make_shared<Shader>(shaderType);
    shader->setSource(source);
    shader->compile();
    return shader;
}

SharedShader Shader::createFromSource(GLenum shaderType, const std::vector<std::string> &sources)
{
    GLOW_ACTION();

    auto shader = std::make_shared<Shader>(shaderType);
    shader->setSource(sources);
    shader->compile();
    return shader;
}

SharedShader Shader::createFromFile(GLenum shaderType, const std::string &filename)
{
    GLOW_ACTION();

    auto loader = glow::getLoader();

    auto source = loader->LoadTextFromAssets(filename);
    if (source == "")
    {
        error() << "Unable to read shader file " << filename;
        return nullptr;
    }

    auto shader = std::make_shared<Shader>(shaderType);
    shader->mFileName = filename;
    shader->mLastModification = util::fileModificationTime(filename);
    shader->setSource(source);
    shader->compile();
    return shader;
}

bool Shader::newerVersionAvailable()
{
    // check if dependency changed
    for (auto const &kvp : mFileDependencies)
        if (util::fileModificationTime(kvp.first) > kvp.second)
            return true;

    // see if file-backed
    if (!mFileName.empty() && std::ifstream(mFileName).good())
    {
        if (util::fileModificationTime(mFileName) > mLastModification)
            return true;
    }

    return false;
}

void Shader::setShaderParser(const SharedShaderParser &parser)
{
    //sParser = parser;
}

bool Shader::resolveFile(const std::string &name, GLenum &shaderType, std::string &content, std::string &realFileName)
{
    // detect shader type
    auto found = false;
    for (auto const &kvp : glow::shaderEndingToType)
        if (util::endswith(name, kvp.first))
        {
            shaderType = kvp.second;
            break;
        }

    // direct match
    auto src = glow::getLoader()->LoadTextFromAssets(name);

    if (src != "")
    {
        realFileName = name;
        content = src;
        return true;
    }

    return false;
}

void Shader::addDependency(const std::string &filename)
{
    // ignore multi-adds
    for (auto const &kvp : mFileDependencies)
        if (kvp.first == filename)
            return;

    mFileDependencies.push_back(make_pair(filename, util::fileModificationTime(filename)));
}
