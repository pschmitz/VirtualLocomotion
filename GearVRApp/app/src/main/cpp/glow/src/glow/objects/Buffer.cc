#include "Buffer.hh"

#include <limits>
#include <cassert>

using namespace glow;

Buffer::Buffer(GLenum bufferType) : mType(bufferType)
{
    mObjectName = std::numeric_limits<decltype(mObjectName)>::max();
    glGenBuffers(1, &mObjectName);
    assert(mObjectName != std::numeric_limits<decltype(mObjectName)>::max() && "No OpenGL Context?");
}

Buffer::~Buffer()
{
    glDeleteBuffers(1, &mObjectName);
}
