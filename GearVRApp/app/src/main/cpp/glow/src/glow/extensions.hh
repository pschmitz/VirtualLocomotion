#pragma once
#include "gl.hh"

#define GL_DEBUG_OUTPUT 0x92E0
#define GL_DEBUG_OUTPUT_SYNCHRONOUS 0x8242
#define GL_DEBUG_NEXT_LOGGED_MESSAGE_LENGTH 0x8243
#define GL_DEBUG_CALLBACK_FUNCTION 0x8244
#define GL_DEBUG_CALLBACK_USER_PARAM 0x8245
#define GL_DEBUG_SOURCE_API 0x8246
#define GL_DEBUG_SOURCE_WINDOW_SYSTEM 0x8247
#define GL_DEBUG_SOURCE_SHADER_COMPILER 0x8248
#define GL_DEBUG_SOURCE_THIRD_PARTY 0x8249
#define GL_DEBUG_SOURCE_APPLICATION 0x824A
#define GL_DEBUG_SOURCE_OTHER 0x824B
#define GL_DEBUG_TYPE_ERROR 0x824C
#define GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR 0x824D
#define GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR 0x824E
#define GL_DEBUG_TYPE_PORTABILITY 0x824F
#define GL_DEBUG_TYPE_PERFORMANCE 0x8250
#define GL_DEBUG_TYPE_OTHER 0x8251
#define GL_MAX_DEBUG_MESSAGE_LENGTH 0x9143
#define GL_MAX_DEBUG_LOGGED_MESSAGES 0x9144
#define GL_DEBUG_LOGGED_MESSAGES 0x9145
#define GL_DEBUG_SEVERITY_HIGH 0x9146
#define GL_DEBUG_SEVERITY_MEDIUM 0x9147
#define GL_DEBUG_SEVERITY_LOW 0x9148
#define GL_DEBUG_TYPE_MARKER 0x8268
#define GL_DEBUG_TYPE_PUSH_GROUP 0x8269
#define GL_DEBUG_TYPE_POP_GROUP 0x826A
#define GL_DEBUG_SEVERITY_NOTIFICATION 0x826B
#define GL_MAX_DEBUG_GROUP_STACK_DEPTH 0x826C
#define GL_DEBUG_GROUP_STACK_DEPTH 0x826D
#define GL_DEBUG_SOURCE_API 0x8246
#define GL_DEBUG_SOURCE_WINDOW_SYSTEM 0x8247
#define GL_DEBUG_SOURCE_SHADER_COMPILER 0x8248
#define GL_DEBUG_SOURCE_THIRD_PARTY 0x8249
#define GL_DEBUG_SOURCE_APPLICATION 0x824A
#define GL_DEBUG_SOURCE_OTHER 0x824B
#define GL_DEBUG_TYPE_ERROR 0x824C
#define GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR 0x824D
#define GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR 0x824E
#define GL_DEBUG_TYPE_PORTABILITY 0x824F
#define GL_DEBUG_TYPE_PERFORMANCE 0x8250
#define GL_DEBUG_TYPE_OTHER 0x8251

//EXT_multisampled_render_to_texture
#define RENDERBUFFER_SAMPLES_EXT                    0x8CAB
#define FRAMEBUFFER_INCOMPLETE_MULTISAMPLE_EXT      0x8D56
#define MAX_SAMPLES_EXT                             0x8D57
#define FRAMEBUFFER_ATTACHMENT_TEXTURE_SAMPLES_EXT  0x8D6C

namespace glow
{
    namespace ext {
        typedef void (*GLDEBUGPROC)(
            GLenum source,
            GLenum type,
            GLuint id,
            GLenum severity,
            GLsizei length,
            const GLchar* message,
            const void* userParam);
        typedef void (*DebugMessageCallbackPtr)(GLDEBUGPROC callback,
                                  const void* userParam);

        typedef void (*PushDebugGroupPtr)(GLenum source,GLuint id,GLsizei length,const char * message);

        typedef void (*PopDebugGroupPtr)(void);

        typedef void (*DrawArraysIndirectPtr)(void);

        extern DebugMessageCallbackPtr glDebugMessageCallback;
        extern PushDebugGroupPtr glPushDebugGroup;
        extern PopDebugGroupPtr glPopDebugGroup;

        //EXT_multisampled_render_to_texture
        //https://www.khronos.org/registry/OpenGL/extensions/EXT/EXT_multisampled_render_to_texture.txt
        typedef void (GL_APIENTRY *RenderbufferStorageMultisamplePtr)(GLenum target,
                                                                               GLsizei samples,
                                                                               GLenum internalformat,
                                                                               GLsizei width,
                                                                               GLsizei height);

        typedef void (GL_APIENTRY *FramebufferTexture2DMultisamplePtr)(GLenum target,
                                                                                GLenum attachment,
                                                                                GLenum textarget,
                                                                                GLuint texture, GLint level,
                                                                                GLsizei samples);

        extern RenderbufferStorageMultisamplePtr glRenderbufferStorageMultisample;
        extern FramebufferTexture2DMultisamplePtr glFramebufferTexture2DMultisample;


        void loadAll();
    }
}
