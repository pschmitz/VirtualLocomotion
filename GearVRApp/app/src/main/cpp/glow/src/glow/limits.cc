#include "limits.hh"

#include "gl.hh"

int glow::limits::maxCombinedTextureImageUnits = -1;
float glow::limits::maxAnisotropy = -1;

void glow::limits::update()
{
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxCombinedTextureImageUnits);
}
