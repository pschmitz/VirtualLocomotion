#include "Texture.hh"

#include <limits>
#include <cassert>

using namespace glow;

Texture::Texture(GLenum target, GLenum bindingTarget, GLenum internalFormat)
  : mTarget(target), mBindingTarget(bindingTarget), mInternalFormat(internalFormat)
{
    mObjectName = std::numeric_limits<decltype(mObjectName)>::max();
    glGenTextures(1, &mObjectName);
    assert(mObjectName != std::numeric_limits<decltype(mObjectName)>::max() && "No OpenGL Context?");

    GLint oldTex;
    glGetIntegerv(mBindingTarget, &oldTex);
    glBindTexture(mTarget, mObjectName); // pin this texture to the correct type
    glBindTexture(mTarget, oldTex);      // restore prev
}

Texture::~Texture()
{
    glDeleteTextures(1, &mObjectName);
}
