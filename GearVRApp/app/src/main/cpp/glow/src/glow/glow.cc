#include "glow.hh"
#include "gl.hh"
#include "debug.hh"
#include "limits.hh"
#include "extensions.hh"

#include "glow/common/log.hh"

using namespace glow;

glow::_glowGLVersion glow::OGLVersion;

static IContentLoader* contentLoader = 0;

bool glow::initGLOW(IContentLoader* _loader)
{
    contentLoader = _loader;
    glow::ext::loadAll();

    // install debug message - not in GLES apparently, but there's an extension
    if (glow::ext::glDebugMessageCallback)
    {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glow::ext::glDebugMessageCallback(DebugMessageCallback, nullptr);
    }

    // update limits
    limits::update();

    return true;
}

IContentLoader* glow::getLoader() { return contentLoader; }


