//
// Created by Benni on 05.12.2016.
//

#include <fstream>

#include <android/log.h>

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_interpolation.hpp>
#include <glm/gtx/string_cast.hpp>

#include "Framework/OpenGL.h"
#include "Framework/Application.h"
#include "Framework/VrapiRenderInterface.h"

#include "Parser.h"
#include "DataCaptureManager.h"

#if VRAPI
#include <VrApi_Helpers.h>
#endif

#include "Framework/UdpSender.h"
#include "Framework/TcpSender.h"

#include "TestScene.h"

#define SEND_MISSED_FRAMES 0

namespace {
    const glm::vec3 sceneOffsetOrigin(-4.f, 0.f, 4.f);

    glm::mat4 transformToViewMatrix(glm::mat4 transform) {
        glm::mat4 view(1.f);

        glm::vec4 position = transform * glm::vec4(0, 0, 0, 1);

        glm::mat3 rotation(transform);
        glm::mat4 transpose = glm::transpose(rotation);

        view =
            glm::translate(glm::mat4(1.f), -glm::vec3(transpose * position)) *
            glm::mat4(transpose);

        return view;
    }
}

TestScene::TestScene() :
    experiment(userKinematic, dataCapture)
//    splatRenderer("output.vr")
{
}

void TestScene::Initialize()
{
    float virtualSceneDistance = 6.5f;
}


void TestScene::Load()
{
    modelRenderer.Load(App->GetContentLoader());
//    splatRenderer.Load(App->GetContentLoader());
    boundaryRenderer.Load(App->GetContentLoader());

    experiment.Load(&modelRenderer);

    currentTrackingPosition = glm::vec3(0, 1.8f, 0);

    loadModels();

//    viewerToBlender.Connect("coudenhove", 1338);
    viewerToOpenAL.Connect("coudenhove", 1338);
}

void TestScene::loadModels()
{
    modelRenderer.LoadModel("models/GroundBorder.bm",
                            "textures/GroundBorderSurface_Color.png");
    modelRenderer.LoadModel("models/RoomGround.bm",
                            "textures/RoomGroundSurface_Color.png");
    modelRenderer.LoadModel("models/Ceiling.bm",
                            "textures/CeilingSurface_Color.png");
    modelRenderer.LoadModel("models/RoomWall.bm",
                            "textures/RoomWallSurface_Color.png");
    modelRenderer.LoadModel("models/OrangeWall.bm",
                            "textures/OrangeWallSurface_Color.png");
    modelRenderer.LoadModel("models/gardenWall.bm",
                            "textures/gardenWallSurface_Color.png");
    modelRenderer.LoadModel("models/gardenroundPlane.bm",
                            "textures/gardenroundPlaneSurface_Color.png");
    modelRenderer.LoadModel("models/picture.bm",
                            "textures/pictureSurface_Color.png");
    modelRenderer.LoadModel("models/carpet.bm",
                            "textures/carpetSurface_Color.png");
    modelRenderer.LoadModel("models/pictureID456.bm",
                            "textures/picture1Surface Color.png");
    //modelRenderer.LoadModel("models/typewriter.bm",
    //                        "textures/typewriterSurface_Color.png");
    modelRenderer.LoadModel("models/Windowbar.bm",
                            "textures/WindowbarSurface_Color.png");
    modelRenderer.LoadModel("models/Frame.bm",
                            "textures/FrameSurface_Color.png");
    modelRenderer.LoadModel("models/books.bm",
                            "textures/booksSurface_Color.png");
    modelRenderer.LoadModel("models/DoorFrame.bm",
                            "textures/DoorFrameSurface_Color.png");
    modelRenderer.LoadModel("models/soundbox1.bm",
                            "textures/soundbox1Surface_Color.png");
    modelRenderer.LoadModel("models/Zylinder.bm",
                            "textures/ZylinderSurface_Color.png");
    modelRenderer.LoadModel("models/Handle.bm",
                            "textures/HandleSurface_Color.png");
    modelRenderer.LoadModel("models/Door.bm",
                            "textures/DoorSurface_Color.png");
    modelRenderer.LoadModel("models/lamptop_alpha.bm",
                            "textures/lamptop_alphaSurface_Color.png");
    modelRenderer.LoadModel("models/lamp.bm",
                            "textures/lampSurface_Color.png");
    modelRenderer.LoadModel("models/tvbox.bm",
                            "textures/tvboxSurface_Color.png");
    modelRenderer.LoadModel("models/tv_body.bm",
                            "textures/tv_bodySurface_Color.png");
    modelRenderer.LoadModel("models/tv_screen.bm",
                            "textures/tv_screenSurface_Color.png");
    modelRenderer.LoadModel("models/vase.bm",
                            "textures/vaseSurface_Color.png");
    modelRenderer.LoadModel("models/klinken.2.bm",
                            "textures/klinken.2Surface_Color.png");
    modelRenderer.LoadModel("models/box.bm",
                            "textures/boxSurface_Color.png");
    modelRenderer.LoadModel("models/blackparts.bm",
                            "textures/blackpartsSurface_Color.png");
    modelRenderer.LoadModel("models/coloredparts.bm",
                            "textures/coloredpartsSurface_Color.png");
    modelRenderer.LoadModel("models/soundbox1ID643.bm",
                            "textures/soundbox11Surface Color.png");
    modelRenderer.LoadModel("models/radiator.bm",
                            "textures/radiatorSurface_Color.png");
    modelRenderer.LoadModel("models/books1.bm",
                            "textures/books1Surface_Color.png");
    modelRenderer.LoadModel("models/books2.bm",
                            "textures/books2Surface_Color.png");
    modelRenderer.LoadModel("models/books3.bm",
                            "textures/books3Surface_Color.png");
    modelRenderer.LoadModel("models/books4.bm",
                            "textures/books4Surface_Color.png");
    modelRenderer.LoadModel("models/books4.2.bm",
                            "textures/books4.2Surface_Color.png");
    modelRenderer.LoadModel("models/chair3.bm",
                            "textures/chair3Surface_Color.png");
    modelRenderer.LoadModel("models/shelve.bm",
                            "textures/shelveSurface_Color.png");
    modelRenderer.LoadModel("models/chair2.bm",
                            "textures/chair2Surface_Color.png");
    modelRenderer.LoadModel("models/chair1.bm",
                            "textures/chair1Surface_Color.png");
    modelRenderer.LoadModel("models/table.bm",
                            "textures/tableSurface_Color.png");
    modelRenderer.LoadModel("models/shelveID751.bm",
                            "textures/shelve1Surface Color.png");
    modelRenderer.LoadModel("models/livingroom_tablelamp.bm",
                            "textures/livingroom_tablelampSurface_Color.png");
    modelRenderer.LoadModel("models/desklamp.bm",
                            "textures/desklampSurface_Color.png");
    auto radio = modelRenderer.LoadModel("models/radio.bm",
                                         "textures/radio.png");
    radio->setTransformation(glm::translate(glm::vec3{8.3f,0.75f,-2.f})
                           * glm::scale(glm::vec3(1.5f))
                           * glm::rotate(-1.8f, glm::vec3{0,1,0}));

    experiment.setRadioModel(radio);
    dataCapture.Start(App);
}

void TestScene::Update(double frameTime)
{
    VrapiRenderInterface *vrapiRenderInterface = dynamic_cast<VrapiRenderInterface*>(
                                                     this->App->GetVrRenderInterface());
    if(App->IsTouchPressed())
    {
        if(!lastFrameButtonPressed)
        {
            totalRealRotation = 0.f;
            totalVirtualRotation = 0.f;
            totalWalkedDistance = 0.f;
            dataCapture.LogReset(userKinematic);

            resetToGlobalTrackerPosition(vrapiRenderInterface);

            lastIsInitialized = false;
            additionalSceneRotation = glm::mat4(1.0f);

            experiment.reset();
        }
        lastFrameButtonPressed = true;
    }
    else
    {
        lastFrameButtonPressed = false;
    }

    computeKinematic(frameTime);

    //     glm::mat4 testTransform = glm::toMat4(rotation);
    //     testTransform[3][0] = currentTrackingPosition[0];
    //     testTransform[3][1] = currentTrackingPosition[1];
    //     testTransform[3][2] = currentTrackingPosition[2];

    glm::mat4 rotX90 = glm::axisAngleMatrix(glm::vec3(1, 0, 0),
                                            glm::radians(90.0f));
    // viewerToBlender.UpdateViewer(
    //     rotX90 * userKinematic.getVirtualHeadTransform(),
    //     frameTime);
    viewerToOpenAL.UpdateViewer(
        userKinematic.getVirtualHeadTransform(),
        frameTime);

    Scene::Update(frameTime);

    vrapiRenderInterface->setGain(userKinematic.getRotationGain());
    experiment.frame();
}

void TestScene::resetToGlobalTrackerPosition(
    VrapiRenderInterface *vrapiRenderInterface)
{
    // Get rotation around the y axis from the tracking system.
    // Using the Y Rotation given by the tracking system did not work.
    glm::vec3 temp = (currentTrackingOrientation * glm::vec4(1, 0, 0, 1));
    temp.y = 0;
    temp = glm::normalize(temp);
    float dotProduct = glm::dot(temp, glm::vec3(1, 0, 0));
    dotProduct = glm::clamp(dotProduct, -1.0f, 1.0f);
    float angleToTarget = glm::acos(dotProduct);
    glm::vec3 test = glm::cross(temp, glm::vec3(1, 0, 0));
    float sign = glm::sign(test.y);
    if(sign != 0.0f && angleToTarget != 0.0f)
    {
        additionalSceneRotation = glm::mat4(1.0f);
        lastRecenterPose =
            glm::rotate(glm::mat4(1.0f), -angleToTarget,
                        glm::vec3(0, sign, 0));
        userKinematic.setVirtualHeadTransform(
            userKinematic.getRealHeadTransform());
        vrapiRenderInterface->RecenterPose();
    }
}

void TestScene::computeKinematic(double frameTime)
{
    IVrRenderInterface *vrRenderInterface =
        this->App->GetVrRenderInterface();
    VrapiRenderInterface *vrapiRenderInterface =
        dynamic_cast<VrapiRenderInterface*>(vrRenderInterface);

    const ovrHeadModelParms headModelParms = vrapi_DefaultHeadModelParms();
    ovrTracking *tracking = vrapiRenderInterface->GetOvrTracking();

    glm::vec3 realHeadPositionOld =
        userKinematic.getRealHeadTransform() * glm::vec4(0, 0, 0, 1);
    glm::vec3 realHeadPositionOffset =
        currentTrackingPosition - realHeadPositionOld;

    userKinematic.setMovementVelocityHead(
        (realHeadPositionOffset) / float(frameTime / 1000.0));
    userKinematic.setAngularVelocityHead(
        glm::mat3(glm::toMat4(toGlm(tracking->HeadPose.Pose.Orientation))) *
        toGlm(tracking->HeadPose.AngularVelocity));

    glm::mat4 realHeadOrientation =
        lastRecenterPose *
        glm::toMat4(toGlm(tracking->HeadPose.Pose.Orientation));
    userKinematic.setRealHeadTransform(
        glm::translate(glm::mat4(1.0f),
                       currentTrackingPosition) *
        realHeadOrientation);

    if (lastIsInitialized)
    {
        float realAngle = userKinematic.getAngularVelocityHead().y * (frameTime / 1000.0);
        float additionalRotationAngle = realAngle * (userKinematic.getRotationGain() - 1.f);

        totalRealRotation += std::abs(realAngle);
        totalVirtualRotation += std::abs(realAngle * userKinematic.getRotationGain());
        totalWalkedDistance += glm::length(realHeadPositionOffset);

        additionalSceneRotation =
            additionalSceneRotation *
            glm::rotate(glm::mat4(1.0f),
                        additionalRotationAngle, glm::vec3(0, 1, 0));
    }

    glm::vec3 virtualHeadPositionOld =
        userKinematic.getVirtualHeadTransform() * glm::vec4(0, 0, 0, 1);
    glm::vec3 virtualHeadPositionOffset =
        additionalSceneRotation *
        glm::vec4(realHeadPositionOffset, 1);

    userKinematic.setVirtualHeadTransform(
        glm::translate(glm::mat4(1.f),
                       virtualHeadPositionOld + virtualHeadPositionOffset) *
        additionalSceneRotation * realHeadOrientation);

    lastIsInitialized = true;
}

void TestScene::Draw(double frameTime, GraphicsContext *context, int eyeIndex)
{
    IVrRenderInterface *vrRenderInterface =  this->App->GetVrRenderInterface();

    Camera camera;
    camera.ProjectionMatrix = vrRenderInterface->GetProjectionMatrix(eyeIndex);

#if VRAPI
    const ovrHeadModelParms headModelParms = vrapi_DefaultHeadModelParms();

    const float eyeOffset =
        (eyeIndex == 1 ? -0.5f : 0.5f) * headModelParms.InterpupillaryDistance;

    glm::mat4 realViewMatrix =
        glm::translate(glm::mat4(1.0f), glm::vec3(eyeOffset, 0, 0)) *
        transformToViewMatrix(userKinematic.getRealHeadTransform());

    glm::mat4 virtualViewMatrix =
        glm::translate(glm::mat4(1.0f), glm::vec3(eyeOffset, 0, 0)) *
        transformToViewMatrix(userKinematic.getVirtualHeadTransform());

    camera.ViewMatrix = virtualViewMatrix; // * glm::translate(glm::mat4(1.0f),
                           //                 sceneOffsetOrigin);
#endif // VRAPI

    glClearDepthf(1.0f);
    glClearColor(111.0/255.0,202.0/255.0,232.0/255.0,1); //Fake sky color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    modelRenderer.Draw(camera);

    auto fov_deg = vrRenderInterface->getFovy();
    auto znear = vrRenderInterface->getNear();
    //splatRenderer.Draw(camera, vrRenderInterface->getViewport(), fov_deg, znear);

    // Use real movement for the boundary view matrix.
    auto realPos = userKinematic.getRealHeadPosition();
//    boundaryRenderer.Render(realPos, glm::vec3(1, 1, 0),
//                            realViewMatrix, camera.ProjectionMatrix,
//                            2.1f, -5.6f, 3.9f, 3.5f);

#if VRAPI
    currentFrame++;
#endif // VRAPI
}

void TestScene::Unload()
{
    modelRenderer.Unload();
}

void TestScene::ReceivePackage(JNIEnv *env, void *buffer, int length)
{
    int header0 = *((int*)buffer);
    int header1 = *(((int*)buffer) + 1);
    int header2 = *(((int*)buffer) + 2);
    if(header0 == 42 && header1 == 0 && header2 == 42)
    {
        DebugControlMessage *messagePtr = ((DebugControlMessage *)(((char*)buffer) + sizeof(int)*3));
        DebugControlMessage message = messagePtr[0];
//        adjustableEyeOffset = message.adjustableEyeOffset;
//        rotationCenterOffset = message.rotationCenterOffset;
    }

    unsigned long frameNr;
    std::vector<body> bodies;
    std::vector<marker> markers;
    ParseMessage(buffer, length, frameNr, bodies, markers);

    if(lastFrameNr < frameNr || (lastFrameNr - frameNr) > 1000)
    {
        hasCompleteBody = false;
        for (body const& body : bodies)
        {
            if (body.id == 0)
            {   // Headset

                currentTrackingPosition.x =
                    body.loc[0] / 1000.0f - sceneOffsetOrigin.x;
                // correct for 43mm tracking 0 offset
                currentTrackingPosition.y =
                    (body.loc[1] + 43) / 1000.0f - sceneOffsetOrigin.y;
                currentTrackingPosition.z =
                    body.loc[2] / 1000.0f - sceneOffsetOrigin.z;

                //TODO: Test
                //Set columes
                currentTrackingOrientation[0]= glm::vec4(body.rot[0], body.rot[1], body.rot[2], 0);
                currentTrackingOrientation[1]= glm::vec4(body.rot[3], body.rot[4], body.rot[5], 0);
                currentTrackingOrientation[2]= glm::vec4(body.rot[6], body.rot[7], body.rot[8], 0);

                //__android_log_print(ANDROID_LOG_ERROR, "Input" , "%f %f %f", currentTrackingPosition.x, currentTrackingPosition.y, currentTrackingPosition.z);
                hasCompleteBody = true;
            } else if (body.id <= 2) {
                // One of 2 Torso Trackers

                auto& trans = torsoTransforms[body.id - 1];
                trans = glm::mat4(0.0f);
                trans[0] = glm::vec4(body.rot[0], body.rot[1], body.rot[2], 0);
                trans[1] = glm::vec4(body.rot[3], body.rot[4], body.rot[5], 0);
                trans[2] = glm::vec4(body.rot[6], body.rot[7], body.rot[8], 0);
                trans[3] = glm::vec4(body.loc[0], body.loc[1], body.loc[2], 1);
            } else {
                LogError("Invalid body ID ");
            }
        }

//        LogError(
//                dataCleanTest.GetCleanedStringFromDataLine(
//                        dataCapture.GetKinematicDataString(frameNr, userKinematic,
//                                                   torsoTransforms, totalRealRotation,
//                                                   totalVirtualRotation, totalWalkedDistance)
//
//                )
//        );

        dataCapture.AddUserKinematicData(frameNr, userKinematic, torsoTransforms,
            totalRealRotation, totalVirtualRotation, totalWalkedDistance);
        if (lastFrameNr + 1 != frameNr)
        {
            dataCapture.LogTrackingFrameSkip(frameNr - lastFrameNr - 1);
        }

#if SEND_MISSED_FRAMES
        static TcpSender sender("coudenhove", 1337);
        if (sender.fd != -1)
        {
            static char logbuffer[1024];

            if (lastFrameNr + 1 != frameNr)
            {
                sprintf(logbuffer, "Missed %d frames\n", int(frameNr - lastFrameNr - 1));
                write(sender.fd, logbuffer, strlen(logbuffer));
            }
            {
                thread_local auto last = std::chrono::high_resolution_clock::now();
                auto now = std::chrono::high_resolution_clock::now();
                auto delta = float((now - last).count()) / 1000000.f;
                last = now;

                if (delta > 32.0f)
                {
                    sprintf(logbuffer, "Frame late: took %f ms\n", delta);
                    write(sender.fd, logbuffer, strlen(logbuffer));
                }
            }
        }
#endif

        lastFrameNr = frameNr;
    }

    /*if (!bodies.empty()){
        thread_local auto last = std::chrono::high_resolution_clock::now();
        auto now = std::chrono::high_resolution_clock::now();
        auto delta = float((now - last).count()) / 1000.f;
        last = now;

        static UdpSender sender("coudenhove", 1337);
        auto p = currentTrackingPosition;

        static char buffer[1024];
        sprintf(buffer, "{t: %f, p: [%f, %f, %f]},\n", delta, p.x, p.y, p.z);
        write(sender.fd, buffer, strlen(buffer));

        LogError("Logged stuff.");
    }*/
}
