//
// Created by Benni on 13.12.2016.
//

#include "FrameworkTestScene.h"
#include "Framework/ObjLoader.h"

using glm::vec2;
using glm::vec3;
using glm::vec4;

void FrameworkTestScene::Initialize()
{

}

void FrameworkTestScene::Load()
{
    spriteBatch.Load(App->GetContentLoader());

    //Load Texture
    TextureData textureData = App->GetContentLoader()->LoadTextureFromAssets(
            "CinderblockClean0003_1_S.jpg");
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureData.Width, textureData.Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData.Data);
    glGenerateMipmap(GL_TEXTURE_2D);

    modelRenderer.Load(App->GetContentLoader());
}

void FrameworkTestScene::Update(double frameTime)
{
    Scene::Update(frameTime);
}

float totalTime = 0;

void FrameworkTestScene::Draw(
    double frameTime, GraphicsContext *context, int eyeIndex)
{
    if(eyeIndex == 0)
    {
        glClearColor(1,0,0,1);
    }
    else
    {
        glClearColor(0,1,1,1);
    }
    glClear(GL_COLOR_BUFFER_BIT);

    totalTime += frameTime;

    Camera camera;
    camera.ViewMatrix =
        glm::lookAt(
            vec3(10 * glm::sin(totalTime), 10, 10 * glm::cos(totalTime)),
            vec3(0,0,0), vec3(0,1,0));
    camera.ProjectionMatrix =
        glm::perspectiveFov(glm::radians(90.0f),
                            float(App->GetWidth()/2), float(App->GetHeight()),
                            1.0f, 100.0f);

    modelRenderer.Draw(camera);

    spriteBatch.Begin(context);
    spriteBatch.Draw(texture, vec2(150,150), vec2(500,500), vec4(1,1,1,1));
    spriteBatch.Draw(vec2(100,100), vec2(1800,100), vec4(1,1,0,1));
    spriteBatch.End();
}

void FrameworkTestScene::Unload()
{
    spriteBatch.Unload();
    modelRenderer.Unload();
}
