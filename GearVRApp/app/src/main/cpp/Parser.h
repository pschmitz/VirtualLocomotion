//
// Created by Benni on 07.12.2016.
//

#ifndef GEARVRAPP_PARSER_H
#define GEARVRAPP_PARSER_H

#include <vector>

struct body
{
    unsigned long id;
    float quality;
    float loc[3];
    float ang[3];
    float rot[9];
};

struct marker
{
    unsigned long id;
    float quality;
    float loc[3];
};

void ParseMessage(void *buffer, int length, unsigned long &frameNr,
                  std::vector<body> &bodies, std::vector<marker> &markers);

#endif //GEARVRAPP_PARSER_H
