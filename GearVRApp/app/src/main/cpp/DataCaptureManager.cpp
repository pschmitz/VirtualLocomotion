//
// Created by Benni on 07.12.2016.
//

#include "DataCaptureManager.h"
#include "TestScene.h"
#include "Framework/Log.h"
#include "Framework/Math.h"

namespace {
    const bool flushStreamAfterEachEntry = true;
        /*
        If true, then the stream for writing is flushed after every data entry that is added.
        If false, the stream is only flushed once in the Finish() method.
        */

    const string recordDirectoryName = "/VirtualLocomotion/Records";
        /*
        Ensure, that the directory exists on the device, otherwise app will crash.
        */

    const string recordFilenamePrefix = "/GearVRRecord_";
        /*
        A timestamp will be attached to the prefix to identify each record file.
        */

    const string fieldSeparator = ",";
        /*
        The character sequence that is used to subdivide each record into fields.
        */

    const string lineSeparator = "\n";
}



//=== === === === === === === === === === === === === === ===
//Public interface:


void DataCaptureManager::Start(Application* app)
{
    if (!capturing)
    {
        capturing = true;
		string filenameSuffix = currentDateAndTimeAsString();

        std::string filePath =
            app->GetNativeToJava()->GetExternalStorageFolder(app->GetCurrentEnv()) +
            recordDirectoryName + recordFilenamePrefix + filenameSuffix;

        saveDataFile = fopen(filePath.c_str(), "w");
        //appendHeadLineToFile();
    }
}

void DataCaptureManager::Finish()
{
    if (capturing)
    {
        fputs("FINISHED DATA CAPTURING.", saveDataFile);
        fflush(saveDataFile);
        fclose(saveDataFile);
        capturing = false;
    }
}

void DataCaptureManager::appendKinematic(UserKinematic& kinematic, std::string& to)
{
    to.append(to_string(kinematic.getRotationGain()));
    to.append(fieldSeparator);

    to.append(getMatlabString(kinematic.getRealHeadTransform()));
    to.append(fieldSeparator);

    to.append(getMatlabString(kinematic.getVirtualHeadTransform()));
    to.append(fieldSeparator);
}

void DataCaptureManager::LogTrackingFrameSkip(int numFrames) {
    if (!capturing) return;

    string result = LineTypes::tracking_loss + fieldSeparator +
                    to_string(currentTimestamp()) + fieldSeparator +
                    to_string(numFrames);

    result.append(lineSeparator);
    fputs(result.c_str(), saveDataFile);
    if (flushStreamAfterEachEntry) {
        fflush(saveDataFile);
    }
}

void DataCaptureManager::LogTargetCollected(UserKinematic &kinematic, glm::vec2 targetPos) {
    if (!capturing) return;

    string result = LineTypes::target + fieldSeparator +
                    to_string(currentTimestamp()) + fieldSeparator;
    result.append(to_string(kinematic.getRotationGain()));
    result.append(fieldSeparator);
    appendKinematic(kinematic, result);
    result.append(fieldSeparator);
    result.append(to_string(targetPos));

    result.append(lineSeparator);
    fputs(result.c_str(), saveDataFile);
    if (flushStreamAfterEachEntry) {
        fflush(saveDataFile);
    }
}

void DataCaptureManager::LogReset(UserKinematic &kinematic) {
    if (!capturing) return;

    string result = LineTypes::reset + fieldSeparator +
                    to_string(currentTimestamp()) + fieldSeparator;
    appendKinematic(kinematic, result);

    result.append(lineSeparator);
    fputs(result.c_str(), saveDataFile);
    if (flushStreamAfterEachEntry) {
        fflush(saveDataFile);
    }
}

void DataCaptureManager::LogUserReset(float gain) {
    if (!capturing) return;

    string result = LineTypes::user_reset + fieldSeparator +
                    to_string(currentTimestamp()) + fieldSeparator;
    result.append(to_string(gain));
    result.append(lineSeparator);

    fputs(result.c_str(), saveDataFile);
    if (flushStreamAfterEachEntry) {
        fflush(saveDataFile);
    }
}

std::string DataCaptureManager::GetKinematicDataString(
        unsigned long frameNumber,
        UserKinematic &kinematicData, glm::mat4 const (&torsos)[2],
        float realRotation, float virtualRotation, float distance) {


    //Here the data record string is created:
    string result = LineTypes::kinematic + fieldSeparator +
                    to_string(frameNumber) + fieldSeparator +
                    to_string(currentTimestamp()) + fieldSeparator;


    //=== === === === === === === === === === === === === ===
    //Modify this part to adjust to the actual contents of
    //UserKinematic struct:

    appendKinematic(kinematicData, result);
    result.append(fieldSeparator);

    result.append(getMatlabString(torsos[0]));
    result.append(fieldSeparator);

    result.append(getMatlabString(torsos[1]));
    result.append(fieldSeparator);

    result.append(to_string(realRotation));
    result.append(fieldSeparator);
    result.append(to_string(virtualRotation));
    result.append(fieldSeparator);
    result.append(to_string(distance));

    //result.append(getMatlabString(
    //    kinematicData.getRealHeadPosition()));
    //result.append(fieldSeparator);

    // result.append(getMatlabString(
    //    kinematicData.getVirtualHeadPosition()));
    // result.append(fieldSeparator);

    // result.append(getMatlabString(
    //     kinematicData.RealWalkDifference
    //     ));
    // result.append(fieldSeparator);

    // result.append(getMatlabString(
    //     kinematicData.testMatrix
    //     ));
    //

    //=== === === === === === === === === === === === === ===

    return result;

}

void DataCaptureManager::AddUserKinematicData(
    unsigned long frameNumber,
    UserKinematic &kinematicData, glm::mat4 const (&torsos)[2],
    float realRotation, float virtualRotation, float distance)
{
    if(!capturing) return;

    std::string result = GetKinematicDataString(frameNumber, kinematicData, torsos,
                                           realRotation, virtualRotation, distance);

    result.append(lineSeparator);
    fputs(result.c_str(), saveDataFile);
    if (flushStreamAfterEachEntry) {
        fflush(saveDataFile);
    }
}



//=== === === === === === === === === === === === === === ===
//Get properly formatted strings:


string DataCaptureManager::currentDateAndTimeAsString() {
    char buffer[64];
    std::time_t now = std::time(NULL);
    tm * ptm = localtime(&now);
    strftime(buffer, sizeof(buffer), "%Y%b%d_%H-%M-%S", ptm);
    return {buffer};
}

string DataCaptureManager::getMatlabString(glm::mat4 v) {
    std::stringstream stringStream;
    stringStream << "[ "
                 << v[0][0] << " " << v[1][0] << " " << v[2][0] << " " << v[3][0] << "; "
                 << v[0][1] << " " << v[1][1] << " " << v[2][1] << " " << v[3][1] << "; "
                 << v[0][2] << " " << v[1][2] << " " << v[2][2] << " " << v[3][2] << "; "
                 << v[0][3] << " " << v[1][3] << " " << v[2][3] << " " << v[3][3] << "]";
    return stringStream.str();
}

string DataCaptureManager::getMatlabString(glm::vec3 v) {
    std::stringstream stringStream;
    stringStream << "[ " << v[0] << "; " << v[1] << "; " << v[2] << "]";
    return stringStream.str();
}

string DataCaptureManager::getMatlabString(float v) {
    return to_string(v);
}


double DataCaptureManager::currentTimestamp() {
    static auto start = std::chrono::system_clock::now();

    auto now = std::chrono::system_clock::now();
    auto delta = now - start;

    return std::chrono::duration<double>(delta).count();
}

const char* DataCaptureManager::LineTypes::kinematic = "KINEMATIC";
const char* DataCaptureManager::LineTypes::reset = "RESET";
const char* DataCaptureManager::LineTypes::user_reset = "USER_RESET";
const char* DataCaptureManager::LineTypes::tracking_loss = "TRACKING_LOSS";
const char* DataCaptureManager::LineTypes::target = "TARGET_COLLECTED";
