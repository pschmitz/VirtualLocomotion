//
// Created by Benni on 13.12.2016.
//

#include "AndroidContentLoader.h"

std::string
AndroidContentLoader::LoadTextFromAssets(std::string filename)
{
    return app->GetNativeToJava()->LoadTextFromAssets(
        app->GetCurrentEnv(), filename);
}

TextureData
AndroidContentLoader::LoadTextureFromAssets(std::string filename)
{
    return app->GetNativeToJava()->LoadTextureFromAssets(
        app->GetCurrentEnv(), filename);
}

std::vector<unsigned char>
AndroidContentLoader::LoadRawBufferFromAssets(std::string filename)
{
    return app->GetNativeToJava()->LoadRawBufferFromAssets(
        app->GetCurrentEnv(), filename);
}

string AndroidContentLoader::GetExternalStorageFolder() {
    return app->GetNativeToJava()->GetExternalStorageFolder(app->GetCurrentEnv());
}

TextureData
AndroidContentLoader::LoadTextureFromFile(std::string filename)
{
    return app->GetNativeToJava()->LoadTextureFromFile(
            app->GetCurrentEnv(), filename);
}
