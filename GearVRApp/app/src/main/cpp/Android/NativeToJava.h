//
// Created by Benni on 05.12.2016.
//

#ifndef GEARVRAPP_NATIVETOJAVA_H
#define GEARVRAPP_NATIVETOJAVA_H

#include <exception>
#include <string>
#include <vector>

#include <jni.h>

#include "../Framework/TextureData.h"

class NativeToJava
{
public:
    NativeToJava(JNIEnv *env, jobject nativeToJava);

    std::string LoadTextFromAssets(JNIEnv *env, std::string filename);
    TextureData LoadTextureFromAssets(JNIEnv *env, std::string filename);
    TextureData LoadTextureFromFile(JNIEnv *env, std::string filename);

    std::vector<unsigned char>
    LoadRawBufferFromAssets(JNIEnv *env, std::string filename);

    std::string GetExternalStorageFolder(JNIEnv *env);

    void Release(JNIEnv *env);

private:
    jobject nativeToJava;
};


#endif //GEARVRAPP_NATIVETOJAVA_H
