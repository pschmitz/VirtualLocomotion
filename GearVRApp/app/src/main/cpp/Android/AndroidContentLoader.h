//
// Created by Benni on 13.12.2016.
//

#ifndef GEARVRAPP_ANDROIDCONTENTLOADER_H
#define GEARVRAPP_ANDROIDCONTENTLOADER_H


#include "../Framework/IContentLoader.h"
#include "../Framework/Application.h"

class AndroidContentLoader : public IContentLoader
{
public:
    AndroidContentLoader(Application *app)
    {
        this->app = app;
    }

    std::string LoadTextFromAssets(std::string filename) override;
    TextureData LoadTextureFromAssets(std::string filename) override;

    virtual std::vector<unsigned char>
    LoadRawBufferFromAssets(std::string filename) override;

    virtual string GetExternalStorageFolder() override;

    TextureData LoadTextureFromFile(string filename) override;
private:
    Application *app;
};


#endif //GEARVRAPP_ANDROIDCONTENTLOADER_H
