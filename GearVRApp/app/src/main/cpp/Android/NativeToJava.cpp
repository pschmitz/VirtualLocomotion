//
// Created by Benni on 05.12.2016.
//

#include "NativeToJava.h"
#include "../Framework/Log.h"
#include <exception>
#include <stdexcept>

NativeToJava::NativeToJava(JNIEnv *env, jobject nativeToJava)
{
    // We have to create a global reference to allow access from
    // different threads/native calls. Normal jobjects are just
    // LocalRefs(per native call)
    this->nativeToJava = env->NewGlobalRef(nativeToJava);
}

std::string
NativeToJava::LoadTextFromAssets(JNIEnv *env, std::string filename)
{
    //  LoadTextFormAssets() form the java implementation of
    //  NativeToJava.
    jclass clazz = env->GetObjectClass(nativeToJava);

    jmethodID methodId =
        env->GetMethodID(clazz, "LoadTextFromAssets",                   \
                         "(Ljava/lang/String;)Ljava/lang/String;");
    jstring javaString = env->NewStringUTF(std::string(filename).c_str());
    jstring returnedString =
        (jstring)env->CallObjectMethod(nativeToJava, methodId, javaString);
    const char *nativeString = env->GetStringUTFChars(returnedString, 0);
    std::string result = std::string(nativeString);
    env->ReleaseStringUTFChars(javaString, nativeString);
    return result;
}

TextureData
NativeToJava::LoadTextureFromAssets(JNIEnv *env, std::string filename)
{
    TextureData textureData;

    //TODO: test if you have to use DeleteLocalRef on all used jobjects
    //env->DeleteLocalRef()

    jclass clazz = env->GetObjectClass(nativeToJava);
    // Get the method that you want to call
    jmethodID methodId =
        env->GetMethodID(
            clazz, "LoadTextureDataFromAssets",
            "(Ljava/lang/String;)Lgraphicsrwth/gearvrapp/TextureData;");

    jstring javaString = env->NewStringUTF(std::string(filename).c_str());

    jobject javaTextureData =
        env->CallObjectMethod(nativeToJava, methodId, javaString);
    jclass javaTextureClass = env->GetObjectClass(javaTextureData);

    jfieldID widthField = env->GetFieldID(javaTextureClass, "Width", "I");
    jfieldID heightField = env->GetFieldID(javaTextureClass, "Height", "I");
    jfieldID bytesField = env->GetFieldID(javaTextureClass, "Bytes", "[B");

    textureData.Width = env->GetIntField(javaTextureData, widthField);
    textureData.Height = env->GetIntField(javaTextureData, heightField);
    jbyteArray javabytearray =
        (jbyteArray)env->GetObjectField(javaTextureData, bytesField);
    textureData.DataLength = env->GetArrayLength(javabytearray);

    jbyte *javaBytes = env->GetByteArrayElements(javabytearray, nullptr);
    textureData.Data = new byte[textureData.DataLength];
    for (int i = 0; i < textureData.DataLength; ++i)
    {
        textureData.Data[i] = javaBytes[i];
    }
    env->ReleaseByteArrayElements(javabytearray, javaBytes, 0);

    return textureData;
}

TextureData
NativeToJava::LoadTextureFromFile(JNIEnv *env, std::string filename)
{
    TextureData textureData;

    //TODO: test if you have to use DeleteLocalRef on all used jobjects
    //env->DeleteLocalRef()

    jclass clazz = env->GetObjectClass(nativeToJava);
    // Get the method that you want to call
    jmethodID methodId =
            env->GetMethodID(
                    clazz, "LoadTextureDataFromFile",
                    "(Ljava/lang/String;)Lgraphicsrwth/gearvrapp/TextureData;");

    jstring javaString = env->NewStringUTF(std::string(filename).c_str());

    jobject javaTextureData =
            env->CallObjectMethod(nativeToJava, methodId, javaString);
    jclass javaTextureClass = env->GetObjectClass(javaTextureData);

    jfieldID widthField = env->GetFieldID(javaTextureClass, "Width", "I");
    jfieldID heightField = env->GetFieldID(javaTextureClass, "Height", "I");
    jfieldID bytesField = env->GetFieldID(javaTextureClass, "Bytes", "[B");

    textureData.Width = env->GetIntField(javaTextureData, widthField);
    textureData.Height = env->GetIntField(javaTextureData, heightField);
    jbyteArray javabytearray =
            (jbyteArray)env->GetObjectField(javaTextureData, bytesField);
    textureData.DataLength = env->GetArrayLength(javabytearray);

    jbyte *javaBytes = env->GetByteArrayElements(javabytearray, nullptr);
    textureData.Data = new byte[textureData.DataLength];
    for (int i = 0; i < textureData.DataLength; ++i)
    {
        textureData.Data[i] = javaBytes[i];
    }
    env->ReleaseByteArrayElements(javabytearray, javaBytes, 0);

    return textureData;
}

std::vector<unsigned char>
NativeToJava::LoadRawBufferFromAssets(JNIEnv *env, std::string filename)
{
    std::vector<unsigned char> assetData;

    jclass clazz = env->GetObjectClass(nativeToJava);
    jmethodID methodId =
        env->GetMethodID(
            clazz, "LoadRawBufferFromAssets",
            "(Ljava/lang/String;)[B");

    jstring javaString = env->NewStringUTF(std::string(filename).c_str());

    jbyteArray javabytearray =
        (jbyteArray)env->CallObjectMethod(nativeToJava, methodId, javaString);

    if(javabytearray == NULL)
    {
        LogError("File not found: " + filename);
        throw std::runtime_error("File not found: " + filename);
    }

    size_t length = env->GetArrayLength(javabytearray);
    assetData.resize(length);

    jbyte *javaBytes = env->GetByteArrayElements(javabytearray, nullptr);
    for (int i = 0; i < length; ++i)
    {
        assetData[i] = javaBytes[i];
    }

    env->ReleaseByteArrayElements(javabytearray, javaBytes, 0);

    return assetData;
}

void NativeToJava::Release(JNIEnv *env)
{
    //delete global reference if we dont want to use it anymore.
    env->DeleteGlobalRef(nativeToJava);
}

std::string NativeToJava::GetExternalStorageFolder(JNIEnv *env) {
    jclass clazz = env->GetObjectClass(nativeToJava);
    jmethodID methodId = env->GetMethodID(
        clazz,
        "GetExternalStorageFolder",
        "()Ljava/lang/String;"
    );

    jstring jresult = (jstring)env->CallObjectMethod(nativeToJava, methodId);
    auto s = env->GetStringUTFChars(jresult, nullptr);
    std::string result{s};
    env->ReleaseStringUTFChars(jresult, s);
    return result;
}
