//
// Created by Benni on 05.12.2016.
//

#ifndef GEARVRAPP_TESTSCENE_H
#define GEARVRAPP_TESTSCENE_H

#include <memory>

#include <lo/lo.h>
#include <lo/lo_cpp.h>

#ifdef ARMEABI_V7A
#include <VrApi/VrApi_Types.h>
#endif

#include "Framework/Scene.h"
#include "Framework/UserKinematic.h"

#include "Rendering/ModelRenderer.h"
#include "Rendering/BoundaryRenderer.h"

#include "Audio/ViewerUDPStreamer.h"

#include "Experiment/AcousticThresholdExperiment.h"

#include "DataCaptureManager.h"
#include "Rendering/SplatRenderer.h"


#include "PostProcessing.h"

class VrapiRenderInterface;

struct DebugControlMessage
{
    float adjustableEyeOffset;
    float rotationCenterOffset;
};

class TestScene : public Scene
{
public:
    TestScene();

    void Initialize() override;
    void Load() override;
    void Update(double frameTime) override;
    void Draw(double frameTime, GraphicsContext *context, int eyeIndex) override;
    void Unload() override;

    void ReceivePackage(JNIEnv *env, void *buffer, int length) override;

private:
    void loadModels();

    void resetToGlobalTrackerPosition(
        VrapiRenderInterface *vrapiRenderInterface);
    void computeKinematic(double frameTime);

    ModelRenderer modelRenderer;
//    SplatRenderer splatRenderer;
    BoundaryRenderer boundaryRenderer;

    glm::mat4 lastRecenterPose = glm::mat4(1.0f);
    bool lastIsInitialized = false;
    bool lastFrameButtonPressed = false;

    glm::mat4 additionalSceneRotation = glm::mat4(1.0f);

    unsigned long currentFrame = 0;
    unsigned long localFrameTimeStamp = 0;
    unsigned long lastFrameNr = 0; //Tracking system frame
    bool hasCompleteBody = false;

    glm::vec3 currentTrackingPosition = glm::vec3(0, 0, 0);
    glm::mat4 currentTrackingOrientation = glm::mat4(1.0f);

    float totalRealRotation = 0.f;
    float totalVirtualRotation = 0.f;
    float totalWalkedDistance = 0.f;

    glm::mat4 torsoTransforms[2];

    // glm::vec3 virtualPosition = glm::vec3(0,0,0);

    UserKinematic userKinematic;

    DataCaptureManager dataCapture;
    AcousticThresholdExperiment experiment;

    PostProcessing dataCleanTest;

//    ViewerUDPStreamer viewerToBlender;
    ViewerUDPStreamer viewerToOpenAL;
};

#endif //GEARVRAPP_TESTSCENE_H
