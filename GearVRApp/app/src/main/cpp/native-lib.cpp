#include <jni.h>
#include <string>

#include "Framework/Application.h"
#include "Framework/Log.h"
#include "TestScene.h"
#include "Framework/MutexLock.h"

#include <android/native_window_jni.h>

#ifdef ARMEABI_V7A
#include "VrApi.h"
#warning Compiled with VrApi
#endif

Application App;
MutexLock AppLock;

extern "C" jstring Java_graphicsrwth_gearvrapp_NativeInterface_NativeTest(JNIEnv *env/*, jobject*/ /* this */) //static, no "this" object
{
    std::string str = "Native code is working.";
    return env->NewStringUTF(str.c_str());
}

extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_initializeApplication(JNIEnv *env, jclass jclazz, jobject activity, jobject nativeTojava)
{
    AppLock.Lock();
    Log("Initialize Application.");
    App.SetCurrentEnv(env);
    App.InitializeAndroid(new NativeToJava(env, nativeTojava), activity);
    App.Initialize(new TestScene());
    AppLock.Unlock();
}

extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_loadApplication(JNIEnv *env, jclass jclazz, jobject surface)
{
    AppLock.Lock();
    Log("Load Application.");
    ANativeWindow *newNativeWindow = ANativeWindow_fromSurface(env, surface);
    App.Window = newNativeWindow;
    int width = ANativeWindow_getWidth(newNativeWindow);
    int height = ANativeWindow_getHeight(newNativeWindow);

    App.SetCurrentEnv(env);
    App.Load();
    AppLock.Unlock();
}
extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_ApplicationAllowVrMode(JNIEnv *env, jclass jclazz, jboolean value)
{
    AppLock.Lock();
    App.AllowVrMode(value);
    AppLock.Unlock();
}

extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_renderThreadFrame(JNIEnv *env, jclass jclazz, jdouble jframeTime)
{
    if(App.IsLoaded())
    {
        AppLock.Lock();
        App.SetCurrentEnv(env);
        if(App.Window != nullptr)
        {
            double frameTime = (double)jframeTime;
            App.Frame(frameTime);
        }
        AppLock.Unlock();
    }
}

extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_unloadApplication(JNIEnv *env, jclass jclazz)
{
    AppLock.Lock();
    Log("Unload Application.");
    App.Unload();
    AppLock.Unlock();
}

extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_destroyApplication(JNIEnv *env, jclass jclazz)
{
    AppLock.Lock();
    Log("Destroy Application.");
    App.SetCurrentEnv(env);
    App.Destroy();
    AppLock.Unlock();
}


extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_udpPackage(JNIEnv *env, jclass jclazz, jbyteArray data, jint length)
{
    //AppLock.Lock();
    //App.SetCurrentEnv(env);
    //Log("Received Package in native code.");

    jbyte* bufferPtr = env->GetByteArrayElements(data, NULL);
    jsize lengthOfArray = env->GetArrayLength(data);

    App.GetCurrentScene()->ReceivePackage(env, bufferPtr, lengthOfArray);

    env->ReleaseByteArrayElements(data, bufferPtr, 0);

    //AppLock.Unlock();
}

#define TOUCHDOWN 0
#define TOUCHUP 1

extern "C" void Java_graphicsrwth_gearvrapp_NativeInterface_touchEvent(JNIEnv *env, jclass jclazz, jint action, jfloat x, jfloat y)
{
    //AppLock.Lock();
    //App.SetCurrentEnv(env);
    Log("TouchEvent.");

    if(action == TOUCHDOWN)
    {
        App.TouchCounter++;
    }
    else if(action == TOUCHUP)
    {
        App.TouchCounter--;
    }

    //AppLock.Unlock();
}
