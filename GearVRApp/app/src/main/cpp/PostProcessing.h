//
// Created by kscharf on 5/23/17.
//

#ifndef GEARVRAPP_DATACLEANER_H
#define GEARVRAPP_DATACLEANER_H

#include <glm/detail/type_mat4x4.hpp>
#include <string>

class PostProcessing {

public:
    std::string GetCleanedStringFromDataLine(std::string line);

private:
    unsigned int frames = 0;

    //Update if there is new data:
    glm::mat4 realHeadTransform;
    glm::mat4 virtualHeadTransform;
    glm::mat4 realFrontTransform;
    glm::mat4 realBackTransform;
    glm::mat4 realTorsoTransform;

    glm::vec3 offsetTorsoFrontToCenter;
    glm::vec3 offsetTorsoBackToCenter;

    //Update each frame:
    glm::mat4 realHeadTransformLast;
    glm::mat4 realHeadTransformLastLast;
    glm::mat4 virtualHeadTransformLast;
    glm::mat4 realFrontTransformLast;
    glm::mat4 realBackTransformLast;
    glm::mat4 realTorsoTransformLast;
    bool lastTorsoTransformValid = false;
    bool lastHeadTransformValid = false;

    float totalDistanceFixed = 0.f;
    float totalAngleYRealRadFixed = 0.f;
    float totalAngleYVirtualRadFixed = 0.f;
    float lastReadTotalDistance = 0.f;
    float lastReadTotalAngleYRealRad = 0.f;
    float lastReadTotalAngleYVirtualRad = 0.f;

    bool hasValidHeadTransform;
    bool hasValidTorsoTransform;
    bool hasValidTorsoOffsets = false;

    bool hasBodyTorsoFront;
    bool hasBodyTorsoBack;

    float relativeAngleYHeadTorsoDeg;
    float angleYHeadCurrentFrameDeg;
    float angleXHeadCurrentFrameDeg;
    float angleYTorsoCurrentFrameDeg;
    float accumulatedAngleYHeadDeg = 0.0f;
    float accumulatedAbsAngleYHeadDeg = 0.0f;
    float currentOrientationAngleXHeadDeg;
    float currentOrientationAngleYHeadDeg;
    float currentOrientationAngleYHeadVirtualDeg;
    float accumulatedAngleYTorsoDeg = 0.0f;
    float accumulatedAbsAngleYTorsoDeg = 0.0f;
    float accumulatedAbsAngleXHeadDeg = 0.0f;

    float angleYHeadCurrentFrameVirtualDeg;
    //float angleYTorsoCurrentFrameVirtualDeg;
    float accumulatedAngleYHeadVirtualDeg = 0.0f;
    float accumulatedAbsAngleYHeadVirtualDeg = 0.0f;
    //float accumulatedAngleYTorsoVirtualDeg = 0.0f;

    //std::string torsoTransformString;
    std::string relativeAngleYString;
    std::string positionHeadRealString;
    std::string positionHeadVirtualString;
    std::string currentOrientationAngleYTorsoString;


    void checkForValidTransforms();

    void computePositionStrings();
    void computeRelativeAngleYHeadToTorsoString();
    void computeAndAddAnglesHead(float currentGain);
    void computeAndAddAngleYTorso(); //Call AFTER torso transform is calculated
    //void computeAndAddAngleXHead();

    void fixAccumulatedValue(float *lastValue, float *currentValue, float *currentSum);

    std::string getMatlabString(glm::mat4 v);
    std::string getMatlabStringPositionXnegativeZWithOffsetCorrection(glm::mat4 v);

    void computeTorsoTransform();

    void handleBodyFront(glm::vec3 & realTorsoPosition,
                                      glm::mat4 & realTorsoOrientation);

    void handleBodyBack(glm::vec3 & realTorsoPosition,
                                     glm::mat4 & realTorsoOrientation);

    void handleBodyBoth(glm::vec3 & realTorsoPosition,
                                     glm::mat4 & realTorsoOrientation);

    float angleAroundGlobalY(glm::mat3 transform);
    float angleAroundLocalX(glm::mat3 transform);

    glm::mat4 correctBodyTransform(glm::mat4 transform);
    glm::mat4 cleanMatrix(glm::mat4 matrix);

    glm::mat4 ReadMat4(std::string strMatrix);
    float ReadFloat(std::string strFloat);

    std::string R2S(float f);

};


#endif //GEARVRAPP_DATACLEANER_H
