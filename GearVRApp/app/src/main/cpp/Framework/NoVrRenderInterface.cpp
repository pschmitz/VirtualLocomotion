//
// Created by Benni on 14.12.2016.
//

#include "NoVrRenderInterface.h"
#include "Application.h"

void NoVrRenderInterface::Initialize(Application *app)
{
    application = app;
}

void NoVrRenderInterface::Load(EglContext *egl)
{
    this->egl = egl;
}

void NoVrRenderInterface::EnterVrMode()
{
    Log("ENTER FAKE VR MODE");
}

void NoVrRenderInterface::LeaveVrMode()
{
    Log("LEAVE FAKE VR MODE");
}

void NoVrRenderInterface::Unload()
{

}

void NoVrRenderInterface::Present()
{
    egl->SwapBuffers();
}

void NoVrRenderInterface::Destroy()
{
    egl = nullptr;
}

void NoVrRenderInterface::BeginEye(GraphicsContext *context, int eyeIndex)
{
    context->PushFramebuffer(0);

    Viewport viewport;

    if(eyeIndex == 0)
    {
        viewport.X = 0;
        viewport.Y = 0;
        viewport.Width = application->GetWidth() / 2;
        viewport.Height = application->GetHeight();
    }
    else
    {
        viewport.X = application->GetWidth() / 2;
        viewport.Y = 0;
        viewport.Width = application->GetWidth() / 2;
        viewport.Height = application->GetHeight();
    }

    context->PushViewport(viewport);
}

void NoVrRenderInterface::EndEye(GraphicsContext *context, int eyeIndex)
{
    context->PopFramebuffer();
    context->PopViewport();
}

glm::mat4 NoVrRenderInterface::GetProjectionMatrix(int eyeIndex)
{
    return glm::perspectiveFov(glm::radians(90.0f), (float)application->GetWidth()/2, (float)application->GetHeight(), 1.0f, 100.0f);
}
