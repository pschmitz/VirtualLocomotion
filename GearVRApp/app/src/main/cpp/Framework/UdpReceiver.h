#pragma once
#include <cstddef>
#include <cstdint>
#include <array>
#include <thread>
#include <linux/in.h>

#include "Log.h"

class UdpReceiver {
public:
    // 2312 == MTU of WiFi == biggest UDP packet receivable
    static constexpr size_t buffer_length = 2312;
    using Handler = std::function<void(uint8_t*,size_t)>;

    UdpReceiver(Handler const& handler, uint16_t port);

    ~UdpReceiver(){
        // TODO make the listen thread close gracefully
        // for now this destructor will crash the program
    }

protected:
    Handler mHandler;
    int mSocket;
    sockaddr_in mAddress = {};
    std::thread mListenThread;
};
