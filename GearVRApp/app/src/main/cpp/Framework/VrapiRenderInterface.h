//
// Created by Benni on 14.12.2016.
//

#ifndef GEARVRAPP_VRAPIRENDERINTERFACE_H
#define GEARVRAPP_VRAPIRENDERINTERFACE_H

#include "IVrRenderInterface.h"
#include "EglHelper.h"
#include "Application.h"
#include "Math.h"

#ifdef VRAPI
#include <VrApi/VrApi.h>
#include <VrApi/VrApi_Types.h>
#include <VrApi/VrApi_Helpers.h>
#include <VrApi_Types.h>

glm::vec3 toGlm(const ovrVector3f vec);
glm::quat toGlm(const ovrQuatf quat);
glm::mat4 toGlm(const ovrMatrix4f matrix);

class VrapiRenderInterface : public IVrRenderInterface
{
private:
    Application *app;
    EglContext *egl;
    int frameIndex = 1;

    ovrJava java;
    ovrMobile *ovr = nullptr;

    ovrTextureSwapChain * colorTextureSwapChain[VRAPI_FRAME_LAYER_EYE_MAX];
    ovrTextureSwapChain * depthTextureSwapChain[VRAPI_FRAME_LAYER_EYE_MAX];
    ovrMatrix4f eyeProjectionMatrix;

    GLuint *eyeDepthBuffer;
    GLuint *eyeFramebuffers;
    int maxSwapChainsPerEye = 0;

    ovrFrameParms frameParms;
    ovrTracking tracking;
    ovrHeadModelParms headModelParms;

    float rotationGain = 1.0;


public:
    void Initialize(Application *app) override;
    void Load(EglContext *egl) override;
    void EnterVrMode() override;
    void LeaveVrMode() override;
    void BeginEye(GraphicsContext *context, int eyeIndex) override;
    void EndEye(GraphicsContext *context, int eyeIndex) override;
    void Unload() override;
    void Present() override;
    void Destroy() override;

    float getFovy() override;
    float getNear() override;
    void setGain(float val) { rotationGain = val; }

    glm::uvec2 getViewport() override;

    void RecenterPose();

    glm::mat4 GetProjectionMatrix(int eyeIndex) override;
    ovrTracking *GetOvrTracking();
};
#endif


#endif //GEARVRAPP_VRAPIRENDERINTERFACE_H
