#include <unistd.h>
#include "UdpReceiver.h"
#include <sys/socket.h>
#include <endian.h>

UdpReceiver::UdpReceiver(Handler const& handler, uint16_t port) : mHandler(handler) {
    mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (mSocket == -1) {
        LogError("Could not create socket!");
        return;
    }
    mAddress.sin_family = AF_INET;
    mAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    mAddress.sin_port = htons(port);

    if (bind(mSocket, (struct sockaddr*)(&mAddress), sizeof(mAddress)) < 0) {
        LogError("Could not bind UDP Receiver to address.");
        return;
    }

    mListenThread = std::thread([this]() {
        std::array<uint8_t, buffer_length> buffer{};

        // intentionally infinite loop => disable infinite-loop detection
#       pragma clang diagnostic push
#       pragma clang diagnostic ignored "-Wmissing-noreturn"
        for (;;) {
            //auto bytes_received = recvfrom(mSocket, buffer.data(), sizeof(buffer), 0, 0, 0);
            auto bytes_received = read(mSocket, buffer.data(), sizeof(buffer));
            if (bytes_received > 0) {
                mHandler(buffer.data(), bytes_received);
            }
        }
#       pragma clang diagnostic pop
    });
}