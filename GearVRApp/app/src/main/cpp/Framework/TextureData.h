//
// Created by Benni on 22.12.2016.
//

#ifndef GEARVRAPP_TEXTUREDATA_H
#define GEARVRAPP_TEXTUREDATA_H

#define byte unsigned char

class TextureData
{
public:
    byte *Data;
    int DataLength;
    int Width;
    int Height;

    void Release()
    {
        delete Data;
        Data = nullptr;
        DataLength = 0;
        Width = 0;
        Height = 0;
    }
};


#endif //GEARVRAPP_TEXTUREDATA_H
