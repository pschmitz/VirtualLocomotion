//
// Created by Benni on 08.12.2016.
//

#include "EglHelper.h"
#include "Log.h"
#include <android/native_window.h>

#include <string>
using std::string;

void EglHelper::CreateContextVrapi(EglContext *egl)
{
    EglContext *shareEgl = nullptr;

    if ( egl->Display != 0 )
    {
        return;
    }

    egl->Display = eglGetDisplay( EGL_DEFAULT_DISPLAY );
    Log( "        eglInitialize( Display, &MajorVersion, &MinorVersion )" );
    eglInitialize( egl->Display, &egl->MajorVersion, &egl->MinorVersion );
    // Do NOT use eglChooseConfig, because the Android EGL code pushes in multisample
    // flags in eglChooseConfig if the user has selected the "force 4x MSAA" option in
    // settings, and that is completely wasted for our warp target.
    const int MAX_CONFIGS = 1024;
    EGLConfig configs[MAX_CONFIGS];
    EGLint numConfigs = 0;
    if ( eglGetConfigs( egl->Display, configs, MAX_CONFIGS, &numConfigs ) == EGL_FALSE )
    {
        LogError("EGL", "Egl failed");
        //LogError( "        eglGetConfigs() failed: %s" + EglErrorString( eglGetError() ) );
        return;
    }

    const EGLint configAttribs[] =
            {
                    EGL_RED_SIZE,		8,
                    EGL_GREEN_SIZE,		8,
                    EGL_BLUE_SIZE,		8,
                    EGL_ALPHA_SIZE,		8, // need alpha for the multi-pass timewarp compositor
                    EGL_DEPTH_SIZE,		0,
                    EGL_STENCIL_SIZE,	0,
                    EGL_SAMPLES,		0,
                    EGL_NONE
            };
    egl->Config = 0;
    for ( int i = 0; i < numConfigs; i++ )
    {
        EGLint value = 0;

#ifdef ARMEABI_V7A
        //Check for OpenGLES 3 (skip for emulator)
        eglGetConfigAttrib( egl->Display, configs[i], EGL_RENDERABLE_TYPE, &value );
        if ( ( value & EGL_OPENGL_ES3_BIT_KHR ) != EGL_OPENGL_ES3_BIT_KHR )
        {
            continue;
        }
#endif

        // The pbuffer config also needs to be compatible with normal window rendering
        // so it can share textures with the window context.
        eglGetConfigAttrib( egl->Display, configs[i], EGL_SURFACE_TYPE, &value );
        if ( ( value & ( EGL_WINDOW_BIT | EGL_PBUFFER_BIT ) ) != ( EGL_WINDOW_BIT | EGL_PBUFFER_BIT ) )
        {
            continue;
        }

        int	j = 0;
        for ( ; configAttribs[j] != EGL_NONE; j += 2 )
        {
            eglGetConfigAttrib( egl->Display, configs[i], configAttribs[j], &value );
            if ( value != configAttribs[j + 1] )
            {
                break;
            }
        }
        if ( configAttribs[j] == EGL_NONE )
        {
            egl->Config = configs[i];
            break;
        }
    }
    if ( egl->Config == 0 )
    {
        LogError("EGL", "Egl failed");
        //LogError( "        eglChooseConfig() failed: %s", EglErrorString( eglGetError() ) );
        return;
    }
    EGLint contextAttribs[] =
            {
                    EGL_CONTEXT_CLIENT_VERSION, 3,
                    0x30FB, 1, // = EGL_CONTEXT_MINOR_VERSION, 1
                    0x30FC, 0x00000001, // = EGL_CONTEXT_FLAGS_KHR, EGL_CONTEXT_OPENGL_DEBUG_BIT_KHR
                    EGL_NONE
            };
    Log("EGL", "        Context = eglCreateContext( Display, Config, EGL_NO_CONTEXT, contextAttribs )" );
    egl->Context = eglCreateContext( egl->Display, egl->Config, ( shareEgl != NULL ) ? shareEgl->Context : EGL_NO_CONTEXT, contextAttribs );

    /*EGLint err = eglGetError();
    if (err != EGL_SUCCESS)
    {
        LogError("EGL", to_string(err));
        return;
    }*/

    if ( egl->Context == EGL_NO_CONTEXT )
    {
        LogError("EGL", "Egl failed");
        //LogError( "        eglCreateContext() failed: %s", EglErrorString( eglGetError() ) );
        return;
    }
    const EGLint surfaceAttribs[] =
            {
                    EGL_WIDTH, 16,
                    EGL_HEIGHT, 16,
                    EGL_NONE
            };

    EGLSurface tinySurface;
    Log("EGL", "        TinySurface = eglCreatePbufferSurface( Display, Config, surfaceAttribs )" );
    tinySurface = eglCreatePbufferSurface( egl->Display, egl->Config, surfaceAttribs );
    if ( tinySurface == EGL_NO_SURFACE )
    {
        LogError("EGL", "Egl failed");
        //LogError( "        eglCreatePbufferSurface() failed: %s", EglErrorString( eglGetError() ) );
        eglDestroyContext( egl->Display, egl->Context );
        egl->Context = EGL_NO_CONTEXT;
        return;
    }
    Log("EGL", "        eglMakeCurrent( Display, TinySurface, TinySurface, Context )" );
    if ( eglMakeCurrent( egl->Display, tinySurface, tinySurface, egl->Context ) == EGL_FALSE )
    {
        LogError("EGL", "Egl failed");
        //LogError( "        eglMakeCurrent() failed: %s", EglErrorString( eglGetError() ) );
        eglDestroySurface( egl->Display, tinySurface );
        eglDestroyContext( egl->Display, egl->Context );
        egl->Context = EGL_NO_CONTEXT;
        return;
    }
    egl->MainSurface = tinySurface;
}


void EglHelper::CreateContext(EglContext *egl, ANativeWindow *window, EGLint &width, EGLint &height)
{
    const EGLint attribs[] = {
            EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
            EGL_BLUE_SIZE, 8,
            EGL_GREEN_SIZE, 8,
            EGL_RED_SIZE, 8,
            EGL_NONE
    };
    EGLDisplay display;
    EGLConfig config;
    EGLint numConfigs;
    EGLint format;
    EGLSurface surface;
    EGLContext context;

    Log("Egl initialization.");

    if ((display = eglGetDisplay(EGL_DEFAULT_DISPLAY)) == EGL_NO_DISPLAY)
    {
        LogError("EGL", "eglGetDisplay() returned error " + to_string(eglGetError()));
    }
    if (!eglInitialize(display, &egl->MajorVersion, &egl->MinorVersion))
    {
        LogError("EGL", "eglInitialize() returned error " + to_string(eglGetError()));
    }

    if (!eglChooseConfig(display, attribs, &config, 1, &numConfigs))
    {
        LogError("EGL", "eglChooseConfig() returned error " + to_string(eglGetError()));
        //destroy();
    }

    if (!eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format))
    {
        LogError("EGL", "eglGetConfigAttrib() returned error " + to_string(eglGetError()));
        //destroy();
    }

    ANativeWindow_setBuffersGeometry(window, 0, 0, format);

    if (!(surface = eglCreateWindowSurface(display, config, window, 0)))
    {
        LogError("EGL", "eglCreateWindowSurface() returned error " + to_string(eglGetError()));
        //destroy();
    }

    EGLint contextAttribs[] =
            {
                    EGL_CONTEXT_CLIENT_VERSION, 2,
                    EGL_NONE
            };
    if (!(context = eglCreateContext(display, config, 0, contextAttribs)))
    {
        LogError("EGL", "eglCreateContext() returned error " + to_string(eglGetError()));
        //destroy();
    }

    if (!eglMakeCurrent(display, surface, surface, context))
    {
        LogError("EGL", "eglMakeCurrent() returned error " + to_string(eglGetError()));
        //destroy();
    }

    if (!eglQuerySurface(display, surface, EGL_WIDTH, &width) ||
        !eglQuerySurface(display, surface, EGL_HEIGHT, &height))
    {
        LogError("EGL", "eglQuerySurface() returned error " + to_string(eglGetError()));
        //destroy();
    }

    egl->Display = display;
    egl->MainSurface = surface;
    egl->Context = context;
    egl->Config = config;
    //egl->TinySurface;
}