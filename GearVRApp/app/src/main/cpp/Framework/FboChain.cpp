#include "FboChain.h"

#include <glow/objects/Texture2D.hh>
#include <glow/objects/Framebuffer.hh>

void FboChain::resize(glm::uvec2 size) {
    if (glm::uvec2(mTargets[0]->getDimensions()) == size) return;

    for (auto& t : mTargets)
    {
        t->bind().resize(size.x, size.y);
    }
}

FboChain::FboChain(size_t count, std::vector<Attachment> const& formats) {
    mFbos.reserve(count);
    mTargets.reserve(count * formats.size());
    mStride = formats.size();

    for (size_t i = 0ull; i < count; i++)
    {
        std::vector<glow::Framebuffer::Attachment> colors;
        glow::SharedTexture2D depth;
        for (auto form : formats)
        {
            switch (form.format) {
                case GL_DEPTH_COMPONENT:
                case GL_DEPTH_COMPONENT24:
                case GL_DEPTH_COMPONENT16:
                case GL_DEPTH_COMPONENT32F:
                    if (depth) glow::warning() << "You can only have one depth buffer per FBO! Overwriting old one.";
                    depth = glow::Texture2D::create(1,1,form.format);
                    depth->bind().setMinFilter(GL_LINEAR);
                    mTargets.push_back(depth);
                    break;
                default:
                    auto color = glow::Texture2D::create(1,1,form.format);
                    color->bind().setMinFilter(GL_LINEAR);
                    colors.push_back({form.name,color});
                    mTargets.push_back(color);
                    break;
            }
        }
        mFbos.push_back(glow::Framebuffer::create(colors, depth));
    }
}