//
// Created by Benni on 05.12.2016.
//

#ifndef GEARVRAPP_APP_H
#define GEARVRAPP_APP_H

#include <chrono>
#include "../Android/NativeToJava.h"
#include "EglHelper.h"
#include "OpenGL.h"
#include "IContentLoader.h"
#include "IVrRenderInterface.h"
#include "UdpReceiver.h"
#include <memory>

class Scene;

class Application
{
public:
    void Initialize(Scene *scene);
    void Load();
    void Unload();
    void Frame(double frameTime);
    void Destroy();

    void AllowVrMode(bool value);

    bool IsLoaded() { return loaded; }

    int GetWidth() { return width; }
    int GetHeight() { return height; }

    IContentLoader *GetContentLoader() { return contentLoader; }
    IVrRenderInterface *GetVrRenderInterface() { return vrRenderInterface; }

    GraphicsContext *GetGraphicsContext() { return &graphicsContext; }
    Scene *GetCurrentScene() { return currentScene; }

private:
    bool allowVrMode = false;
    bool isInVrMode = false;

    IVrRenderInterface *vrRenderInterface;
    IContentLoader *contentLoader;
    bool loaded = false;

    int width;
    int height;
    Scene *currentScene;
    EglContext egl;
    const GLubyte *glVersion;
    GraphicsContext graphicsContext;

    void initializeEgl();
    void initializeOpenGL();

    using FpsClock = std::chrono::high_resolution_clock;
    FpsClock::time_point fpsCounterStartpoint;
    size_t accumulatedFrames = 0;

#ifdef ANDROID
public:
    ANativeWindow *Window = nullptr;
    int TouchCounter = 0;
    bool IsTouchPressed() { return TouchCounter > 0; }

    void InitializeAndroid(NativeToJava *nativeToJava, jobject activity);
    NativeToJava* GetNativeToJava() { return nativeToJava; }
    void SetCurrentEnv(JNIEnv *env);
    JNIEnv *GetCurrentEnv() { return currentEnv; }
    jobject GetMainActivity() { return mainactivity; }

private:
    jobject mainactivity;
    NativeToJava *nativeToJava;
    JNIEnv *currentEnv;
    void destroyAndroid();

    std::unique_ptr<UdpReceiver> trackingReceiver;
#endif
};


#endif //GEARVRAPP_APP_H
