#include "ObjLoader.h"

#include <sstream>
#include <exception>

#include "Log.h"
#include <stdlib.h>

void ObjLoader::loadObjFile(std::string &objFileContent,
                            std::vector<glm::vec3> &positions,
                            std::vector<glm::vec3> &normals,
                            std::vector<glm::vec2> &texcoords,
                            std::vector<unsigned int> &indices)
{

    if (objFileContent.length() == 0)
    {
        LogError("Content is empty");
        throw std::exception();
    }

    this->reset();

    std::istringstream f(objFileContent);

    std::string line;
    while (getline(f, line))
    {
        try
        {
            this->handleLine(line);
        }
        catch (std::exception e)
        {
            LogError(std::string(e.what()));
        }
    }

    for (long i = 0; i < this->vertexCount; i++)
    {
        indices.push_back((unsigned int)i);
        positions.push_back(glm::vec3(this->positions[this->positionIndices[i]-1]));
        normals.push_back(glm::vec3(this->normals[this->normalIndices[i]-1]));
        texcoords.push_back(glm::vec2(this->texCoords[this->texCoordIndices[i]-1]));
    }
}

#pragma mark - private helper functions

// process one line
void ObjLoader::handleLine(std::string &line)
{
    // find text until the first space
    size_t index = line.find(" ");
    if (index != std::string::npos) {
        std::string symbol = line.substr(0, index);
        std::string restOfLine = line.substr(index + 1);

        if (symbol == "o") {
            // Maybe use this later?
            // this->currentObjectName = restOfLine;
        } else if (symbol == "v") {
            this->addPosition(restOfLine);
        } else if (symbol == "vn") {
            this->addNormal(restOfLine);
        } else if (symbol == "vt") {
            this->addTexCoord(restOfLine);
        } else if (symbol == "f") {
            this->addIndices(restOfLine);
        } else if (symbol == "#" || symbol == "s" || symbol == "g" || symbol == "usemtl" || symbol == "mtllib") {
            // Ignore
        } else
        {
            LogError(("Unrecognized symbol in obj file:" + symbol).c_str());
        }
    }
}

void ObjLoader::addPosition(std::string &line) {
    float *values = this->stringToFloatArray(line, 3);
    glm::vec3 vertex = {values[0], values[1], values[2]};
    this->positions.push_back(vertex);
    delete values;
}

void ObjLoader::addNormal(std::string &line) {
    float *values = this->stringToFloatArray(line, 3);
    glm::vec3 normal = {values[0], values[1], values[2]};
    this->normals.push_back(normal);
    delete values;
}

void ObjLoader::addTexCoord(std::string &line) {
    float *values = this->stringToFloatArray(line, 2);
    glm::vec2 texCoord = {values[0], 1.0f-values[1]};
    this->texCoords.push_back(texCoord);

    delete values;
}

void ObjLoader::addIndices(std::string &line) {
    int *values = this->faceIndexStringToIndexArray(line); // will be int[9]
    this->positionIndices.push_back(values[0]);
    this->texCoordIndices.push_back(values[1]);
    this->normalIndices.push_back(values[2]);

    this->positionIndices.push_back(values[3]);
    this->texCoordIndices.push_back(values[4]);
    this->normalIndices.push_back(values[5]);

    this->positionIndices.push_back(values[6]);
    this->texCoordIndices.push_back(values[7]);
    this->normalIndices.push_back(values[8]);

    this->vertexCount += 3;

    delete values;
}

const int NPOS = -1;
// convert string of format 1/2/3 4/5/6 7/8/9 to int array: {1,2,...,9}
int* ObjLoader::faceIndexStringToIndexArray(std::string str)
{
    size_t index = str.find(" ");

    int *result = (int*) malloc(sizeof(int) * 9);

    uint8_t i = 0;

    int vertexCount = 0;
    bool stopSearching = false;
    // As long as there are spaces
    while (!stopSearching) {
        // Add float value to result value
        std::string vertexString = str.substr(0, index);
        if (vertexString.length() > 0) {

            // Now split each part by slashes
            size_t index2 = vertexString.find("/");
            int vertexPartCount = 0;
            bool stopSearching2 = false;

            while (!stopSearching2) {
                std::string vertexPartString = vertexString.substr(0, index2);
                if (vertexString.length() > 0) {
                    result[i++] = atoi(vertexPartString.c_str());
                    vertexPartCount++;
                }

                if (index2 == NPOS) {
                    stopSearching2 = true;
                } else {
                    vertexString = vertexString.substr(index2 + 1);
                    index2 = vertexString.find("/");
                }
            }

            if (vertexPartCount != 3)
            {
                LogError("Expected 3 components per vertex, but found " + to_string(vertexPartCount) + ". Please include position, normal and texture coordinates.");
                throw std::exception();
            }

            vertexCount++;
        }

        if (index == NPOS) {
            stopSearching = true;
        } else {
            str = str.substr(index + 1);
            index = str.find(" ");
        }
    }

    if (vertexCount != 3)
    {
        LogError("Expected 3 vertices per face, but found " + to_string(vertexCount) + ". Please check \"triangulate faces\" when export.");
        throw std::exception();
    }

    return result;
}


float* ObjLoader::stringToFloatArray(std::string str, uint8_t assertSize) {
    size_t index = str.find(" ");
    float *result = (float*)malloc(sizeof(float) * assertSize);
    uint8_t i = 0;

    bool stopSearching = false;
    // As long as there are spaces
    while (!stopSearching) {
        // Add float value to result value
        std::string floatString = str.substr(0, index);
        if (floatString.length() > 0) {
            result[i++] = atof(floatString.c_str());
        }

        if (index == NPOS || i >= assertSize) {
            stopSearching = true;
        } else {
            str = str.substr(index + 1);
            index = str.find(" ");
        }
    }

    if (assertSize != i) {
        LogError("Expected " + to_string(assertSize) + " indices but found " + to_string(i) + ".");
        throw std::exception();
    }

    return result;
}

void ObjLoader::reset()
{

    this->positions.clear();
    this->normals.clear();
    this->texCoords.clear();

    this->vertexCount = 0;
}
