//
// Created by Benni on 14.12.2016.
//

#ifdef VRAPI
#include <exception>
#include <stddef.h>
#include <VrApi/VrApi.h>
#include <VrApi/VrApi_Types.h>
#include <VrApi/VrApi_Helpers.h>
#include <VrApi_Types.h>
#include <glm/common.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glow/common/log.hh>
#include "VrapiRenderInterface.h"
#include "glow/extensions.hh"
using std::exception;

#include "Application.h"

glm::vec3 toGlm(const ovrVector3f vec)
{
    return glm::vec3(vec.x, vec.y, vec.z);
}

glm::quat toGlm(const ovrQuatf quat)
{
    glm::quat quaternion;
    quaternion.x = quat.x;
    quaternion.y = quat.y;
    quaternion.z = quat.z;
    quaternion.w = quat.w;
    return quaternion;
}

glm::mat4 toGlm(const ovrMatrix4f matrix)
{
    glm::mat4 glmMatrix;

    glmMatrix[0][0] = matrix.M[0][0];
    glmMatrix[0][1] = matrix.M[1][0];
    glmMatrix[0][2] = matrix.M[2][0];
    glmMatrix[0][3] = matrix.M[3][0];

    glmMatrix[1][0] = matrix.M[0][1];
    glmMatrix[1][1] = matrix.M[1][1];
    glmMatrix[1][2] = matrix.M[2][1];
    glmMatrix[1][3] = matrix.M[3][1];

    glmMatrix[2][0] = matrix.M[0][2];
    glmMatrix[2][1] = matrix.M[1][2];
    glmMatrix[2][2] = matrix.M[2][2];
    glmMatrix[2][3] = matrix.M[3][2];

    glmMatrix[3][0] = matrix.M[0][3];
    glmMatrix[3][1] = matrix.M[1][3];
    glmMatrix[3][2] = matrix.M[2][3];
    glmMatrix[3][3] = matrix.M[3][3];

    return glmMatrix;
}

void VrapiRenderInterface::Initialize(Application *app)
{
    this->app = app;
    JNIEnv *env = app->GetCurrentEnv();

    //Initialize ovrJava
    JavaVM *vm;
    env->GetJavaVM(&vm);
    java.Vm = vm;
    java.Env = env;

    //This should not be necessary
    //(*java.Vm)->AttachCurrentThread( java.Vm, &java.Env, NULL );

    java.ActivityObject = app->GetMainActivity();

    const ovrInitParms initParms = vrapi_DefaultInitParms( &java );
    int32_t initResult = vrapi_Initialize( &initParms );
    if ( initResult != VRAPI_INITIALIZE_SUCCESS )
    {
        LogError("VrApi initialization failed.");
        vrapi_Shutdown();
    }
    else
    {
        Log("Vrapi successfully initialized.");
    }
}

void VrapiRenderInterface::Load(EglContext *egl)
{
    int numberOfMultiSamples = 4;
    bool multisampling = true;

    this->egl = egl;
    frameIndex = 1;

    // Get the suggested resolution to create eye texture swap chains.
    const int suggestedEyeTextureWidth = vrapi_GetSystemPropertyInt( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_WIDTH );
    const int suggestedEyeTextureHeight = vrapi_GetSystemPropertyInt( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_HEIGHT );

    // Allocate a texture swap chain for each eye with the application's EGLContext current.
    for ( int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++ )
    {
        colorTextureSwapChain[eye] = vrapi_CreateTextureSwapChain(VRAPI_TEXTURE_TYPE_2D,
                                                                  VRAPI_TEXTURE_FORMAT_8888,
                                                                  suggestedEyeTextureWidth,
                                                                  suggestedEyeTextureHeight,
                                                                  1, true);
    }

    // Get the suggested FOV to setup a projection matrix.
    const float suggestedEyeFovDegreesX = vrapi_GetSystemPropertyFloat( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_FOV_DEGREES_X );
    const float suggestedEyeFovDegreesY = vrapi_GetSystemPropertyFloat( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_FOV_DEGREES_Y );

    // Setup a projection matrix based on the suggested FOV.
    // Note that this is an infinite projection matrix for the best precision.
    eyeProjectionMatrix = ovrMatrix4f_CreateProjectionFov( suggestedEyeFovDegreesX,
                                                                             suggestedEyeFovDegreesY,
                                                                             0.0f, 0.0f, VRAPI_ZNEAR, 0.0f );

    //
    // Create eye framebuffers
    //
    //Find max amount of swapchain buffers
    for (int i = 0; i < VRAPI_FRAME_LAYER_EYE_MAX; ++i)
    {
        int swapChainLength = vrapi_GetTextureSwapChainLength( colorTextureSwapChain[0] );
        maxSwapChainsPerEye = glm::max(maxSwapChainsPerEye, swapChainLength);
    }

    //Allocate buffer array.
    eyeFramebuffers = new GLuint[VRAPI_FRAME_LAYER_EYE_MAX * maxSwapChainsPerEye];
    eyeDepthBuffer = new GLuint[VRAPI_FRAME_LAYER_EYE_MAX * maxSwapChainsPerEye];

    for (int i = 0; i < VRAPI_FRAME_LAYER_EYE_MAX * maxSwapChainsPerEye; ++i)
    {
        eyeFramebuffers[i] = 0;
    }

    //https://www.khronos.org/registry/OpenGL/extensions/EXT/EXT_multisampled_render_to_texture.txt
    //Generate OpenGL framebuffers
    for ( int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++ )
    {
        int swapChainLength = vrapi_GetTextureSwapChainLength( colorTextureSwapChain[eye] );
        for (int swapChainIndex = 0; swapChainIndex < swapChainLength; ++swapChainIndex)
        {
            GLenum error = glGetError();

            //Create depth buffer
            GLuint renderBuffer;
            glGenRenderbuffers(1, &renderBuffer);
            GLenum error0 = glGetError();

            glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);
            GLenum error1 = glGetError();

            if(multisampling)
            {
                glow::ext::glRenderbufferStorageMultisample(GL_RENDERBUFFER, numberOfMultiSamples,
                                                       GL_DEPTH_COMPONENT24, suggestedEyeTextureWidth, suggestedEyeTextureHeight);
            }
            else
            {
                glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, suggestedEyeTextureWidth, suggestedEyeTextureHeight);
            }
            GLenum error2= glGetError();

            glBindRenderbuffer(GL_RENDERBUFFER, 0);
            eyeDepthBuffer[eye * maxSwapChainsPerEye + swapChainIndex] = renderBuffer;

            const unsigned int textureId = vrapi_GetTextureSwapChainHandle( colorTextureSwapChain[eye], swapChainIndex );
            auto depthId = vrapi_GetTextureSwapChainHandle(depthTextureSwapChain[eye], swapChainIndex);
            glGenFramebuffers(1, &eyeFramebuffers[eye * maxSwapChainsPerEye + swapChainIndex]);
            glBindFramebuffer(GL_FRAMEBUFFER, eyeFramebuffers[eye * maxSwapChainsPerEye + swapChainIndex]);

            if(multisampling)
            {
                glow::ext::glFramebufferTexture2DMultisample(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0, numberOfMultiSamples);
            }
            else
            {
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);
            }

            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthId, 0);

            GLenum error3 = glGetError();
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBuffer);
            GLenum error4 = glGetError();

            GLenum result = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            if(result != GL_FRAMEBUFFER_COMPLETE)
            {
                throw exception();
            }

            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
    }

    frameParms = vrapi_DefaultFrameParms( &java, VRAPI_FRAME_INIT_DEFAULT, 0, NULL );
    frameParms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Flags |= VRAPI_FRAME_LAYER_FLAG_CHROMATIC_ABERRATION_CORRECTION;
    //frameParms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Flags |= VRAPI_FRAME_LAYER_FLAG_FIXED_TO_VIEW;
}

void VrapiRenderInterface::EnterVrMode()
{
    java.Env = app->GetCurrentEnv();

    //TODO: where is this coming from
    ANativeWindow * nativeWindow = app->Window;

    Log("ENTER VR MODE");

    // Enter VR mode once the Android Activity is in the resumed state with a valid ANativeWindow.
    ovrModeParms modeParms = vrapi_DefaultModeParms( &java );
    modeParms.Flags |= VRAPI_MODE_FLAG_NATIVE_WINDOW;
    modeParms.Display = (size_t)egl->Display;
    modeParms.WindowSurface = (size_t)nativeWindow;
    modeParms.ShareContext = (size_t)egl->Context;
    ovr = vrapi_EnterVrMode( &modeParms );
}

void VrapiRenderInterface::LeaveVrMode()
{
    Log("LEAVE VR MODE");
    //Must leave VR mode when the Android Activity is paused or the Android Surface is destroyed or changed.
    vrapi_LeaveVrMode(ovr);
}

void VrapiRenderInterface::Unload()
{
    for (int i = 0; i < VRAPI_FRAME_LAYER_EYE_MAX * maxSwapChainsPerEye; ++i)
    {
        if(eyeFramebuffers[i] != 0)
        {
            glDeleteFramebuffers(1, &eyeFramebuffers[i]);
        }
    }

    for (int i = 0; i < VRAPI_FRAME_LAYER_EYE_MAX * maxSwapChainsPerEye; ++i)
    {
        if(eyeDepthBuffer[i] != 0)
        {
            glDeleteRenderbuffers(1, &eyeDepthBuffer[i]);
        }
    }

    delete eyeFramebuffers;
    eyeFramebuffers = nullptr;
    delete eyeDepthBuffer;
    eyeDepthBuffer = nullptr;

    // Destroy the texture swap chains.
    // Make sure to delete the swapchains before the application's EGLContext is destroyed.
    for ( int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++ )
    {
        vrapi_DestroyTextureSwapChain( colorTextureSwapChain[eye] );
    }
}

void VrapiRenderInterface::BeginEye(GraphicsContext *context, int eyeIndex)
{
    const double predictedDisplayTime = vrapi_GetPredictedDisplayTime( ovr, frameIndex );
    const double currentTime = vrapi_GetTimeInSeconds();
    const double offset = predictedDisplayTime - currentTime;

    tracking = vrapi_GetPredictedTracking( ovr, currentTime + rotationGain * offset );
    auto head_model = vrapi_DefaultHeadModelParms();
    tracking = vrapi_ApplyHeadModel(&head_model, &tracking);
    tracking.HeadPose.TimeInSeconds = predictedDisplayTime - rotationGain * offset;

    //glow::error() << predictedDisplayTime - currentTime;
    frameParms.FrameIndex = frameIndex;
    frameParms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eyeIndex].HeadPose = tracking.HeadPose;

    const int suggestedEyeTextureWidth = vrapi_GetSystemPropertyInt( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_WIDTH );
    const int suggestedEyeTextureHeight = vrapi_GetSystemPropertyInt( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_HEIGHT );
    Viewport viewport;
    viewport.X = 0;
    viewport.Y = 0;
    viewport.Width = suggestedEyeTextureWidth;
    viewport.Height = suggestedEyeTextureHeight;

    context->PushViewport(viewport);
    int colorTextureSwapChainIndex = frameIndex % vrapi_GetTextureSwapChainLength( colorTextureSwapChain[eyeIndex] );
    context->PushFramebuffer(eyeFramebuffers[eyeIndex * maxSwapChainsPerEye + colorTextureSwapChainIndex]);
}

void VrapiRenderInterface::EndEye(GraphicsContext *context, int eyeIndex)
{
    int colorTextureSwapChainIndex = frameIndex % vrapi_GetTextureSwapChainLength( colorTextureSwapChain[eyeIndex] );

    frameParms.Layers[0].Textures[eyeIndex].ColorTextureSwapChain = colorTextureSwapChain[eyeIndex];
    frameParms.Layers[0].Textures[eyeIndex].TextureSwapChainIndex = colorTextureSwapChainIndex;
    frameParms.Layers[0].Textures[eyeIndex].TexCoordsFromTanAngles = ovrMatrix4f_TanAngleMatrixFromProjection( &eyeProjectionMatrix );
    frameParms.Layers[0].Textures[eyeIndex].HeadPose = tracking.HeadPose;
    frameParms.Layers[0].Textures[eyeIndex].CompletionFence = 0;//fence;
    context->PopFramebuffer();
    context->PopViewport();
}

void VrapiRenderInterface::Present()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    vrapi_SubmitFrame( ovr, &frameParms );
    frameIndex++;
}

void VrapiRenderInterface::Destroy()
{
    vrapi_Shutdown();
    egl = nullptr;
}

ovrTracking *VrapiRenderInterface::GetOvrTracking()
{
    return &tracking;
}

void VrapiRenderInterface::RecenterPose()
{
    vrapi_RecenterPose(ovr);
}


glm::mat4 VrapiRenderInterface::GetProjectionMatrix(int eyeIndex)
{
    return toGlm(eyeProjectionMatrix);
}

#endif

float VrapiRenderInterface::getFovy() {
    return vrapi_GetSystemPropertyFloat( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_FOV_DEGREES_Y );
}

float VrapiRenderInterface::getNear() {
    return VRAPI_ZNEAR;
}

glm::uvec2 VrapiRenderInterface::getViewport() {
    const int suggestedEyeTextureWidth = vrapi_GetSystemPropertyInt( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_WIDTH );
    const int suggestedEyeTextureHeight = vrapi_GetSystemPropertyInt( &java, VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_HEIGHT );
    return {suggestedEyeTextureWidth, suggestedEyeTextureHeight};
}
