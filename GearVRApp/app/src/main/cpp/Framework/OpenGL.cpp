//
// Created by Benni on 13.12.2016.
//

#include "OpenGL.h"
#include "Log.h"

void glHelperCheckError()
{
    GLenum error = glGetError();
    if(error != GL_NO_ERROR)
    {
        LogError("OpenGL", "glGetError() == " + to_string(error));
    }
}