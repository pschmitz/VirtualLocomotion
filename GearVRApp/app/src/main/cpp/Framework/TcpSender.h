#pragma once

#include <unistd.h>
#include <sys/socket.h>
#include <linux/in.h>
#include <netdb.h>
#include <strings.h>
#include <endian.h>
#include "Log.h"

class TcpSender {
public:
    TcpSender() {}
    TcpSender(const char* host, short port)
    {
        if(fd != -1) {
            close(fd);
        }

        struct sockaddr_in serv_addr;
        struct hostent *server;

        fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (fd < 0)
            LogError("TcpSender: failed to create socket");
        server = gethostbyname(host);
        if (server == NULL) {
            LogError("TcpSender: no such host");
        }

        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *)server->h_addr,
              (char *)&serv_addr.sin_addr.s_addr,
              server->h_length);
        serv_addr.sin_port = htons(port);

        if (connect(fd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
            LogError("TcpSender: connect error");

        LogError("TcpSender: connected. fd: ",
                 to_string(fd));
    }

    int fd = -1;
};
