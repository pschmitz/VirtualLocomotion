//
// Created by pschmitz on 3/20/17.
//

#include "UserKinematic.h"

float
UserKinematic::getRotationGain() {
    return rotationGain;
}

void
UserKinematic::setRotationGain(float gain) {
    rotationGain = gain;
}

glm::vec3
UserKinematic::getRealHeadPosition() {
    return realHeadTransform * glm::vec4(0, 0, 0, 1);
}

glm::mat4
UserKinematic::getRealHeadTransform() {
    return realHeadTransform;
}

void
UserKinematic::setRealHeadTransform(glm::mat4 transform) {
    realHeadTransform = transform;
}

glm::vec3
UserKinematic::getVirtualHeadPosition() {
    return virtualHeadTransform * glm::vec4(0, 0, 0, 1);
}

glm::mat4
UserKinematic::getVirtualHeadTransform() {
    return virtualHeadTransform;
}

void
UserKinematic::setVirtualHeadTransform(glm::mat4 transform) {
    virtualHeadTransform = transform;
}

void
UserKinematic::setMovementVelocityHead(glm::vec3 velocity) {
    movementVelocityHead = velocity;
}

glm::vec3
UserKinematic::getAngularVelocityHead() {
    return angularVelocityHead;
}

void
UserKinematic::setAngularVelocityHead(glm::vec3 velocity) {
    angularVelocityHead = velocity;
}

glm::mat4
UserKinematic::getRealTorsoTransform(int i) {
    return realTorsoTransform[i];
}