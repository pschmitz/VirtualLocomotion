//
// Created by Benni on 22.12.2016.
//

#ifndef GEARVRAPP_GRAPHICSCONTEXT_H
#define GEARVRAPP_GRAPHICSCONTEXT_H

#include <stack>
#include <exception>
#include "OpenGL.h"

class StackStateException : std::exception
{
public:
    /*const char* what() const override
    {
        return "Invalid framebuffer stack state.";
    }*/
};

struct Viewport
{
public:
    int X;
    int Y;
    int Width;
    int Height;
};

class GraphicsContext
{
private:
    std::stack<GLuint> frameBufferStack;
    std::stack<Viewport> viewportStack;
    void setViewport(Viewport &viewport);

public:
    int GetFramebufferStackSize() { return frameBufferStack.size(); }
    int GetViewportStackSize() { return viewportStack.size(); }

    void PushFramebuffer(GLuint frameBuffer);
    GLuint PeekFramebuffer();
    GLuint PopFramebuffer();

    void PushViewport(Viewport viewport);
    Viewport PeekViewport();
    Viewport PopViewport();
};

#endif //GEARVRAPP_GRAPHICSCONTEXT_H
