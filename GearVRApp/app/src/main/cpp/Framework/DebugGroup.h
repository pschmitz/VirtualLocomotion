#pragma once
#include <glow/extensions.hh>

struct DebugGroup {
    DebugGroup(char const * label) {
        if (glow::ext::glPushDebugGroup)
            glow::ext::glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 0, -1, label);
    }

    ~DebugGroup() {
        if (glow::ext::glPushDebugGroup)
            glow::ext::glPopDebugGroup();
    }
};

#define GPU_GROUP(label) DebugGroup gearVrAppFrameworkDebugGroup ##  __LINE__ (label);