#include "CommandQueue.h"

void CommandQueue::enque(const std::function<void()>& func) {
    std::lock_guard<std::mutex> guard(mWriteMutex);
    mChain.push_back(func);
}

bool CommandQueue::execute() {
    if (!mChain.empty())
    {
        auto& head = mChain.front();
        head();
        mChain.pop_front();
        return true;
    }
    return false;
}

CommandQueue &CommandQueue::gui() {
    static CommandQueue result;
    return result;
}