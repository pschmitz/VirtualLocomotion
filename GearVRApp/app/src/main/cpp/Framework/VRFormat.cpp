#include "VRFormat.h"

#include <zlib.h>
#include <iostream>
#include <stdio.h>

namespace VR {

    void VR::SplatCloud::writeToFile(const std::string &filename)
    {
        auto file = gzopen(filename.c_str(), "w");
        if (!file) {
            std::cerr << "Could not open " << filename << " for writing." << std::endl;
            return;
        }

        FileHeader header;
        header.splatCount = uint32_t(splats.size());
        header.cellCount  = uint32_t(cells.size());
        gzwrite(file, &header, sizeof(header));
        gzwrite(file, splats.data(), sizeof(Splat) * header.splatCount);
        gzwrite(file, cells.data(), sizeof(Cell) * header.cellCount);
        gzclose(file);
    }

    void VR::SplatCloud::readFromFile(const std::string& filename)
    {
        reset();

        auto file = gzopen(filename.c_str(), "r");
        if (!file) {
            std::cerr << "Could not open " << filename << " for reading." << std::endl;
            return;
        }

        FileHeader header;
        gzread(file, &header, sizeof(header));
        splats.resize(header.splatCount);
        gzread(file, splats.data(), sizeof(Splat) * header.splatCount);
        cells.resize(header.cellCount);
        gzread(file, cells.data(), sizeof(Cell) * header.cellCount);
        gzclose(file);
    }

    using byteptr = uint8_t*;
    struct GzStream {
        GzStream(byteptr buffer, size_t len)
        {
            z_stream stream{0,0,0,0,0,0,0,0,0,0,0,0,0,0};
            stream.next_in = buffer;
            stream.avail_in = unsigned(len);
            inflateInit2(&stream, 32 + 15);
            const auto block_size = 4 * 1024 * 1024;
            auto status = Z_OK;
            do {
                auto offset = uncompressed.size();
                uncompressed.resize(uncompressed.size() + block_size);
                stream.next_out = uncompressed.data() + offset;
                stream.avail_out = uint(uncompressed.size() - stream.total_out);
                status = inflate(&stream, Z_SYNC_FLUSH);
                assert(status >= 0);
            } while (status != Z_STREAM_END);
        }

        void read(byteptr target, size_t len)
        {
            const byteptr from = uncompressed.data() + reading;
            const byteptr to   = uncompressed.data() + std::min(reading + len, uncompressed.size());

            std::copy(from, to, target);
            reading += len;
        }

        std::vector<uint8_t> uncompressed;
        z_stream stream = {};
        size_t reading = 0;
    };

    void VR::SplatCloud::readFromBuffer(uint8_t* buffer, size_t len)
    {
        GzStream stream(buffer, len);

        FileHeader header;
        stream.read(byteptr(&header), sizeof(FileHeader));

        splats.resize(header.splatCount);
        stream.read(byteptr(splats.data()), sizeof(Splat) * header.splatCount);

        cells.resize(header.cellCount);
        stream.read(byteptr(cells.data()), sizeof(Cell) * header.cellCount);
    }
}
