#pragma once

#include <glm/vec3.hpp>
#include <vector>
#include <string>

namespace VR {
    struct Splat {
        glm::vec3 position;
        glm::vec3 normal;
        float radius;
        uint8_t color[3];
    };

    struct Cell {
        uint32_t first;
        uint32_t count;
        glm::vec3 bbmin;
        glm::vec3 bbmax;
    };

    struct FileHeader {
        uint32_t splatCount;
        uint32_t cellCount;
    };

    struct SplatCloud {
        std::vector<Splat> splats;
        std::vector<Cell>  cells;

        void reset() {
            splats.clear();
            cells.clear();
        }

        void writeToFile(const std::string& filename);
        void readFromFile(const std::string& filename);
        void readFromBuffer(uint8_t *buffer, size_t len);
    };

}
