//
// Created by Benjamin Roth on 07.12.2016.
//

#include "Shader.h"
#include "Log.h"

GLuint Shader::Compile(std::string shaderCode, GLenum shaderType)
{
    GLuint shader = glCreateShader(shaderType);

    const char *str = shaderCode.c_str();
    int length = (int)shaderCode.size();
    glShaderSource(shader, 1, &str, &length);
    glCompileShader(shader);

    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE)
    {
        GLint requiredLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &requiredLength);
        GLsizei length;
        GLchar infoLog[1000];
        glGetShaderInfoLog(shader, 1000, &length, infoLog);
        LogError("SHADER: ", infoLog);
    }

    return shader;
}


void Shader::LinkProgram(GLuint program)
{
    glLinkProgram(program);
    GLint status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if(status == GL_FALSE)
    {
        GLint requiredLength;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &requiredLength);
        GLsizei length;
        GLchar infoLog[1000];
        glGetProgramInfoLog(program, 1000, &length, infoLog);
        LogError("SHADER: ", infoLog);
    }
}
