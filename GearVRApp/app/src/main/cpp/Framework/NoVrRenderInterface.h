//
// Created by Benni on 14.12.2016.
//

#ifndef GEARVRAPP_NOVRRENDERINTERFACE_H
#define GEARVRAPP_NOVRRENDERINTERFACE_H


#include "EglHelper.h"
#include "IVrRenderInterface.h"
#include "Application.h"

class NoVrRenderInterface : public IVrRenderInterface
{
private:
    EglContext *egl;
    Application *application;
public:
    void Initialize(Application *app) override;
    void Load(EglContext *egl) override;
    void EnterVrMode() override;
    void LeaveVrMode() override;

    void BeginEye(GraphicsContext *context, int eyeIndex) override;
    void EndEye(GraphicsContext *context, int eyeIndex) override;

    void Unload() override;
    void Present() override;
    void Destroy() override;

    glm::mat4 GetProjectionMatrix(int eyeIndex) override;
};


#endif //GEARVRAPP_NOVRRENDERINTERFACE_H
