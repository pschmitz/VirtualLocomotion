//
// Created by Benni on 29.07.2016.
//

#ifndef NATIVEGEARVR_OBJLOADER_H
#define NATIVEGEARVR_OBJLOADER_H

#include <string>
#include <vector>
#include <stdio.h>
#include <glm/glm.hpp>

/*!
 * An object loader loads the vertex data from a given file
 */
class ObjLoader
{
private:
    std::vector<glm::vec3> positions;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;
    std::vector<int> positionIndices;
    std::vector<int> normalIndices;
    std::vector<int> texCoordIndices;
    int vertexCount;
public:
    /*!
     * Loads an object from a file. It gives back the positions, normals,
     * texCoords and indices if there.
     * @param filename Input: the path to the obj file
     * @param vertexCount Output: The number of vertices in the file
     * @param positionData Output: The position values of the vertices
     * @param normalData Output: The normal values of the vertices
     * @param texCoordData Output: The texture coordinate values of the vertices
     * @param indices Output: the indices
     */
    void loadObjFile(std::string &objFileContent,
                     std::vector<glm::vec3> &positions,
                     std::vector<glm::vec3> &normals,
                     std::vector<glm::vec2> &texcoords,
                     std::vector<unsigned int> &indices);

private:
    void reset();
    void handleLine(std::string &line);

    void addPosition(std::string &line);
    void addNormal(std::string &line);
    void addTexCoord(std::string &line);
    void addIndices(std::string &line);

    float* stringToFloatArray(std::string str, uint8_t assertSize);
    int* faceIndexStringToIndexArray(std::string str);
};

#endif //NATIVEGEARVR_OBJLOADER_H
