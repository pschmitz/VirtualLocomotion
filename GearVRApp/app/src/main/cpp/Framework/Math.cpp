//
// Created by Benni on 05.12.2016.
//

#include "Math.h"
#include <glm/glm.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <string>
#include <sstream>

template <typename T>
std::string to_string(T value)
{
    std::ostringstream os ;
    os << value ;
    return os.str() ;
}

std::string to_string(glm::mat4 matrix)
{
    return to_string(matrix[0].x) + to_string(matrix[1].x) + to_string(matrix[2].x)+ to_string(matrix[3].x) + "\n" +
           to_string(matrix[0].y) + to_string(matrix[1].y) + to_string(matrix[2].y)+ to_string(matrix[3].y) + "\n" +
           to_string(matrix[0].z) + to_string(matrix[1].z) + to_string(matrix[2].z)+ to_string(matrix[3].z) + "\n" +
           to_string(matrix[0].w) + to_string(matrix[1].w) + to_string(matrix[2].w)+ to_string(matrix[3].w);
}

std::string to_string(glm::vec2 vector)
{
    return to_string(vector.x) + ", " + to_string(vector.y);
}

std::string to_string(glm::vec3 vector)
{
    return to_string(vector.x) + ", " +
        to_string(vector.y) + ", " + to_string(vector.z);
}

std::string to_string(glm::quat quat)
{
    return
        to_string(quat[0]) + ", " + to_string(quat[1]) + ", " +
        to_string(quat[2]) + ", " + to_string(quat[3]);
}

glm::mat4 extractRotation(glm::mat4 matrix)
{
    glm::vec3 scale;
    glm::quat orientation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;

    glm::decompose(matrix, scale, orientation, translation, skew, perspective);
    return glm::mat4_cast(orientation);
}


/*glm::quat toGlm(const ovrQuatf quat)
{
    glm::quat quaternion;
    quaternion.x = quat.x;
    quaternion.y = quat.y;
    quaternion.z = quat.z;
    quaternion.w = quat.w;
    return quaternion;
}


vec3 toGlm(ovrVector3f vector)
{
    return vec3(vector.x, vector.y, vector.z);
}*/
