//
// Created by Benjamin Roth on 05.12.2016.

#include <exception>
#include <string>
#include <android/log.h>
#include "Scene.h"
#include "OpenGL.h"
#include "Application.h"
#include <glow/glow.hh>
#include <glow/common/log.hh>
#include "CommandQueue.h"

#ifdef ANDROID
#include "../Android/AndroidContentLoader.h"
#include "VrapiRenderInterface.h"
#include "NoVrRenderInterface.h"

#endif

#ifdef ANDROID
void Application::InitializeAndroid(NativeToJava *nativeToJava, jobject activity)
{
    Log("Application", "InitializeAndroid");
    this->nativeToJava = nativeToJava;
    mainactivity = GetCurrentEnv()->NewGlobalRef(activity);
}

void Application::destroyAndroid()
{
    GetCurrentEnv()->DeleteGlobalRef(mainactivity);
    mainactivity = nullptr;

    nativeToJava->Release(currentEnv);
    delete nativeToJava;
    nativeToJava = nullptr;
}

void Application::SetCurrentEnv(JNIEnv *env)
{
    currentEnv = env;
}
#endif

void Application::Initialize(Scene *scene)
{
    //Initialize application components
#ifdef ANDROID
#ifdef VRAPI
    vrRenderInterface = new VrapiRenderInterface();
#else
    vrRenderInterface = new NoVrRenderInterface();
#endif
    vrRenderInterface->Initialize(this);

    this->contentLoader = new AndroidContentLoader(this);
#else
    #error NotImplemented
#endif

    //Initialize scene
    currentScene = scene;
    if(currentScene != nullptr)
    {
        currentScene->InternalInitialize(this);
    }

    trackingReceiver.reset(new UdpReceiver([this](uint8_t* buffer, size_t len) {
        currentScene->ReceivePackage(nullptr, buffer, len);
    }, 1235));
}

void Application::initializeEgl()
{
#ifdef ANDROID
#ifdef VRAPI
    EglHelper::CreateContextVrapi(&egl);
#else
    EglHelper::CreateContext(&egl, this->Window, this->width, this->height);
#endif
#else
#error NotImplemented
#endif

    EGLint error = eglGetError();
    if(error != EGL_SUCCESS)
    {
        LogError("Egl initialization failed.");
        throw std::exception();
    }
}

void Application::initializeOpenGL()
{
    glVersion = glGetString(GL_VERSION);
    //const char *cstr = reinterpret_cast<char *>(glGetString(GL_VENDOR));
    //Log("Application", string("OpenGL version: ") + cstr);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);

    glow::initGLOW(this->contentLoader);
}

void Application::Load()
{
    //Load graphics and other resources
    if(!loaded)
    {
        initializeEgl();
        egl.MakeCurrent();
        initializeOpenGL();
        vrRenderInterface->Load(&egl);

        if(currentScene != nullptr)
        {
            currentScene->Load();
        }

        egl.ReleaseFromThread();
        loaded = true;
        fpsCounterStartpoint = FpsClock::now();
    }
}


void Application::AllowVrMode(bool value)
{
    allowVrMode = value;
    if(!allowVrMode && isInVrMode)
    {
        vrRenderInterface->LeaveVrMode();
        isInVrMode = false;
    }
}

void Application::Frame(double frameTime)
{
    if (CommandQueue::gui().execute()) return;
    if(loaded)
    {
        accumulatedFrames++;
        if (accumulatedFrames >= 50)
        {
            double seconds = std::chrono::duration<double, std::milli>(FpsClock::now() - fpsCounterStartpoint).count();
            double average = seconds / 50.0;

            glow::info() << "Frametime: " << average  << "ms, " << 1000.0 / average << " FPS";
            accumulatedFrames = 0;
            fpsCounterStartpoint = FpsClock::now();
        }

        egl.MakeCurrent();
        egl.GetCurrentSurfaceDimensions(width, height);
        glViewport(0,0, width, height);

        if(graphicsContext.GetFramebufferStackSize() > 1 || graphicsContext.GetViewportStackSize() > 1)
        {
            LogError("Application is pushing more Framebuffers/Viewports than it pops.");
        }

        if(allowVrMode && !isInVrMode)
        {
            vrRenderInterface->EnterVrMode();
            isInVrMode = true;
        }


        if (currentScene != nullptr)
        {
            currentScene->Update(frameTime);
        }

        for (int eyeIndex = 0; eyeIndex < 2; ++eyeIndex)
        {
            vrRenderInterface->BeginEye(GetGraphicsContext(), eyeIndex);

            //Rendering
            if (currentScene != nullptr)
            {
                currentScene->Draw(frameTime, GetGraphicsContext(), eyeIndex);
            }

            vrRenderInterface->EndEye(GetGraphicsContext(), eyeIndex);
        }

        vrRenderInterface->Present();
    }
}

void Application::Unload()
{
    //Unload graphics and other resources
    if(loaded)
    {
        loaded = false;
        if(currentScene != nullptr)
        {
            currentScene->Unload();
        }

        vrRenderInterface->Unload();
        egl.Release();
    }
}

void Application::Destroy()
{
    Unload();

    if(currentScene != nullptr)
    {
        currentScene->Destroy();
        delete currentScene;
        currentScene = nullptr;
    }

    vrRenderInterface->Destroy();
    delete vrRenderInterface;
    vrRenderInterface = nullptr;

    delete contentLoader;
    contentLoader = nullptr;

#ifdef ANDROID
    destroyAndroid();
#endif

}