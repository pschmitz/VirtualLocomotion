#pragma once

#include <vector>
#include <glow/fwd.hh>
#include <glm/vec2.hpp>
#include <glow/gl.hh>
#include <glow/objects/Framebuffer.hh>

class FboChain {

public:
    struct Attachment {
        std::string name;
        GLenum format;
    };


    FboChain(size_t count, std::vector<Attachment> const& formats);
    FboChain() {};

    void resize(glm::uvec2 size);

    /// Advances the FBO-Chain to the next FBO
    FboChain& operator++() {
        mCurrent = (mCurrent + 1) % mFbos.size();
        return *this;
    }

    /// get the n-th target of the current FBO
    glow::SharedTexture2D const& operator[](size_t index) {
        return mTargets[mCurrent * mStride + index];
    }

    glow::Framebuffer::BoundFramebuffer bind() {
        return mFbos[mCurrent]->bind();
    }

protected:

    std::vector<glow::SharedFramebuffer> mFbos;
    std::vector<glow::SharedTexture2D> mTargets;
    size_t mCurrent = 0ull;
    size_t mStride = 0ull;
};