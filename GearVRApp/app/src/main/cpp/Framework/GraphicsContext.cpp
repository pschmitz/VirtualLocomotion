//
// Created by Benni on 22.12.2016.
//

#include "GraphicsContext.h"

void GraphicsContext::PushFramebuffer(GLuint frameBuffer)
{
    frameBufferStack.push(frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
}

GLuint GraphicsContext::PeekFramebuffer()
{
    return frameBufferStack.top();
}

GLuint GraphicsContext::PopFramebuffer()
{
    if(frameBufferStack.size() == 0)
    {
        throw StackStateException();
    }

    GLuint temp = frameBufferStack.top();
    frameBufferStack.pop();

    if(frameBufferStack.size() > 0)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, frameBufferStack.top());
    }
    else
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    return temp;
}

void GraphicsContext::PushViewport(Viewport viewport)
{
    viewportStack.push(viewport);
    setViewport(viewport);
}

Viewport GraphicsContext::PeekViewport()
{
    return viewportStack.top();
}

Viewport GraphicsContext::PopViewport()
{
    if(viewportStack.size() == 0)
    {
        throw StackStateException();
    }

    Viewport temp = viewportStack.top();
    viewportStack.pop();

    if(viewportStack.size() > 0)
    {
        Viewport top = viewportStack.top();
        setViewport(top);
    }

    return temp;
}

void GraphicsContext::setViewport(Viewport &viewport)
{
    glViewport(viewport.X, viewport.Y, viewport.Width, viewport.Height);
}







