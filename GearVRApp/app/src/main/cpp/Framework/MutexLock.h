//
// Created by Benni on 14.12.2016.
//

#ifndef GEARVRAPP_LOCK_H
#define GEARVRAPP_LOCK_H

#include <pthread.h>
#include "Log.h"

int pthread_mutex_lock(pthread_mutex_t *mutex);
int pthread_mutex_trylock(pthread_mutex_t *mutex);

class MutexLock
{
private:
    pthread_mutex_t mutex;
public:
    void Lock()
    {
        if(pthread_mutex_lock(&mutex) != 0)
        {
            LogError("Mutex lock failed");
        }
    }

    void Unlock()
    {
        if(pthread_mutex_unlock(&mutex) != 0)
        {
            LogError("Mutex unlock failed");
        }
    }
};


#endif //GEARVRAPP_LOCK_H
