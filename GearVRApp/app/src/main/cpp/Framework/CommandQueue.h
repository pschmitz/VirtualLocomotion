#pragma once

#include <functional>
#include <deque>
#include <mutex>

class CommandQueue {
public:

    /// Append command to the chain
    void enque(const std::function<void()>& func);

    /// Execute a chained command, returns true if something was in the queue
    bool execute();

    static CommandQueue & gui();

protected:
    std::deque<std::function<void()>> mChain;
    std::mutex mWriteMutex;
};
