#pragma once

#include <array>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>



class CullingHelper {
public:
    void setMatrix(const glm::mat4& viewprojection);

    bool isInvisible(glm::vec3 const& bbmin, glm::vec3 const& bbmax) const;
    bool isInvisible(glm::vec3 const& center, float radius) const;

protected:
    std::array<glm::vec4,4> mPlanes;
};
