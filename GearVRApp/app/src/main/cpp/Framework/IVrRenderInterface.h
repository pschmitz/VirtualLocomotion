//
// Created by Benni on 14.12.2016.
//

#ifndef GEARVRAPP_IVRRENDERINTERFACE_H
#define GEARVRAPP_IVRRENDERINTERFACE_H

#include "EglHelper.h"
#include "GraphicsContext.h"
#include "Math.h"

class Application;

class IVrRenderInterface
{
public:
    virtual ~IVrRenderInterface() { };

    virtual void Initialize(Application *app) = 0;
    virtual void Load(EglContext *egl) = 0;

    virtual void EnterVrMode() = 0;
    virtual void LeaveVrMode() = 0;

    virtual void BeginEye(GraphicsContext *context, int eyeIndex) = 0;
    virtual void EndEye(GraphicsContext *context, int eyeIndex) = 0;

    virtual void Unload() = 0;
    virtual void Present() = 0;
    virtual void Destroy() = 0;

    virtual float getFovy() = 0;
    virtual float getNear() = 0;
    virtual glm::uvec2 getViewport() = 0;

    virtual glm::mat4 GetProjectionMatrix(int eyeIndex) = 0;
};


#endif //GEARVRAPP_IVRRENDERINTERFACE_H
