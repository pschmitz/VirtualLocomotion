//
// Created by Benni on 07.12.2016.
//

#ifndef GEARVRAPP_SHADER_H
#define GEARVRAPP_SHADER_H

#include <string>
#include "OpenGL.h"

namespace Shader
{
    GLuint Compile(std::string shaderCode, GLenum shaderType);
    void LinkProgram(GLuint program);
};


#endif //GEARVRAPP_SHADER_H
