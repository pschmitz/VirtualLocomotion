//
// Created by Benni on 07.12.2016.
//
#include "Log.h"
#include <android/log.h>


void Log(std::string tag, std::string message)
{
    __android_log_write(ANDROID_LOG_DEBUG, tag.c_str(), message.c_str());
}

void LogError(std::string tag, std::string message)
{
    __android_log_write(ANDROID_LOG_ERROR, tag.c_str(), message.c_str());
}

void Log(std::string message)
{
    __android_log_write(ANDROID_LOG_DEBUG, "NATIVE", message.c_str());
}

/*
void Log(char* tag, char* message)
{
    __android_log_write(ANDROID_LOG_DEBUG, tag, message);
}
 */

void LogError(std::string message)
{
    __android_log_write(ANDROID_LOG_ERROR, "NATIVE", message.c_str());
}

void LogWarning(std::string tag, std::string message)
{
    __android_log_write(ANDROID_LOG_WARN, tag.c_str(), message.c_str());
}

/*
void LogError(char* tag, char* message)
{
    __android_log_write(ANDROID_LOG_ERROR, tag, message);
}
 */
