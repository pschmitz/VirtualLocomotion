//
// Created by Benni on 05.12.2016.
//

#ifndef GEARVRAPP_SCENE_H
#define GEARVRAPP_SCENE_H

#include <jni.h>

#include "Application.h"

class Scene
{
public:
    Application *App;

    void InternalInitialize(Application *app);
    virtual void Initialize() { }
    virtual void Load() { }
    virtual void Update(double frameTime) { }
    virtual void Draw(double frameTime, GraphicsContext *graphicsContext, int eyeIndex) { }
    virtual void Unload() { }
    virtual void Destroy() { }

    virtual void ReceivePackage(JNIEnv *env, void *buffer, int length) { }
};


#endif //GEARVRAPP_SCENE_H
