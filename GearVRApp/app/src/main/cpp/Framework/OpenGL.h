//
// Created by Benjamin Roth on 23.11.2016.
//

#ifndef GEARVRAPP_OPENGL_H
#define GEARVRAPP_OPENGL_H

#define OPENGLES3

#ifdef OPENGLES3
#include <GLES3/gl31.h>
#else
#include <GLES2/gl2.h>
#endif

void glHelperCheckError();

#endif //GEARVRAPP_OPENGL_H
