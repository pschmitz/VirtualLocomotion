//
// Created by Benjamin Roth on 23.11.2016.
//
#ifndef GEARVRAPP_LOG_H
#define GEARVRAPP_LOG_H
#include <string>
#include <sstream>

template <typename T>
std::string to_string(T value)
{
    std::ostringstream os;
    os << value;
    return os.str();
}

void Log(std::string message);
void Log(std::string tag, std::string message);

void LogError(std::string message);
void LogError(std::string tag, std::string message);

void LogWarning(std::string tag, std::string message);

#endif //GEARVRAPP_LOG_H
