//
// Created by Benni on 05.12.2016.
//

#ifndef GEARVRAPP_MATH_H
#define GEARVRAPP_MATH_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>


#ifdef ARMEABI_V7A
#include <VrApi/VrApi_Types.h>
#endif

glm::mat4 extractRotation(glm::mat4 matrix);

std::string to_string(glm::mat4 matrix);
std::string to_string(glm::vec2 vector);
std::string to_string(glm::vec3 vector);
std::string to_string(glm::quat quat);

#endif //GEARVRAPP_MATH_H
