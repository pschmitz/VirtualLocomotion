#include "CullingHelper.h"
#include <glm/glm.hpp>

namespace
{
    glm::vec3 projNormalize(glm::vec4 const& v)
    {
        return glm::vec3(v) / v.w;
    }
    glm::vec4 makePlane(glm::vec3 const& v1,glm::vec3 const& v2,glm::vec3 const& v3)
    {
        glm::vec3 n = glm::normalize(glm::cross(v2 - v1, v3 - v1));
        float d = glm::dot(n, v1);
        return glm::vec4(n, -d);
    }
}

void CullingHelper::setMatrix(const glm::mat4& viewprojection) {
    auto inverted = glm::inverse(viewprojection);

    auto minZ = -1.0f;
    // not 1.0, because the corresponding world coordinate is infinitely far away
    auto maxZ =   .5f;

    glm::vec3 v000 = projNormalize(inverted * glm::vec4(-1, -1, minZ, 1));
    glm::vec3 v001 = projNormalize(inverted * glm::vec4(-1, -1, maxZ, 1));
    glm::vec3 v010 = projNormalize(inverted * glm::vec4(-1,  1, minZ, 1));
    glm::vec3 v011 = projNormalize(inverted * glm::vec4(-1,  1, maxZ, 1));
    glm::vec3 v100 = projNormalize(inverted * glm::vec4( 1, -1, minZ, 1));
    glm::vec3 v101 = projNormalize(inverted * glm::vec4( 1, -1, maxZ, 1));
    glm::vec3 v110 = projNormalize(inverted * glm::vec4( 1,  1, minZ, 1));

    // left
    mPlanes[0] = makePlane(v000, v001, v010);
    // right
    mPlanes[1] = makePlane(v100, v110, v101);
    // bottom
    mPlanes[2] = makePlane(v000, v100, v001);
    // top
    mPlanes[3] = makePlane(v010, v011, v110);
}

static bool fullyBelow(std::array<glm::vec3,8> const& corners,  glm::vec4 const& plane)
{
    for (auto const& p : corners)
    {
        if (glm::dot(glm::vec4(p,1.0), plane) > 0.0f) return false;
    }
    return true;
}

bool CullingHelper::isInvisible(glm::vec3 const &bbmin, glm::vec3 const &bbmax) const {
    std::array<glm::vec3,8> corners;
    corners[0] = {bbmin.x, bbmin.y, bbmin.z};
    corners[1] = {bbmin.x, bbmin.y, bbmax.z};
    corners[2] = {bbmin.x, bbmax.y, bbmin.z};
    corners[3] = {bbmin.x, bbmax.y, bbmax.z};
    corners[4] = {bbmax.x, bbmin.y, bbmin.z};
    corners[5] = {bbmax.x, bbmin.y, bbmax.z};
    corners[6] = {bbmax.x, bbmax.y, bbmin.z};
    corners[7] = {bbmax.x, bbmax.y, bbmax.z};


    for (auto const& plane : mPlanes)
    {
        if (fullyBelow(corners, plane)) return true;
    }
    return false;
}

bool CullingHelper::isInvisible(glm::vec3 const &center, float radius) const {
    for (auto const& plane : mPlanes)
    {
        if (glm::dot(glm::vec3(plane), center) + plane.w < - radius) return true;
    }
    return false;
}

