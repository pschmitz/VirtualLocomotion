//
// Created by Benni on 08.12.2016.
//

#ifndef GEARVRAPP_EGLHELPER_H
#define GEARVRAPP_EGLHELPER_H

#include <EGL/egl.h>
#include <android/native_window.h>
#include "Log.h"

#if !defined( EGL_OPENGL_ES3_BIT_KHR )
#define EGL_OPENGL_ES3_BIT_KHR		0x0040
#endif

struct EglContext
{
public:
    EGLint		MajorVersion;
    EGLint		MinorVersion;
    EGLDisplay	Display;
    EGLConfig	Config;
    //EGLSurface	TinySurface; //Note: this was used in the vrapi example but is unused in the current setup.
    EGLSurface	MainSurface;
    EGLContext	Context;

    EglContext()
    {
        this->MajorVersion = 0;
        this->MinorVersion = 0;
        this->Display = 0;
        this->Config = 0;
        //this->TinySurface = EGL_NO_SURFACE;
        this->MainSurface = EGL_NO_SURFACE;
        this->Context = EGL_NO_CONTEXT;
    }

    void MakeCurrent()
    {
        if(!eglMakeCurrent(Display, MainSurface, MainSurface, Context))
        {
            LogError("eglMakeCurrent failed.");
        }
    }

    void ReleaseFromThread()
    {
        if(!eglMakeCurrent(Display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT))
        {
            LogError("Failed to release egl context.");
        }
    }

    void GetCurrentSurfaceDimensions(int &width, int &height)
    {
        if (!eglQuerySurface(Display, MainSurface, EGL_WIDTH, &width) ||
            !eglQuerySurface(Display, MainSurface, EGL_HEIGHT, &height))
        {
            LogError("EGL", "eglQuerySurface() returned error " +
                     to_string(eglGetError()));
            //destroy();
        }
    }

    void SwapBuffers()
    {
        if (!eglSwapBuffers(Display, MainSurface))
        {
            LogError("eglSwapBuffers() returned error " +
                     to_string(eglGetError()));
        }
    }

    void Release()
    {
        eglDestroySurface(Display, MainSurface);
        //eglDestroySurface(Display, TinySurface);
        eglDestroyContext(Display, Context);
        Context = EGL_NO_CONTEXT;
        MainSurface = EGL_NO_SURFACE;
        //TinySurface = EGL_NO_SURFACE;
        Display = EGL_NO_DISPLAY;
        MajorVersion = 0;
        MinorVersion = 0;
    }
};

namespace EglHelper
{
    void CreateContextVrapi(EglContext *egl);
    void CreateContext(EglContext *egl, ANativeWindow *window, EGLint &width, EGLint &height);
};


#endif //GEARVRAPP_EGLHELPER_H
