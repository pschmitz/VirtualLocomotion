//
// Created by pschmitz on 1/10/17.
//

#ifndef GEARVRAPP_OPENALSOURCE_H
#define GEARVRAPP_OPENALSOURCE_H

#include <vector>

#include <glm/vec3.hpp>

#include <AL/al.h>

class OpenALSource {
public:

    OpenALSource();
    ~OpenALSource();

    void Load(const std::vector<unsigned char> &fileBuffer);
    void Play(bool looping = false);
    void Stop();

    void SetGain(float gain);
    void SetPosition(glm::vec3 position);

private:

    ALuint source;
    ALuint buffer;
};


#endif //GEARVRAPP_OPENALSOURCE_H
