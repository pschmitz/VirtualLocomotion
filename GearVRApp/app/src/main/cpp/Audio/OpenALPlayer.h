//
// Created by pschmitz on 1/6/17.
//

#ifndef GEARVRAPP_OPENALPLAYER_H
#define GEARVRAPP_OPENALPLAYER_H

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

class OpenALPlayer
{
public:

    void Initialize();

    void SetListenerPosition(glm::vec3 position);
    void SetListenerOrientation(glm::quat orientation);

private:
};


#endif //GEARVRAPP_OPENALPLAYER_H
