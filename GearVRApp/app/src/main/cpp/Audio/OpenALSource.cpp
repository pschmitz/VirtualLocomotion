//
// Created by pschmitz on 1/10/17.
//

//#include <AL/alc.h>
#include <AL/alut.h>

#include "OpenALSource.h"

OpenALSource::OpenALSource() :
    buffer(-1)
{
    alGenSources(1, &source);
}

OpenALSource::~OpenALSource()
{
    if(buffer != -1) {
        alDeleteBuffers(1, &buffer);
    }
    alDeleteSources(1, &source);
}

void
OpenALSource::Load(const std::vector<unsigned char> &fileBuffer)
{
    if(buffer != -1) {
        alDeleteBuffers(1, &buffer);
    }
    buffer = alutCreateBufferFromFileImage(fileBuffer.data(),
                                           fileBuffer.size());
    alSourcei(source, AL_BUFFER, buffer);
}

void
OpenALSource::Play(bool looping)
{
    alSourcei(source, AL_LOOPING, int(looping));
    alSourcePlay(source);
}

void OpenALSource::Stop()
{
    alSourceStop(source);
}

void OpenALSource::SetGain(float gain)
{
    alSourcef(source, AL_GAIN, gain);
}

void OpenALSource::SetPosition(glm::vec3 position)
{
    alSource3f(source, AL_POSITION,
               position[0], position[1], position[2]);
}
