//
// Created by pschmitz on 1/19/17.
//

#ifndef GEARVRAPP_VIEWERUDPSTREAMER_H
#define GEARVRAPP_VIEWERUDPSTREAMER_H

#include <string>

class ViewerUDPStreamer {
public:
    ViewerUDPStreamer();
    ~ViewerUDPStreamer();

    void Connect(std::string host, int port);
    void UpdateViewer(glm::mat4x4 viewerTransform, double frameTime);

private:

    int sockfd;

    double updateThreshold;
    double lastUpdateTime;
};

#endif //GEARVRAPP_VIEWERUDPSTREAMER_H
