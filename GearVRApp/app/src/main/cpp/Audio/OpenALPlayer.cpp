//
// Created by pschmitz on 1/6/17.
//

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alut.h>

#include "../Framework/Math.h"
#include "../Framework/Log.h"

#include "OpenALPlayer.h"

void OpenALPlayer::Initialize()
{
    alutInit(0, NULL);
    alGetError();
}

void OpenALPlayer::SetListenerPosition(glm::vec3 position)
{
    alListenerfv(AL_POSITION, glm::value_ptr(position));
}

void OpenALPlayer::SetListenerOrientation(glm::quat orientation)
{
    glm::vec3 at = glm::vec3(0, 0, -1);
    glm::vec3 up = glm::vec3(0, 1, 0);

    at = glm::rotate(orientation, at);
    up = glm::rotate(orientation, up);

    float vectors[] = {
        at[0], at[1], at[2],
        up[0], up[1], up[2]
    };

    alListenerfv(AL_ORIENTATION, vectors);
}
