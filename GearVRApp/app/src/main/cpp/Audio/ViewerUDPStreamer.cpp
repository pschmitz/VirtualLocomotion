//
// Created by pschmitz on 1/19/17.
//
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../Framework/Log.h"

#include "ViewerUDPStreamer.h"

ViewerUDPStreamer::ViewerUDPStreamer() :
    sockfd(-1),
    updateThreshold(0.1), // throttle update rate to 100ms.
    lastUpdateTime(0)
{}

ViewerUDPStreamer::~ViewerUDPStreamer()
{
    close(sockfd);
}

void ViewerUDPStreamer::Connect(std::string host, int port)
{
    if(sockfd != -1) {
        close(sockfd);
    }

    struct sockaddr_in serv_addr;
    struct hostent *server;

    sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd < 0)
        LogError("ViewerUDPStreamer: failed to create socket");
    server = gethostbyname(host.c_str());
    if (server == NULL) {
        LogError("ViewerUDPStreamer: no such host");
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(port);

    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        LogError("ViewerUDPStreamer: connect error");

    LogError("ViewerUDPStreamer: connected. fd: ",
             to_string(sockfd));
}

void ViewerUDPStreamer::UpdateViewer(glm::mat4x4 viewerTransform,
                                     double frameTime)
{
    // LogError("ViewerUDPStreamer: frameTime: ", to_string(frameTime));

    // if(frameTime - lastUpdateTime > updateThreshold)
    //     lastUpdateTime = frameTime;
    // else
    //     return;

//    LogError("viewer: ", to_string(viewerTransform));

    write(sockfd, glm::value_ptr(viewerTransform), 64);
}
