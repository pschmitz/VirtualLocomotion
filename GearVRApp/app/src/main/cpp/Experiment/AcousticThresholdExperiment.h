//
// Created by pschmitz on 3/1/17.
//

#ifndef GEARVRAPP_ACOUSTICTHRESHOLDEXPERIMENT_H
#define GEARVRAPP_ACOUSTICTHRESHOLDEXPERIMENT_H

#include <memory>
#include <vector>

#include "../Framework/TcpSender.h"

#include "PlayerTarget.h"

#include "RDWAgent.h"
#include "RDWTargetStrategy.h"
#include "../Rendering/Model.h"
#include "../Framework/TcpReceiver.h"
#include "../DataCaptureManager.h"

class UserKinematic;

class AcousticThresholdExperiment {
public:
    AcousticThresholdExperiment(UserKinematic & kinematic,
                                DataCaptureManager& dataCapture);

    void Load(ModelRenderer *modelRenderer);

    void frame();

    void reset();

    void setRadioModel(Model* model) { radioModel = model; }

private:
    enum Mode {
        E1Continuous,
        E2Manual
    };

    void startTrial();
    void frameTrial();

    void placeTargets();
    void placeActiveTarget();
    void placeOtherTargets();

    glm::vec3 getNextTargetPosition();
    void randomizeTargets();

    void sendAcousticTransform();
    void setupControlServer();

    void targetCollected();

    bool trialRunning;

    std::unique_ptr<PlayerTarget> targetStart;
    std::vector<std::unique_ptr<PlayerTarget>> targets;

    UserKinematic & kinematic;
    RDWTargetStrategy::TargetSet targetSet;

    bool targetStartActive = false;

//    TcpSender toAcousticSender;
    TcpReceiver controlReceiver;

    bool gainIncreasing = false;
    float defaultRotationGain = 1.f;

    Model* radioModel = nullptr;
    DataCaptureManager& dataCapture;

    Mode mode = E1Continuous;
//    int numTargetsUntilGainChange = 0;
    float newGainAfterTarget = 1.0f;
};

#endif //GEARVRAPP_ACOUSTICTHRESHOLDEXPERIMENT_H
