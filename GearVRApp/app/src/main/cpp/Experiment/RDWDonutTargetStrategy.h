/*
  ==============================================================================

    RDWDonutTargetStrategy.h
    Created: 8 Feb 2017 5:33:50pm
    Author:  pschmitz

  ==============================================================================
*/

#ifndef RDWDONUTTARGETSTRATEGY_H_INCLUDED
#define RDWDONUTTARGETSTRATEGY_H_INCLUDED

#include "RDWTargetStrategy.h"

class RDWDonutTargetStrategy : public RDWTargetStrategy {
public:
    RDWDonutTargetStrategy(TargetSet & targets, RDWAgent & agent,
                           float radius);

    void step() override;
    glm::vec2 getTarget() override;

private:
    TargetSet & targets;
    RDWAgent & agent;
    float radius;
};

#endif  // RDWDONUTTARGETSTRATEGY_H_INCLUDED
