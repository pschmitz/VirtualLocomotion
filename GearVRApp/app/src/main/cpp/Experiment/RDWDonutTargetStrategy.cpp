/*
  ==============================================================================

    RDWDonutTargetStrategy.cpp
    Created: 8 Feb 2017 5:33:50pm
    Author:  pschmitz

  ==============================================================================
*/

#include "RDWAgent.h"

#include "RDWDonutTargetStrategy.h"

RDWDonutTargetStrategy::RDWDonutTargetStrategy(
    TargetSet & targets, RDWAgent & agent, float radius) :
    targets(targets),
    agent(agent),
    radius(radius) {
}

void RDWDonutTargetStrategy::step() {
    float utilMin = std::numeric_limits<float>::max();
    int indexMin = 0;
    for(int target = 0 ; target < targets.targets.size() ; ++target) {
        if(target == targets.currentTarget - targets.targets.begin())
            continue;

        RDWAgent agentSim(agent);
        agentSim.setVerbose(false);
        agentSim.setErrorRadius(0);
        agentSim.turnTo(targets.targets[target], RDWAgent::shortTurn);
        agentSim.walkTo(targets.targets[target]);
        RDWAgent agentSimLong(agent);
        agentSimLong.turnTo(targets.targets[target], RDWAgent::longTurn);
        agentSimLong.walkTo(targets.targets[target]);

        auto plateauUtilFunc = [&](glm::vec2 pos, float rad) {
            return fmax(glm::length(pos) - rad, 0);
        };
        float utilDonut = plateauUtilFunc(agentSim.getRealPosition(), radius);
        float utilDonutLong = plateauUtilFunc(agentSimLong.getRealPosition(), radius);
        float utilDonutEffective = fmax(utilDonut, utilDonutLong);

        float utilVirtCenter =
            plateauUtilFunc(agentSim.getVirtualPosition(), 2.f);

        float utilVirtDistance =
            glm::clamp(
                1.f - glm::length(agent.getVirtualPosition() -
                                  agentSim.getVirtualPosition()),
                0.0f, 1.0f);

        float util =
            utilDonutEffective + 0.4 * utilVirtCenter + 1.5f * utilVirtDistance;

        if(util < utilMin) {
            utilMin = util;
            indexMin = target;
        }
    }
    targets.currentTarget = targets.targets.begin() + indexMin;
}

glm::vec2 RDWDonutTargetStrategy::getTarget() {
    if(targets.currentTarget == targets.targets.end()) {
        return {0, 0};
    }
    return *targets.currentTarget;
}
