#ifndef RDWAGENT_H_INCLUDED
#define RDWAGENT_H_INCLUDED

#include <utility>
#include <vector>

#include <glm/glm.hpp>

class RDWAgent {
public:
    enum TurnDirection {
        shortTurn,
        longTurn,
        randomTurn
    };

    enum ErrorMode {
        overshoot,
        circleUniform
    };

    RDWAgent(glm::vec2 position,
             float orientation);

    void setRotationGain(float gain);

    void setErrorMode(ErrorMode mode);
    void setErrorRadius(float error);

    void setVerbose(bool verbose);

    void turn(float angle);
    void turnTo(glm::vec2 point, TurnDirection direction = shortTurn);
    void walk(float distance);
    void walkTo(glm::vec2 point);

    glm::vec2 getRealPosition();
    void setRealPosition(glm::vec2 position);

    float getRealOrientation();
    void setRealOrientation(float orientation);

    glm::vec2 getVirtualPosition();
    void setVirtualPosition(glm::vec2 position);

    float getVirtualOrientation();
    void setVirtualOrientation(float orientation);

    std::vector<glm::vec2> & getRealTrajectory();
    std::vector<glm::vec2> & getVirtualTrajectory();

    void clearTrajectories();
    void resetPosition();

private:

    glm::vec2 positionStart;
    float orientationStart;

    float orientationReal;
    float orientationVirtual;

    float gainRotation;

    ErrorMode errorMode;
    float errorRadius;

    bool verbose;

    std::vector<glm::vec2> trajectoryReal;
    std::vector<glm::vec2> trajectoryVirtual;
};

#endif // RDWAGENT_H_INCLUDED
