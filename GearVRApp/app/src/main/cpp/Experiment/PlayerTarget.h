//
// Created by Benni on 17.02.2017.
//

#ifndef GEARVRAPP_PLAYERTARGET_H
#define GEARVRAPP_PLAYERTARGET_H

#include "../Rendering/Model.h"
#include "../Rendering/ModelRenderer.h"
#include "../Framework/Math.h"
#include <glm/gtx/transform.hpp>

class PlayerTarget
{
public:
    PlayerTarget(ModelRenderer *modelRenderer,
                 glm::vec4 color = glm::vec4(1, 0.8f, 0, 1))
    {
        bodyModel = modelRenderer->LoadModel(
            "models/body.1.bm", "textures/bodyBaked.png");

        glass = modelRenderer->LoadTransparentModel(
            "models/glass.bm", "textures/tempScaleTexture.png");

        water = modelRenderer->LoadTransparentModel(
            "models/water.bm", "textures/MetalBase0121_9_S.jpg", true);
        water->setColor(color);
    }

    glm::vec3 getPosition() {
        return currentPos;
    }

    void setPosition(glm::vec3 position) {
        currentPos = position;
        setTransformations();
    }

    void setPercent(float value) {
        currentValue = value;
        setTransformations();
    }

    void setVisible(bool visible) {
        bodyModel->setVisible(visible);
        water->setVisible(visible);
        glass->setVisible(visible);
    }

private:
    void setTransformations() {
        bodyModel->setTransformation(
            glm::translate(glm::mat4(1.0f), currentPos));
        glass->setTransformation(
            glm::translate(glm::mat4(1.0f), currentPos));

        auto watertrans = glm::translate(currentPos + glm::vec3{0.f, 1.4f * currentValue, 0.f})
                        * glm::scale(glm::vec3{-1.f, -(0.1f + 1.4f * currentValue), -1.f});

        water->setTransformation(watertrans);
    }

    glm::vec3 currentPos;
    float currentValue;

    Model *bodyModel;
    Model *water;
    Model *glass;
};

#endif //GEARVRAPP_PLAYERTARGET_H
