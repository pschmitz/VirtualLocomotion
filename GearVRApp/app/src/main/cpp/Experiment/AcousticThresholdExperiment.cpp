//
// Created by pschmitz on 3/1/17.
//

#include <memory>

#include <glm/vec2.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../Framework/UserKinematic.h"

#include "RDWDonutTargetStrategy.h"

#include "AcousticThresholdExperiment.h"
#include <cstdio>

namespace {
    const int targetCount = 5;
    const int targetGridSize = 36 + 1;

    const glm::vec3 targetAreaStart(1, 0, -1);
    const glm::vec3 targetAreaEnd(7, 0, -7);

    const glm::vec3 startPosition =
        (targetAreaStart + targetAreaEnd) / 2.f;

    const float targetRadius = 0.8f;
    const float targetActiveRadius = 0.15f;

    const float targetPercentActiveMin = 0.9f;
    const float targetPercentActiveMax = 1.15f;
    const float targetPercentOtherMin = 0.1f;
    const float targetPercentOtherMax = 0.7f;

    const float targetDistanceMin = 1.f;

    float gainIncrement = 0.f;
    float gainIncrementMagnitude = 0.1f/2.f;
}

AcousticThresholdExperiment::AcousticThresholdExperiment(
    UserKinematic & kinematic,
    DataCaptureManager& dataCapture) :
    trialRunning(false),
    kinematic(kinematic),
    dataCapture(dataCapture),
//    toAcousticSender("coudenhove", 4200),
    controlReceiver(4201)
{
    const float targetGridOffset = 6.f / (targetGridSize);
    for(int row = 0 ; row < targetGridSize ; ++row) {
        for(int col = 0 ; col < targetGridSize ; ++col) {
            targetSet.targets.push_back(
                glm::vec2(
                    (row - targetGridSize/2) * targetGridOffset,
                    (col - targetGridSize/2) * targetGridOffset));
        }
    }

    kinematic.setRotationGain(defaultRotationGain);
    setupControlServer();
}

void AcousticThresholdExperiment::Load(ModelRenderer *modelRenderer) {
    targets.resize(targetCount);

    for(auto & targetP : targets) {
        targetP = std::unique_ptr<PlayerTarget>
            (new PlayerTarget(modelRenderer));
        targetP->setVisible(false);
    }

    targetStart = std::unique_ptr<PlayerTarget>
        (new PlayerTarget(modelRenderer,
                          glm::vec4(1, 0, 0, 1)));
    targetStart->setPosition(startPosition);
    targetStart->setVisible(true);
    targetStart->setPercent(1.f);

    targetStartActive = false;
}

void AcousticThresholdExperiment::frame() {
    glm::vec3 headOnGround = kinematic.getVirtualHeadPosition();
    headOnGround.y = 0;

    if(targetStartActive)
    {
        if(!trialRunning) {
            if(glm::distance(headOnGround, startPosition) < targetActiveRadius) {
                startTrial();
            }
        }
        else {
            frameTrial();
        }
    }
}

void AcousticThresholdExperiment::reset() {
    trialRunning = false;
    for(auto & targetP : targets) {
        targetP->setVisible(false);
    }
    targetStart->setVisible(true);

    kinematic.setRotationGain(defaultRotationGain);
    gainIncrement = (gainIncreasing ? 1.f : -1.f) *
        gainIncrementMagnitude;
    LogError("-------- RESET --------------------------");
    LogError("gain: " + to_string(kinematic.getRotationGain()));
    LogError("gainIncrement: " + to_string(gainIncrement));

//    sendAcousticTransform();
}

void AcousticThresholdExperiment::startTrial() {
        trialRunning = true;

        targetStart->setVisible(false);
        for(auto & targetP : targets) {
            targetP->setVisible(true);
        }

        placeTargets();
}

void AcousticThresholdExperiment::frameTrial() {
    glm::vec3 headOnGround = kinematic.getVirtualHeadPosition();
    headOnGround.y = 0;

    if(glm::distance(headOnGround,
                     targets[0]->getPosition()) < targetActiveRadius) {

        targetCollected();

//        sendAcousticTransform();
    }
}

void AcousticThresholdExperiment::targetCollected() {
    dataCapture.LogTargetCollected(kinematic, targets[0]->getPosition());

    placeTargets();

    if(mode == E1Continuous) {
        kinematic.setRotationGain(
            kinematic.getRotationGain() + gainIncrement);
        LogError("gain: " + to_string(kinematic.getRotationGain()));
    }
    else if(mode == E2Manual) {
        kinematic.setRotationGain(newGainAfterTarget);
        //newGainAfterTarget = 1.f;
        LogError("gain: " + to_string(kinematic.getRotationGain()));
    }
}

void AcousticThresholdExperiment::placeTargets() {
    placeActiveTarget();
    placeOtherTargets();
}

void AcousticThresholdExperiment::placeActiveTarget() {
    glm::vec3 positionReal =
        kinematic.getRealHeadPosition() - startPosition;
    glm::vec3 positionVirtual =
        kinematic.getVirtualHeadPosition() - startPosition;

    glm::vec3 orientationReal =
        glm::mat3(kinematic.getRealHeadTransform()) * glm::vec3(0, 0, -1.f);
    glm::vec3 orientationVirtual =
        glm::mat3(kinematic.getVirtualHeadTransform()) * glm::vec3(0, 0, -1.f);

    float angleReal = atan2(-orientationReal.z, orientationReal.x);
    float angleVirtual = atan2(-orientationVirtual.z, orientationVirtual.x);

    RDWAgent agent({0, 0}, 0);
    agent.setRealPosition({positionReal.x, -positionReal.z});
    agent.setRealOrientation(angleReal);

    agent.setVirtualPosition({positionVirtual.x, -positionVirtual.z});
    agent.setVirtualOrientation(angleVirtual);

    agent.setRotationGain(kinematic.getRotationGain());

    LogError("---------------------------------------");
    LogError("ori real: " + to_string(orientationReal));
    LogError("ori virt: " + to_string(orientationVirtual));
    LogError("real position:    " + to_string(positionReal));
    LogError("real angle:       " + to_string(angleReal));
    LogError("virtual position: " + to_string(positionVirtual));
    LogError("virtual angle:    " + to_string(angleVirtual));

    RDWDonutTargetStrategy strategy(targetSet, agent, targetRadius);
    strategy.step();
    glm::vec2 target2D = strategy.getTarget();
    LogError("target:    " + to_string(target2D));

    targets[0]->setPosition(
        glm::vec3(target2D.x, 0, -target2D.y) + startPosition);
    targets[0]->setPercent(
        glm::linearRand(targetPercentActiveMin,
                        targetPercentActiveMax));
}

void AcousticThresholdExperiment::placeOtherTargets() {
    bool fits = false;
    glm::vec3 position;

    for(int target = 1 ; target < targets.size() ; ++target) {
        do  {
            position = glm::linearRand(targetAreaStart, targetAreaEnd);
            fits = true;

            for(int otherTarget = 0 ;
                otherTarget < target ; ++otherTarget) {
                if(glm::distance(targets[otherTarget]->getPosition(),
                                 position) < targetDistanceMin) {
                    fits = false;
                    break;
                }
            }
        } while(!fits);

        targets[target]->setPosition(position);
        targets[target]->setPercent(
            glm::linearRand(targetPercentOtherMin,
                            targetPercentOtherMax));
    }
}

void AcousticThresholdExperiment::randomizeTargets() {
    for(auto & targetP : targets) {
        glm::vec2 position =
            glm::linearRand(targetAreaStart, targetAreaEnd);
        targetP->setPosition(
            glm::vec3(position.x, 0, position.y));

        targetP->setPercent(glm::linearRand(0.f, 1.f));
    }
}

void AcousticThresholdExperiment::sendAcousticTransform() {
    // send gain
    float gain = kinematic.getRotationGain();
    // write(toAcousticSender.fd, &gain, sizeof(float));

    // // send virtual head transform
    // write(toAcousticSender.fd,
    //       glm::value_ptr(kinematic.getVirtualHeadTransform()),
    //       16*sizeof(float));
}

static bool stop_control_server = false;

void AcousticThresholdExperiment::setupControlServer() {
    std::thread([this](){
        std::array<char,256> buffer = {};
        while (!stop_control_server) {
            auto socket = accept(controlReceiver.mSocket, nullptr, nullptr);
            for (;;) {
                std::fill(begin(buffer), end(buffer), char(0));
                auto bytes_received = read(socket, buffer.data(), sizeof(buffer) - 1);

                if (bytes_received >= 2) {
                    // if(buffer[0] == 'c')
                    // {
                    //     mode = E1Continuous;
                    //     reset();
                    //     LogError("set to mode: CONTINUOUS (gain 
                    // }
                    if(buffer[0] == 'm')
                    {
                        float gain = float(strtod(buffer.data() + 2, nullptr));
                        newGainAfterTarget = gain;
                        defaultRotationGain = 1.f;

                        mode = E2Manual;
                        LogError("set to mode: MANUAL (gain " + to_string(gain) + ")");
                    }
                    // if(buffer[0] == 'r')
                    // {
                    //     // float gain = float(strtod(buffer.data() + 2, nullptr));
                    //     // dataCapture.LogUserReset(gain);
                    //     // LogError("User reset: " + to_string(gain));
                    // }
                    else if(buffer[0] == 's')
                    {
                        targetStartActive = true;
                    } else
                    {
                        bool gain_inc = (buffer[0] == '1');

                        float gain = float(strtod(buffer.data() + 2, nullptr));
                        if (gain != 0.f)
                        {
                            defaultRotationGain = gain;
                        }

                        mode = E1Continuous;
//                        LogError("set to mode: CONTINUOUS (gain 

                        gainIncreasing = gain_inc;
                        gainIncrement = (gainIncreasing ? 1.f : -1.f) *
                                        gainIncrementMagnitude;
//                        if (radioModel) radioModel->setVisible(radio_shown);
//                        LogError("AcousticThresholdExperiment: received update");
                        LogError("gainIncrement: " + to_string(gainIncrement));
//                        LogError("radio visible: " + to_string(radio_shown));
                    }
                } else {
                    //close(socket);
                    break;
                }
            }
        }
    }).detach();
}
