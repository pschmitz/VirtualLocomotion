#include <cstdlib>

#include <glm/gtc/constants.hpp>
#include <glm/gtc/random.hpp>

#include "../Framework/Log.h"

#include "RDWAgent.h"

const float epsilon = 0.1;

RDWAgent::RDWAgent(glm::vec2 position,
                   float orientation) :
    orientationStart(orientation),
    orientationReal(orientationStart),
    orientationVirtual(orientationStart),
    gainRotation(1.f),
    errorMode(overshoot),
    errorRadius(0),
    verbose(false) {
    positionStart = position;
    trajectoryReal.push_back(position);
    trajectoryVirtual.push_back(position);
}

void RDWAgent::setRotationGain(float gain) {
    gainRotation = gain;
}

void RDWAgent::setErrorMode(ErrorMode mode) {
    errorMode = mode;
}

void RDWAgent::setErrorRadius(float error) {
    errorRadius = error;
}

void RDWAgent::setVerbose(bool verbose) {
    this->verbose = verbose;
}

void RDWAgent::turn(float angle) {
    // float errorFactor =
    //     (1.f + (rng.nextFloat()*2 - 1.0f) * errorAngle/100.f);

    orientationVirtual += angle;
    orientationVirtual = fmodf(orientationVirtual, 2.f * glm::pi<float>());

    float angleReal = angle / gainRotation; // * errorFactor;
    orientationReal += angleReal;
    orientationReal = fmodf(orientationReal, 2.f * glm::pi<float>());

    if(verbose) {
        Log("virtOri", to_string(glm::degrees(orientationVirtual)));
        Log("realOri", to_string(glm::degrees(orientationReal)));
    }
}

void RDWAgent::turnTo(glm::vec2 point, TurnDirection direction) {
    glm::vec2 vector = point - getVirtualPosition();
    float angle = atan2(vector.y, vector.x);
    float turnAngle = angle - orientationVirtual;

    const float pi = glm::pi<float>();

    while(turnAngle < -pi) {
        turnAngle += 2.f * pi;
    }
    while(turnAngle > pi) {
        turnAngle -= 2.f * pi;
    }

    if(verbose) {
        Log("angle", to_string(glm::degrees(turnAngle)));
    }

    if(direction == randomTurn) {
        if(rand() % 10 <= 7) {
            direction = shortTurn;
        }
        else {
            direction = longTurn;
        }
    }

    if(direction == longTurn) {
        if(turnAngle < -epsilon) {
            turnAngle += 2.f * pi;
        }
        else if(turnAngle > epsilon) {
            turnAngle -= 2.f * pi;
        }

        if(verbose) {
            Log("taking long turn", to_string(glm::degrees(turnAngle)));
        }
    }
    else {
        if(verbose) {
            Log("taking short turn", to_string(glm::degrees(turnAngle)));
        }
    }

    // Logger::writeToLog("vector: " + vector.toString());
    // Logger::writeToLog("angle: " + String(angle));
    // Logger::writeToLog("orientationVirtual: " + String(orientationVirtual));
    // Logger::writeToLog("turn: " + String(turnAngle));
    turn(turnAngle);
}

void RDWAgent::walk(float distance) {
    glm::vec2 posVirtual = getVirtualPosition();

    posVirtual += glm::vec2(
        cos(orientationVirtual), sin(orientationVirtual)) * distance;
    trajectoryVirtual.push_back(posVirtual);

    glm::vec2 posReal = getRealPosition();
    // float errorFactor =
    //     (1.f + (rng.nextFloat()*2 - 1.0f) * errorLength/100.f);
    // posReal += glm::vec2(
    //     cos(orientationReal), sin(orientationReal)) *
    //     distance * errorFactor;

    glm::vec2 error(0, 0);

    switch(errorMode) {
    case overshoot: {
        float errorLength = glm::linearRand(-1.f, 1.f) * errorRadius;
        error = glm::vec2(cos(orientationReal),
                          sin(orientationReal)) * errorLength;
        break;
    }
    case circleUniform: {
        error = glm::circularRand(errorRadius);
        break;
    }}

    posReal +=
        glm::vec2(cos(orientationReal),
                     sin(orientationReal)) * distance + error;

    trajectoryReal.push_back(posReal);
}

void RDWAgent::walkTo(glm::vec2 point) {
    walk(glm::length(point - getVirtualPosition()));
}

glm::vec2 RDWAgent::getRealPosition() {
    return trajectoryReal.back();
}

void RDWAgent::setRealPosition(glm::vec2 position) {
    trajectoryReal.back() = position;
}

float RDWAgent::getRealOrientation() {
    return orientationReal;
}

void RDWAgent::setRealOrientation(float orientation) {
    orientationReal = orientation;
}

glm::vec2 RDWAgent::getVirtualPosition() {
    return trajectoryVirtual.back();
}

void RDWAgent::setVirtualPosition(glm::vec2 position) {
    trajectoryVirtual.back() = position;
}

float RDWAgent::getVirtualOrientation() {
    return orientationVirtual;
}

void RDWAgent::setVirtualOrientation(float orientation) {
    orientationVirtual = orientation;
}

std::vector<glm::vec2> & RDWAgent::getRealTrajectory() {
    return trajectoryReal;
}

std::vector<glm::vec2> & RDWAgent::getVirtualTrajectory() {
    return trajectoryVirtual;
}

void RDWAgent::clearTrajectories() {
    trajectoryReal.clear();
    trajectoryVirtual.clear();

    resetPosition();
}

void RDWAgent::resetPosition() {
    orientationReal = orientationStart;
    orientationVirtual = orientationStart;
    trajectoryReal.push_back(positionStart);
    trajectoryVirtual.push_back(positionStart);
}
