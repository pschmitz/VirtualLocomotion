#ifndef RDWTARGETSTRATEGY_H_INCLUDED
#define RDWTARGETSTRATEGY_H_INCLUDED

#include <vector>
#include <glm/glm.hpp>

class RDWAgent;
class RDWSpace;

class RDWTargetStrategy {
public:
    struct TargetSet {
        std::vector<glm::vec2> targets;
        std::vector<glm::vec2>::iterator currentTarget;
    };

    virtual ~RDWTargetStrategy() {}

    virtual void step() = 0;
    virtual glm::vec2 getTarget() = 0;

private:
};

#endif // RDWTARGETSTRATEGY_H_INCLUDED
