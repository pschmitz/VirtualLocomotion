#include "SplatRenderer.h"
#include "../Framework/VRFormat.h"
#include "../Framework/IContentLoader.h"
#include <glm/gtx/transform.hpp>
#include <glow/objects/ArrayBuffer.hh>
#include <glow/objects/Program.hh>
#include <glow/objects/VertexArray.hh>
#include <glow/objects/Texture2D.hh>
#include <glow/objects/Framebuffer.hh>
#include <glow/common/scoped_gl.hh>
#include <glm/gtx/string_cast.hpp>
#include <future>
#include <cstdlib>
#include "../Framework/CommandQueue.h"
#include <glow/debug.hh>
#include "../Framework/DebugGroup.h"

SplatRenderer::SplatRenderer(const std::string& vrfile)
    : mFilename(vrfile) {
}

void SplatRenderer::Load(IContentLoader *loader) {
    auto quadAb = glow::ArrayBuffer::create();
    quadAb->defineAttribute<glm::vec2>("aPosition");
    quadAb->bind().setData(std::vector<glm::vec2>{
                               glm::vec2(0.f,1.f),
                               glm::vec2(1.f,1.f),
                               glm::vec2(0.f,0.f),
                               glm::vec2(1.f,0.f)
                           });
    mFullscreenQuad = glow::VertexArray::create(quadAb, 0, GL_TRIANGLE_STRIP);

    auto ab = glow::ArrayBuffer::create();
    ab->defineAttribute(&VR::Splat::position, "aPosition");
    ab->defineAttribute(&VR::Splat::normal, "aNormal");
    ab->defineAttribute(&VR::Splat::radius, "aRadius");
    ab->defineAttribute(&VR::Splat::color, "aColor", glow::AttributeMode::NormalizedInteger);

    auto cloudfile = loader->GetExternalStorageFolder() + "/VirtualLocomotion/output.vr";
    std::thread([=]() {
        VR::SplatCloud cloud;
        cloud.readFromFile(cloudfile);
        if (cloud.splats.empty()) glow::error() << "No splat data loaded. Did you remember to push the file to SD-Card?";

        CommandQueue::gui().enque([=](){
            ab->bind().setData(cloud.splats);
            mCells = std::move(cloud.cells);
        });
    }).detach();

    mVertexArray = glow::VertexArray::create(ab, 0, GL_POINTS);
    mMainProgram = glow::Program::createFromFiles({"splats_main.vsh", "splats_main.fsh"});
    mDepthProgram = glow::Program::createFromFiles({"splats_zpre.vsh", "splats_zpre.fsh"});
    mNormalizeProgram = glow::Program::createFromFiles({"splats_normalize.vsh", "splats_normalize.fsh"});

    mFbos = FboChain(6, {
        {"fColor", GL_RGBA16F},
        {"depth",  GL_DEPTH_COMPONENT24}
    });
}

void SplatRenderer::Draw(Camera &camera, glm::uvec2 viewport, float fovy, float znear) {
    GPU_GROUP("SplatRenderer::Draw");
    //camera.ViewMatrix = glm::mat4();

    mFbos.resize(viewport);

    glm::mat4 biasMatrix = glm::rotate(-0.5f * glm::pi<float>(), glm::vec3{1.0f, 0.0f, 0.0f});
    auto vc  = glm::vec4(float(viewport.x) / std::tan(fovy*M_PI/360.0f), znear, 0.5f, 1.0f);
    auto invProj = glm::inverse(camera.ProjectionMatrix);
    auto pix2Ray = glm::vec4(
        invProj[0][0] * 2.0f / float(viewport.x),
        -invProj[0][0] + invProj[0][3],
        invProj[1][1] * 2.0f / float(viewport.y),
        -invProj[1][1] + invProj[1][3]
    );


    mCullingHelper.setMatrix(camera.ProjectionMatrix * camera.ViewMatrix * biasMatrix);

    {
        auto vao = mVertexArray->bind();

        ++mFbos;
        auto boundFbo = mFbos.bind();

        {   // First pass - fill depth buffer depth
            GPU_GROUP("SplatRenderer::Draw/Pass1 (Depth)");
            glow::scoped::clearColor black(0.f, 0.f, 0.f, 0.f);
            glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

            auto program = mDepthProgram->use();
            program.setUniform("uViewMatrix", camera.ViewMatrix * biasMatrix);
            program.setUniform("uProjectionMatrix", camera.ProjectionMatrix);
            program.setUniform("uNearPlane", znear);
            program.setUniform("vc", vc);
            program.setUniform("uPix2Ray", pix2Ray);

            vao.negotiateBindings();

            glow::scoped::colorMask noColor(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

            for (auto const& cell : mCells)
            {
                if (!mCullingHelper.isInvisible(cell.bbmin, cell.bbmax))
                {
                    //vao.drawRange(cell.first, cell.first + cell.count);
                    glDrawArrays(GL_POINTS, cell.first, cell.count);
                }
            }
        }

        {   // Main Pass / Blending
            GPU_GROUP("SplatRenderer::Draw/Pass2 (Blending)");
            auto program = mMainProgram->use();
            program.setUniform("uViewMatrix", camera.ViewMatrix * biasMatrix);
            program.setUniform("uProjectionMatrix", camera.ProjectionMatrix);
            program.setUniform("uNearPlane", znear);
            program.setUniform("vc", vc);
            program.setUniform("uPix2Ray", pix2Ray);

            vao.negotiateBindings();

            glow::scoped::blendFunc additive(GL_SRC_ALPHA, GL_ONE, GL_ONE, GL_ONE);
            glow::scoped::enable blend(GL_BLEND);
            glow::scoped::depthMask no_depth(GL_FALSE);

            for (auto const& cell : mCells)
            {
                if (!mCullingHelper.isInvisible(cell.bbmin, cell.bbmax))
                {
                    glDrawArrays(GL_POINTS, cell.first, cell.count);
                    //vao.drawRange(cell.first, cell.first + cell.count);
                }
            }
        }

    }
    {   // Third pass - normalization
        GPU_GROUP("SplatRenderer::Draw/Pass3 (Normalization)");
        glow::scoped::disable nocull(GL_CULL_FACE);

        auto program = mNormalizeProgram->use();
        program.setTexture("uColorTexture", mFbos[0]);

        mFullscreenQuad->bind().draw();
    }
}

SplatRenderer::~SplatRenderer() {

}


SplatRenderer::SplatRenderer() {}