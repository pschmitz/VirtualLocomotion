//
// Created by Benni on 17.02.2017.
//

#ifndef GEARVRAPP_MODEL_H
#define GEARVRAPP_MODEL_H

#include "../Framework/Math.h"
#include "../Framework/OpenGL.h"

class Model
{
public:
    Model(GLuint texture,
          GLuint vertexBuffer, GLuint vertexArrayObject,
          int vertexCount) :
        texture(texture),
        textureLightmap(0),
        vertexBuffer(vertexBuffer),
        vertexArrayObject(vertexArrayObject),
        vertexCount(vertexCount)
    {
    }

    glm::mat4 getTransformation() const;
    void setTransformation(glm::mat4 transformation);

    GLuint getTexture() const;
    void setTexture(GLuint texture);

    GLuint getTextureLightmap() const;
    void setTextureLightmap(GLuint texture);

    glm::vec4 getColor() const;
    void setColor(glm::vec4 color);

    GLuint getVertexBuffer() const;
    GLuint getVertexArrayObject() const;
    int getVertexCount() const;

    bool isVisible();
    void setVisible(bool visible);

    void setAdditive(bool _additive) { additive = _additive; }
    bool isAdditive() const { return additive; }

private:
    glm::mat4 transformation;

    GLuint texture;
    GLuint textureLightmap;

    glm::vec4 color;
    bool draw = true;

    GLuint vertexBuffer;
    GLuint vertexArrayObject;
    int vertexCount;

    bool additive = false;
};

#endif //GEARVRAPP_MODEL_H
