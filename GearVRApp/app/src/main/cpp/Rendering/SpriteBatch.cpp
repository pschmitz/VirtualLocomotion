//
// Created by Benjamin Roth on 13.12.2016.
//

#include "SpriteBatch.h"
#include "../Framework/Shader.h"
#include "../Framework/IContentLoader.h"
#include "../Framework/Log.h"

void SpriteBatch::Load(IContentLoader *contentLoader)
{
    std::string vsCode = contentLoader->LoadTextFromAssets("SpriteBatchVS.glsl");
    std::string fsCode = contentLoader->LoadTextFromAssets("SpriteBatchFS.glsl");
    GLuint vsShader = Shader::Compile(vsCode, GL_VERTEX_SHADER);
    GLuint fsShader = Shader::Compile(fsCode, GL_FRAGMENT_SHADER);
    program = glCreateProgram();
    glAttachShader(program, vsShader);
    glAttachShader(program, fsShader);
    Shader::LinkProgram(program);

    viewportSizeUniformLocation = glGetUniformLocation(program, "ViewportSize");
    VertexPositionLocation = glGetAttribLocation(program, "VertexPosition");
    VertexTexCoordLocation = glGetAttribLocation(program, "VertexTexCoord");
    VertexColorLocation = glGetAttribLocation(program, "VertexColor");
    VertexTextureIdLocation = glGetAttribLocation(program, "VertexTextureId");
    glBindAttribLocation(program, 0, "VertexPosition");
    glBindAttribLocation(program, 1, "VertexTexCoord");
    glBindAttribLocation(program, 2, "VertexColor");
    glBindAttribLocation(program, 3, "VertexTextureId");
    Shader::LinkProgram(program);

    glDeleteShader(vsShader);
    glDeleteShader(fsShader);

    //Bind samplers
    glUseProgram(program);
    for (int i = 0; i < SPRITEBATCH_TEXTURE_COUNT; ++i)
    {
        int location =
            glGetUniformLocation(
                program, ("Textures[" + to_string(i) + "]").c_str());
        glUniform1i(location, i);
    }

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER,
                 SPRITEBATCH_VERTEXBUFFERSIZE * sizeof(SpriteBatchVertex),
                 nullptr, GL_DYNAMIC_DRAW);

#ifdef OPENGLES3
//    glGenVertexArrays(1, &vertexBufferObject);
//    glBindVertexArray(vertexBufferObject);
//    setVertexAttributes();
//    glBindVertexArray(0);
#endif
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //Generate white pixel texture
    glGenTextures(1, &whitePixel);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, whitePixel);
    unsigned char data[3] = { 255, 255 ,0 };
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0,
                 GL_RGB, GL_UNSIGNED_BYTE, data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void SpriteBatch::setVertexAttributes()
{
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteBatchVertex),
                          0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(SpriteBatchVertex),
                          (void *) (sizeof(float) * 2));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(SpriteBatchVertex),
                          (void *) (sizeof(float) * 4));

    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(SpriteBatchVertex),
                          (void *) (sizeof(float) * 8));
}

void SpriteBatch::Begin(GraphicsContext *context)
{
    Viewport viewport = context->PeekViewport();
    this->viewportWidth = viewport.Width;
    this->viewportHeight = viewport.Height;
}

void SpriteBatch::Draw(glm::vec2 position, glm::vec2 size, glm::vec4 color)
{
    Draw(whitePixel, position, size, color);
}

void SpriteBatch::Draw(GLuint texture,
                       glm::vec2 position, glm::vec2 size, glm::vec4 color)
{
    if(vertexIndex == SPRITEBATCH_VERTEXBUFFERSIZE ||
       textureCurrentCount == SPRITEBATCH_TEXTURE_COUNT)
    {
        internalDraw();
    }

    float textureId = 0;
    bool foundTexture = false;

    for (int i = 0; i < textureCurrentCount; ++i)
    {
        if(textures[i] == texture)
        {
            textureId = (float)i;
            foundTexture = true;
            break;
        }
    }

    if(!foundTexture)
    {
        textureId = (float)textureCurrentCount;
        textures[textureCurrentCount] = texture;
        textureCurrentCount++;
    }

    SpriteBatchVertex v0;
    v0.Position = position;
    v0.TexCoord = glm::vec2(0,1);
    v0.Color = color;
    v0.TextureId = textureId;

    SpriteBatchVertex v1;
    v1.Position = position + glm::vec2(size.x, 0);
    v1.TexCoord = glm::vec2(1,1);
    v1.Color = color;
    v1.TextureId = textureId;

    SpriteBatchVertex v2;
    v2.Position = position + glm::vec2(size.x, size.y);
    v2.TexCoord = glm::vec2(1,0);
    v2.Color = color;
    v2.TextureId = textureId;

    SpriteBatchVertex v3;
    v3.Position = position + glm::vec2(0, size.y);
    v3.TexCoord = glm::vec2(0,0);
    v3.Color = color;
    v3.TextureId = textureId;

    vertices[vertexIndex] = v0;
    vertexIndex++;
    vertices[vertexIndex] = v2;
    vertexIndex++;
    vertices[vertexIndex] = v1;
    vertexIndex++;
    vertices[vertexIndex] = v2;
    vertexIndex++;
    vertices[vertexIndex] = v0;
    vertexIndex++;
    vertices[vertexIndex] = v3;
    vertexIndex++;
}

void SpriteBatch::internalDraw()
{
    if(vertexIndex != 0)
    {
        glHelperCheckError();
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glBufferData(GL_ARRAY_BUFFER,
                     SPRITEBATCH_VERTEXBUFFERSIZE * sizeof(SpriteBatchVertex),
                     vertices, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

#ifdef  OPENGLES3
//        glBindVertexArray(vertexBufferObject);
#else
        setVertexAttributes();
#endif
        glUseProgram(program);
        glUniform2f(viewportSizeUniformLocation,
                    float(viewportWidth), float(viewportHeight));

        for (unsigned int i = 0; i < textureCurrentCount; ++i)
        {
            glActiveTexture(GL_TEXTURE0 + i);
            glBindTexture(GL_TEXTURE_2D, textures[i]);
        }

        glDrawArrays(GL_TRIANGLES, 0, vertexIndex);
        glHelperCheckError();

#ifdef OPENGLES3
        glBindVertexArray(0);
#endif
        vertexIndex = 0;
        textureCurrentCount = 0;
    }
}

void SpriteBatch::End()
{
    internalDraw();
}

void SpriteBatch::Unload()
{
    glDeleteProgram(program);
    glDeleteTextures(1, &whitePixel);
}
