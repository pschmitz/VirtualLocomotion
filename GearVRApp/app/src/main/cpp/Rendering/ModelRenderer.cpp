//
// Created by Benni on 05.12.2016.
//

#include <string>
#include <algorithm>
#include "ModelRenderer.h"
#include "../Framework/Shader.h"
#include "../Framework/Math.h"
#include "../Framework/IContentLoader.h"
#include "../Framework/ObjLoader.h"
#include "../Framework/Log.h"

#ifndef OPENGLES3
#error ModelRender requires OpenGL ES 3
#endif

using std::string;
using namespace glm;

void ModelRenderer::Load(IContentLoader *contentLoader)
{
    this->contentLoader = contentLoader;

    //****** Load Material Shaders ******

    // Textured
    string vscode = contentLoader->LoadTextFromAssets("ModelRendererVS.glsl");
    string fscode = contentLoader->LoadTextFromAssets("ModelRendererFS.glsl");
    GLuint vertexShader = Shader::Compile(vscode, GL_VERTEX_SHADER);
    GLuint fragmentShader = Shader::Compile(fscode, GL_FRAGMENT_SHADER);

    texturedProgram = glCreateProgram();
    glAttachShader(texturedProgram, vertexShader);
    glAttachShader(texturedProgram, fragmentShader);
    Shader::LinkProgram(texturedProgram);
    configureProgramAttribLocations(texturedProgram);
    glDeleteShader(fragmentShader);
    texturedProgramLocations = getSharedLocations(texturedProgram);

    // Texture multiplied with lightmap
    string lightmapfscode = contentLoader->LoadTextFromAssets("ModelRendererLightMapFS.glsl");
    GLuint lightmapFragmentShader = Shader::Compile(lightmapfscode, GL_FRAGMENT_SHADER);
    lightmapProgram = glCreateProgram();
    glAttachShader(lightmapProgram, vertexShader);
    glAttachShader(lightmapProgram, lightmapFragmentShader);
    Shader::LinkProgram(lightmapProgram);
    configureProgramAttribLocations(lightmapProgram);
    glDeleteShader(lightmapFragmentShader);
    lightmapProgramLocations = getSharedLocations(lightmapProgram);

    // Transparent
    string transparentfscode = contentLoader->LoadTextFromAssets("ModelRendererTransparentFS.glsl");
    GLuint transparentFragmentShader = Shader::Compile(transparentfscode, GL_FRAGMENT_SHADER);
    transparentDynamicProgram = glCreateProgram();
    glAttachShader(transparentDynamicProgram, vertexShader);
    glAttachShader(transparentDynamicProgram, transparentFragmentShader);
    Shader::LinkProgram(transparentDynamicProgram);
    configureProgramAttribLocations(transparentDynamicProgram);
    glDeleteShader(transparentFragmentShader);
    transparentDynamicProgramLocations = getSharedLocations(transparentDynamicProgram);

    glDeleteShader(vertexShader);
}

Model* ModelRenderer::LoadTransparentModel(
    string meshFilename, string textureFilename, bool additive)
{
    int vertexCount;
    GLuint vertexBuffer = createVertexBuffer(meshFilename, &vertexCount);
    GLuint vertexArrayObject = createVertexArrayObject(vertexBuffer, transparentDynamicProgramLocations);

    //Create texture
    GLuint texture = loadTexture(textureFilename);

    Model *model = new Model(texture, vertexBuffer, vertexArrayObject, vertexCount);
    model->setAdditive(additive);
    transparentModels.push_back(model);
    return model;
}

Model* ModelRenderer::LoadModel(
    string meshFilename, string textureFilename)
{
    int vertexCount;
    GLuint vertexBuffer = createVertexBuffer(meshFilename, &vertexCount);
    GLuint vertexArrayObject =
        createVertexArrayObject(vertexBuffer, texturedProgramLocations);

    //Create texture
    GLuint texture = loadTexture(textureFilename);

    Model *model = new Model(
        texture, vertexBuffer, vertexArrayObject, vertexCount);
    texturedModels.push_back(model);
    return model;
}

Model* ModelRenderer::LoadModel(
    string meshFilename, string textureFilename, string lightmapTexture)
{
    int vertexCount;
    GLuint vertexBuffer = createVertexBuffer(meshFilename, &vertexCount);
    GLuint vertexArrayObject =
        createVertexArrayObject(vertexBuffer, lightmapProgramLocations);

    GLuint texture = loadTexture(textureFilename);
    GLuint lightmap = loadTexture(lightmapTexture);

    Model *model =
        new Model(texture, vertexBuffer, vertexArrayObject, vertexCount);
    model->setTextureLightmap(lightmap);

    lightmapModels.push_back(model);

    return model;
}

void ModelRenderer::configureProgramAttribLocations(GLuint program)
{
    GLint positionLocation = glGetAttribLocation(program, "VertexPosition");
    GLint normalLocation = glGetAttribLocation(program, "VertexNormal");
    GLint texcoord0Location = glGetAttribLocation(program, "VertexTexCoord0");
    GLint texcoord1Location = glGetAttribLocation(program, "VertexTexCoord1");
    glBindAttribLocation(program, 0, "VertexPosition");
    glBindAttribLocation(program, 1, "VertexNormal");
    glBindAttribLocation(program, 2, "VertexTexCoord0");
    glBindAttribLocation(program, 3, "VertexTexCoord1");
    //Link again to apply attrib locations.
    Shader::LinkProgram(program);
}

SharedLocations ModelRenderer::getSharedLocations(GLuint program)
{
    SharedLocations locations;
    locations.uMvpMatrix = glGetUniformLocation(program, "uMvpMatrix");
    locations.uNormalMatrix = glGetUniformLocation(program, "uNormalMatrix");
    locations.Texture = glGetUniformLocation(program, "Texture");
    locations.Texture2 = glGetUniformLocation(program, "Texture2");
    locations.uColor = glGetUniformLocation(program, "uColor");
    return locations;
}

void ModelRenderer::Unload()
{
    glDeleteProgram(texturedProgram);

    //Delete textures
    for (auto i = textureDictionary.begin(); i != textureDictionary.end(); ++i)
    {
        glDeleteTextures(1, &(i->second));
    }

    for (int i = 0; i < texturedModels.size(); ++i)
    {
        delete texturedModels[i];
    }

    for (int i = 0; i < lightmapModels.size(); ++i)
    {
        delete lightmapModels[i];
    }

    for (int i = 0; i < transparentModels.size(); ++i)
    {
        delete transparentModels[i];
    }
}

void ModelRenderer::Draw(Camera &camera)
{
    mat4 vpMatrix = camera.ProjectionMatrix * camera.ViewMatrix;
    drawModels(texturedProgram, texturedProgramLocations, vpMatrix, texturedModels);
    drawModels(lightmapProgram, lightmapProgramLocations, vpMatrix, lightmapModels);

    glEnable( GL_BLEND );
    glDepthMask(GL_FALSE);

    auto camDistance = [&camera](Model* a, Model* b) {
        auto posA = glm::vec3{camera.ViewMatrix * a->getTransformation() * glm::vec4(0,0,0,1)};
        auto posB = glm::vec3{camera.ViewMatrix * b->getTransformation() * glm::vec4(0,0,0,1)};
        return glm::length(posA) > glm::length(posB);
    };
    std::sort(begin(transparentModels), end(transparentModels), camDistance);
    drawModels(transparentDynamicProgram, transparentDynamicProgramLocations, vpMatrix, transparentModels, true);

    glDisable( GL_BLEND );
    glDepthMask(GL_TRUE);
}

void
ModelRenderer::drawModels(GLuint program, SharedLocations &locations, mat4 &vp,
                          std::vector<Model*> &models, bool blending) {
    glUseProgram(program);

    enum BlendMode { NONE, ADDITIVE, SUBTRACTIVE };
    BlendMode lastBlendMode = NONE;

    for (int i = 0; i < models.size(); ++i)
    {
        Model *model = models[i];

        if(!model->isVisible())
            continue;

        if (blending) {
            if (model->isAdditive() && lastBlendMode != ADDITIVE) {
                glBlendFunc(GL_ONE, GL_ONE);
                lastBlendMode = ADDITIVE;
            } else if (!model->isAdditive() && lastBlendMode != SUBTRACTIVE) {
                glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                lastBlendMode = SUBTRACTIVE;
            }
        }

        GLuint vao = model->getVertexArrayObject();
        glBindVertexArray(vao);

        mat4 mvp = vp * model->getTransformation();
        glUniformMatrix4fv(locations.uMvpMatrix, 1, GL_FALSE,
                           glm::value_ptr(mvp));

        mat4 normalMatrix = model->getTransformation();
        glUniformMatrix4fv(locations.uNormalMatrix, 1, GL_FALSE,
                           glm::value_ptr(normalMatrix));

        glUniform4fv(locations.uColor, 1, glm::value_ptr(model->getColor()));

        if(models[i]->getTexture() != 0)
        {
            glUniform1i(locations.Texture, 0);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, model->getTexture());
        }
        if(models[i]->getTextureLightmap() != 0)
        {
            glUniform1i(locations.Texture, 1);
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, model->getTexture());
        }

        glDrawArrays(GL_TRIANGLES, 0, model->getVertexCount());
    }
}

void ModelRenderer::checkModelFileHeader(unsigned char *dataPtr)
{
    //C# uses 2bytes for each char
    if(dataPtr[0] != 'g' || dataPtr[1] != 'v' || dataPtr[2] != 'r' || dataPtr[3] != 'a')
    {
        LogError("The loaded model file has NOT the correct header.");
        throw std::exception();
    }
    int version = *((int*)&dataPtr[4]);

    if(version != 1)
    {
        LogError("Loaded model has a different file layout version.");
        throw std::exception();
    }
}

GLuint ModelRenderer::createTexture(string textureFilename)
{
    string completePath = contentLoader->GetExternalStorageFolder() + "/VirtualLocomotion/" + textureFilename;

    GLuint texture;
    glGenTextures(1, &texture);
    TextureData textureData2 = contentLoader->LoadTextureFromFile(completePath);
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureData2.Width, textureData2.Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData2.Data);
    glGenerateMipmap(GL_TEXTURE_2D);
    textureData2.Release();
    return texture;
}

GLuint ModelRenderer::createVertexBuffer(string meshFilename, int *ptrVertexCount)
{
//Load model geometry
    std::vector<unsigned char> rawModelData = contentLoader->LoadRawBufferFromFile("/VirtualLocomotion/" + meshFilename);
    checkModelFileHeader(&rawModelData[0]);

    int vertexCount = *((int*)&rawModelData[8]);
    *ptrVertexCount = vertexCount;
    ModelVertex *vertices = (ModelVertex*)&rawModelData[12];

    for (int i = 0; i < vertexCount; ++i)
    {
        //Log(to_string(vertices[i].Position));
        vertices[i].TexCoord0.y = 1 - vertices[i].TexCoord0.y;
    }

    GLuint vertexBuffer;
    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(ModelVertex) * vertexCount, vertices, GL_STATIC_DRAW);
    glBindBuffer (GL_ARRAY_BUFFER, 0);
    return vertexBuffer;
}

GLuint ModelRenderer::loadTexture(string textureFilename)
{
    GLuint texture;
    auto result = textureDictionary.find(textureFilename);
    if (result != textureDictionary.end())
    {
        //Texture is already loaded.
        texture = result->second;
    }
    else
    {
        //Load texture
        texture = createTexture(textureFilename);
        textureDictionary[textureFilename] = texture;
    }
    return texture;
};

GLuint ModelRenderer::createVertexArrayObject(GLuint vertexBuffer, SharedLocations &locations)
{
    GLuint vertexArrayObject;
    glGenVertexArrays(1, &vertexArrayObject);
    glBindVertexArray(vertexArrayObject);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, sizeof(ModelVertex), NULL);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer (1, 3, GL_FLOAT, GL_FALSE, sizeof(ModelVertex), (const GLvoid *)sizeof(vec3));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer (2, 2, GL_FLOAT, GL_FALSE, sizeof(ModelVertex), (const GLvoid *)(sizeof(vec3) * 2));
    glEnableVertexAttribArray(3);
    glVertexAttribPointer (3, 2, GL_FLOAT, GL_FALSE, sizeof(ModelVertex), (const GLvoid *)(sizeof(vec3) * 2 + sizeof(vec2)));
    glBindVertexArray(0);
    return vertexArrayObject;
}
