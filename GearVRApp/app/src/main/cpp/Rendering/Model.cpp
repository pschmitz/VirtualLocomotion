//
// Created by Benni on 17.02.2017.
//

#include "Model.h"

glm::mat4
Model::getTransformation() const {
    return transformation;
}

void
Model::setTransformation(glm::mat4 transformation) {
    this->transformation = transformation;
}

GLuint
Model::getTexture() const {
    return texture;
}

void
Model::setTexture(GLuint texture) {
    texture = texture;
}

GLuint
Model::getTextureLightmap() const {
    return textureLightmap;
}

void
Model::setTextureLightmap(GLuint texture) {
    textureLightmap = texture;
}

glm::vec4
Model::getColor() const {
    return color;
}

void
Model::setColor(glm::vec4 color) {
    this->color = color;
}

GLuint
Model::getVertexBuffer() const {
    return vertexBuffer;
}

GLuint
Model::getVertexArrayObject() const {
    return vertexArrayObject;
}

int
Model::getVertexCount() const {
    return vertexCount;
}

bool Model::isVisible() {
    return draw;
}

void Model::setVisible(bool visible) {
    draw = visible;
}
