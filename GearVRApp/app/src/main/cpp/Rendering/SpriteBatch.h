//
// Created by Benni on 13.12.2016.
//

#ifndef GEARVRAPP_SPRITEBATCH_H
#define GEARVRAPP_SPRITEBATCH_H

#include "../Framework/Math.h"
#include "../Framework/OpenGL.h"
#include "../Framework/IContentLoader.h"
#include "../Framework/GraphicsContext.h"

struct SpriteBatchVertex
{
    glm::vec2 Position;
    glm::vec2 TexCoord;
    glm::vec4 Color;
    float TextureId;
};

#define SPRITEBATCH_TEXTURE_COUNT 5
#define SPRITEBATCH_BATCH_SIZE 100
#define SPRITEBATCH_VERTEXBUFFERSIZE SPRITEBATCH_BATCH_SIZE * 6

class SpriteBatch
{
public:
    void Load(IContentLoader *contentLoader);
    void Begin(GraphicsContext *context);
    void Draw(glm::vec2 position, glm::vec2 size, glm::vec4 color);
    void Draw(GLuint texture,
              glm::vec2 position, glm::vec2 size, glm::vec4 color);
    void End();
    void Unload();
private:
    SpriteBatchVertex vertices[SPRITEBATCH_VERTEXBUFFERSIZE];
    int vertexIndex = 0;

    int viewportWidth, viewportHeight;
    GLuint textures[SPRITEBATCH_TEXTURE_COUNT];
    int textureCurrentCount = 0;

    GLuint vertexBuffer;
    GLuint program;
    GLint VertexPositionLocation;
    GLint VertexTexCoordLocation;
    GLint VertexColorLocation;
    GLint VertexTextureIdLocation;
    GLint viewportSizeUniformLocation;
    GLuint whitePixel;
    GLuint vertexBufferObject;

    void setVertexAttributes();

#ifdef OPENGLES3
    GLuint vertexArrayObject;
#endif

    void internalDraw();
};


#endif //GEARVRAPP_SPRITEBATCH_H
