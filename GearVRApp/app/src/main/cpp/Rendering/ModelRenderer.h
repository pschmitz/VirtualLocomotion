//
// Created by Benni on 05.12.2016.
//

#ifndef GEARVRAPP_MODELRENDERER_H
#define GEARVRAPP_MODELRENDERER_H

#include <vector>
#include "../Framework/OpenGL.h"
#include "Camera.h"
#include "../Android/NativeToJava.h"
#include "../Framework/IContentLoader.h"
#include "../Framework/Log.h"
#include "Model.h"

#include <map>

struct ModelVertex
{
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoord0;
    glm::vec2 TexCoord1;
};

struct SharedLocations
{
    GLint uMvpMatrix;
    GLint uNormalMatrix;
    GLint Texture;
    GLint Texture2;
    GLint uColor;
};

class ModelRenderer
{
public:
    void Load(IContentLoader *contentLoader);
    void Draw(Camera &camera);
    void Unload();

    Model* LoadModel(string meshFilename, string textureFilename);
    Model* LoadModel(string meshFilename, string textureFilename, string lightmapTexture);

    Model* LoadTransparentModel(string meshFilename, string textureFilename, bool additive = false);
    //Model* LoadColoredAdditiveModel(string meshFilename);

    //void ReleaseModel(Model *model);
private:
    IContentLoader *contentLoader;

    std::vector<Model*> texturedModels;
    std::vector<Model*> lightmapModels;

    std::vector<Model*> transparentModels;

    std::map<string, GLuint> textureDictionary;
    GLuint loadTexture(string textureFilename);

    //****** Materials ******
    //Textured
    GLuint texturedProgram;
    SharedLocations texturedProgramLocations;

    //Texture multiplied with lightmap
    GLuint lightmapProgram;
    SharedLocations lightmapProgramLocations;

    //Transparent colored
    GLuint transparentDynamicProgram;
    SharedLocations transparentDynamicProgramLocations;

private:
    void configureProgramAttribLocations(GLuint program);
    SharedLocations getSharedLocations(GLuint program);

    void drawModels(GLuint program, SharedLocations &locations, glm::mat4 &vp, std::vector<Model*> &models, bool blending = false);

    void checkModelFileHeader(unsigned char *dataPtr);
    GLuint createTexture(string textureFilename);
    GLuint createVertexBuffer(string meshFilename, int *ptrVertexCount);
    GLuint createVertexArrayObject(GLuint vertexBuffer, SharedLocations &locations);
};

#endif //GEARVRAPP_MODELRENDERER_H
