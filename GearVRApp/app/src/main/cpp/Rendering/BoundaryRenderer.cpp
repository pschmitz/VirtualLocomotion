//
// Created by Benni on 07.12.2016.
//

#include "BoundaryRenderer.h"
#include "../Framework/Shader.h"

using namespace glm;

struct vertex
{
    vec3 Position = vec3(0,0,0);
    vec3 Normal = vec3(0,1,0);
    vec2 TexCoord = vec2(0,0);

    vertex()
    {

    }

    vertex(vec3 pos, vec3 normal, vec2 tex)
    {
        Position = pos;
        Normal = normal;
        TexCoord = tex;
    }

    vertex(float x, float y, float z,
           float nx, float ny, float nz,
           float tx, float ty)
    {
        Position = vec3(x,y,z);
        Normal = vec3(nx,ny,nz);
        TexCoord = vec2(tx,ty);
    }
};

const int indexCount = 36 - 12;

void BoundaryRenderer::Load(IContentLoader *contentLoader)
{
    const int vertexCount = 24;
    vertex vertices[vertexCount]
            {
                    vertex(0,0,0, 0,-1,0, 0,0),
                    vertex(1,0,0, 0,-1,0, 1,0),
                    vertex(0,0,1, 0,-1,0, 0,1),
                    vertex(1,0,1, 0,-1,0, 1,1),

                    vertex(0,0,0, 0,0,-1, 0,0),
                    vertex(1,0,0, 0,0,-1, 1,0),
                    vertex(0,1,0, 0,0,-1, 0,1),
                    vertex(1,1,0, 0,0,-1, 1,1),

                    vertex(1,0,0, 1,0,0, 0,0),
                    vertex(1,0,1, 1,0,0, 1,0),
                    vertex(1,1,0, 1,0,0, 0,1),
                    vertex(1,1,1, 1,0,0, 1,1),

                    vertex(1,0,1, 0,0,1, 0,0),
                    vertex(0,0,1, 0,0,1, 1,0),
                    vertex(1,1,1, 0,0,1, 0,1),
                    vertex(0,1,1, 0,0,1, 1,1),

                    vertex(0,0,1, -1,0,0, 0,0),
                    vertex(0,0,0, -1,0,0, 1,0),
                    vertex(0,1,1, -1,0,0, 0,1),
                    vertex(0,1,0, -1,0,0, 1,1),

                    vertex(0,1,0, 0,1,0, 0,0),
                    vertex(1,1,0, 0,1,0, 1,0),
                    vertex(0,1,1, 0,1,0, 0,1),
                    vertex(1,1,1, 0,1,0, 1,1),
            };

    for (int i = 0; i < vertexCount; ++i)
    {
        //make [1,1,1]
        vertices[i].Position.x = (vertices[i].Position.x - 0.5f);
        vertices[i].Position.y = (vertices[i].Position.y - 0.5f);
        vertices[i].Position.z = (vertices[i].Position.z - 0.5f);

        //from d3d texcoords to openGL texcoords
        vertices[i].TexCoord.x = 1 - vertices[i].TexCoord.x;
        vertices[i].TexCoord.y = 1 - vertices[i].TexCoord.y;

        vertices[i].TexCoord.x *= 10;
        vertices[i].TexCoord.y *= 5;
    }

    uint indices[indexCount]
            {
                    //0, 1, 3, //Bottom
                    //0, 3, 2,

                    4, 6, 5, //Front
                    5, 6, 7,

                    8, 10, 11, //Right
                    8, 11, 9,

                    12, 15, 13, //Back //TODO: Fix Texcoords
                    12, 14, 15,

                    16, 19, 17, //Left
                    16, 18, 19,

                    //20, 22, 21, //Top
                    //21, 22, 23,
            };

    for (int i = 0; i < indexCount; i += 3)
    {
        uint temp = indices[i];
        indices[i] = indices[i+2];
        indices[i+2] = temp;
    }

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex) * vertexCount, vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * indexCount, indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    //Create shader
    std::string vscode = contentLoader->LoadTextFromAssets("BoundaryVS.glsl");
    std::string fscode = contentLoader->LoadTextFromAssets("BoundaryFS.glsl");

    GLuint vertexShader = Shader::Compile(vscode, GL_VERTEX_SHADER);
    GLuint fragmentShader = Shader::Compile(fscode, GL_FRAGMENT_SHADER);

    program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    uViewMatrixLocation = glGetUniformLocation(program, "uViewMatrix");
    uProjectionMatrixLocation = glGetUniformLocation(program, "uProjectionMatrix");
    uTransformationMatrixLocation = glGetUniformLocation(program, "uTransformMatrix");
    uCameraPositionLocation = glGetUniformLocation(program, "uCameraPosition");
    uColorLocation = glGetUniformLocation(program, "uColor");
    uBoundaryDebugLocation = glGetUniformLocation(program, "uBoundaryDebug");

    //InputLayout
#ifdef OPENGLES3
    glGenVertexArrays(1, &vertexBufferObject);
    glBindVertexArray(vertexBufferObject);
#endif

    glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
    setVertexAttributes();

    //create texture
    glGenTextures(1, &texture);

    const int textureSize = 128;
    unsigned char textureData[textureSize * textureSize * 4];

    for (int k = 0; k < textureSize; ++k)
    {
        for (int i = 0; i < textureSize; ++i)
        {
            textureData[k * textureSize * 4 + i * 4] = 255;
            textureData[k * textureSize * 4 + i * 4 + 1] = 255;
            textureData[k * textureSize * 4 + i * 4 + 2] = 255;
            textureData[k * textureSize * 4 + i * 4 + 3] = 255;
        }
    }

    for (int l = 0; l < textureSize; ++l)
    {
        textureData[l * 4] = 50;
        textureData[l * 4 + 1] = 50;
        textureData[l * 4 + 2] = 50;
        textureData[l * 4 + 3] = 255;
    }

    for (int l = 0; l < textureSize; ++l)
    {
        textureData[l * textureSize * 4 + 0 * 4] = 50;
        textureData[l * textureSize * 4 + 0 * 4 + 1] = 50;
        textureData[l * textureSize * 4 + 0 * 4 + 2] = 50;
        textureData[l * textureSize * 4 + 0 * 4 + 3] = 255;
    }

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureSize, textureSize, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, textureData);
    glGenerateMipmap(GL_TEXTURE_2D);

    setBoundaryDebug(false);
}

void BoundaryRenderer::setVertexAttributes()
{
    GLint tempint = glGetAttribLocation(program, "position");
    GLint tempint2 = glGetAttribLocation(program, "normal");
    GLint tempint3 = glGetAttribLocation(program, "texcoord");

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);

    glEnableVertexAttribArray(tempint);
    glVertexAttribPointer (tempint, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), NULL);
    if(tempint2 != -1)
    {
        glEnableVertexAttribArray(tempint2);
        glVertexAttribPointer(tempint2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                              (const GLvoid *) sizeof(vec3));
    }
    glEnableVertexAttribArray(tempint3);
    glVertexAttribPointer (tempint3, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid *)(sizeof(vec3) * 2));
}


void BoundaryRenderer::Render(
    glm::vec3 cameraPosition, glm::vec3 color,
    glm::mat4 eyeViewMatrix, glm::mat4 projectionMatrix,
    float x, float y, float width, float height)
{
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glBindVertexArray(vertexBufferObject);
    glUseProgram(program);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

    glm::vec3 center = glm::vec3(x + width/2, 1.5f, y + height/2);

    glm::mat4 transformation =
        glm::translate(mat4(1.0f), center) *
        glm::scale(mat4(1.0f), vec3(width, 0.2f, height));

    glUniformMatrix4fv(uTransformationMatrixLocation, 1, GL_FALSE,
                       glm::value_ptr(transformation));
    glUniformMatrix4fv(uViewMatrixLocation, 1, GL_FALSE,
                       glm::value_ptr(eyeViewMatrix));
    glUniformMatrix4fv(uProjectionMatrixLocation, 1, GL_FALSE,
                       glm::value_ptr(projectionMatrix));
    glUniform3f(uCameraPositionLocation,
                cameraPosition.x, cameraPosition.y, cameraPosition.z);
    glUniform3f(uColorLocation, color.x, color.y, color.z);


    glBindTexture(GL_TEXTURE_2D, texture);
    glActiveTexture(GL_TEXTURE0);

    //glDrawArrays(GL_TRIANGLES, 0, 6);
    glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, NULL);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
}

void BoundaryRenderer::Unload()
{
    glDeleteBuffers(1, &vertexBuffer);
#ifdef OPENGLES3
    glDeleteVertexArrays(1, &vertexBufferObject);
#endif
    glDeleteProgram(program);
}

void BoundaryRenderer::setBoundaryDebug(bool enabled) {
    glProgramUniform1i(program, uBoundaryDebugLocation, int(enabled));
}


