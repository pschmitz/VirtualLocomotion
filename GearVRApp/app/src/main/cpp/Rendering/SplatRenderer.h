#pragma once

#include <string>
#include <glow/fwd.hh>
#include "../Framework/VRFormat.h"
#include "Camera.h"
#include "../Framework/FboChain.h"
#include "../Framework/CullingHelper.h"

class IContentLoader;
class SplatRenderer {
public:
    SplatRenderer();
    ~SplatRenderer();
    SplatRenderer(const std::string& vrfile);
    void Load(IContentLoader*);
    void Draw(Camera &camera, glm::uvec2 viewport, float fovy, float znear);


protected:
    const std::string mFilename = "";

    glow::SharedVertexArray mVertexArray, mFullscreenQuad;
    glow::SharedProgram mMainProgram, mDepthProgram, mNormalizeProgram;
    FboChain mFbos;
    CullingHelper mCullingHelper;

    std::vector<VR::Cell> mCells;
};
