//
// Created by Benni on 07.12.2016.
//

#ifndef GEARVRAPP_BOUNDARYRENDERER_H
#define GEARVRAPP_BOUNDARYRENDERER_H

#include <jni.h>
#include "../Framework/OpenGL.h"
#include "../Framework/Math.h"
#include "../Android/NativeToJava.h"
#include "../Framework/IContentLoader.h"

class BoundaryRenderer
{
public:
    void Load(IContentLoader *contentLoader);
    void Render(glm::vec3 cameraPosition, glm::vec3 color,
                glm::mat4 eyeViewMatrix, glm::mat4 projectionMatrix,
                float x, float y, float width, float height);
    void Unload();

    void setBoundaryDebug(bool enabled);

private:
    void setVertexAttributes();

    GLuint vertexBuffer;
    GLuint indexBuffer;
    GLuint vertexBufferObject;
    GLuint program;

    GLint uViewMatrixLocation;
    GLint uProjectionMatrixLocation;
    GLint uTransformationMatrixLocation;
    GLint uCameraPositionLocation;
    GLint uColorLocation;
    GLint uBoundaryDebugLocation;

    GLuint texture;
};


#endif //GEARVRAPP_BOUNDARYRENDERER_H
