//
// Created by Benni on 05.12.2016.
//

#ifndef GEARVRAPP_CAMERA_H
#define GEARVRAPP_CAMERA_H

#include "../Framework/Math.h"

class Camera
{
public:
    glm::mat4 ViewMatrix;
    glm::mat4 ProjectionMatrix;
};


#endif //GEARVRAPP_CAMERA_H
