//
// Created by Benni on 07.12.2016.
//

#ifndef GEARVRAPP_DATACAPTUREMANAGER_H
#define GEARVRAPP_DATACAPTUREMANAGER_H

#include <vector>
#include <glm/vec3.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include "Framework/Application.h"
#include "Framework/UserKinematic.h"

struct UserKinematic;

class DataCaptureManager
{
public:
    void Start(Application* app);
        //Opens/creates textfile for the record.

    void Finish();
        //Closes the record file after flushing the corresponding input stream.

    /*
    Creates the data record string and writes it (or at least puts in the
    corresponding stream for writing the output file, depending on configuration).
    Modify this method to adjust to the actual contents of UserKinematic instances.
    Only call between calling the Start(...) and Finish() methods!
    */

    void AddUserKinematicData(
        unsigned long frameNumber,
        UserKinematic &kinematicData, glm::mat4 const (&torsos)[2],
        float realRotation, float virtualRotation, float distance);

    std::string GetKinematicDataString(
            unsigned long frameNumber,
            UserKinematic &kinematicData, glm::mat4 const (&torsos)[2],
            float realRotation, float virtualRotation, float distance);

    void LogReset(UserKinematic &kinematic);
    void LogUserReset(float gain);
    void LogTrackingFrameSkip(int numFrames);
    void LogTrialStarted(UserKinematic &kinematic);
    void LogTargetCollected(UserKinematic &kinematic, glm::vec2 targetPos);

    bool IsCapturing() { return capturing; }
        /*
        Returns true when called between calling the Start(...) and Finish() methods,
        and false otherwise.
        */

private:
    struct LineTypes {
        static const char* kinematic;
        static const char* reset;
        static const char* user_reset;
        static const char* tracking_loss;
        static const char* target;
    };

    double currentTimestamp();

    void appendKinematic(UserKinematic& kinematic, std::string& to);

    //void appendHeadLineToFile();

    string currentDateAndTimeAsString();

    string getMatlabString(glm::mat4 v);
    string getMatlabString(glm::vec3 v);
    string getMatlabString(float v);

    bool capturing = false;

    FILE * saveDataFile;

};


#endif //GEARVRAPP_DATACAPTUREMANAGER_H
