//
// Created by Benni on 13.12.2016.
//

#ifndef GEARVRAPP_FRAMEWORKTESTSCENE_H
#define GEARVRAPP_FRAMEWORKTESTSCENE_H


#include "Framework/Scene.h"
#include "Rendering/SpriteBatch.h"
#include "Rendering/ModelRenderer.h"

class FrameworkTestScene : public Scene
{
public:
    void Initialize() override;
    void Load() override;
    void Update(double frameTime) override;
    void Draw(double frameTime, GraphicsContext *context, int eyeIndex) override;
    void Unload() override;

private:
    GLuint texture;
    SpriteBatch spriteBatch;

    ModelRenderer modelRenderer;
};


#endif //GEARVRAPP_FRAMEWORKTESTSCENE_H
