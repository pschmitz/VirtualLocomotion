//
// Created by kscharf on 5/23/17.
//

#include "PostProcessing.h"

#include "Framework/Log.h"

#include <vector>
#include <sstream>
#include <iomanip>

#include <glm/ext.hpp>
#include <glm/gtx/matrix_decompose.hpp>


std::string PostProcessing::R2S(float f) {
    std::stringstream stringStream;
    stringStream << std::setprecision(std::numeric_limits<double>::digits10 + 1) << f;
    return stringStream.str();
}

void PostProcessing::computeRelativeAngleYHeadToTorsoString() {

    relativeAngleYString = "UNDEFINED";



    if (hasValidTorsoTransform && hasValidHeadTransform) {
        glm::mat4 relativeTransformation = glm::inverse(realTorsoTransform) * realHeadTransform;

        relativeAngleYHeadTorsoDeg = glm::degrees(angleAroundGlobalY(glm::mat3(relativeTransformation)));
        relativeAngleYString = R2S(relativeAngleYHeadTorsoDeg);

    } else {
        LogError("head: " + to_string(hasValidHeadTransform));
        LogError("torso: " + to_string(hasValidTorsoTransform));
        LogError("front: " + to_string(hasBodyTorsoFront));
        LogError("back: " + to_string(hasBodyTorsoBack));
        LogError("offsets: " + to_string(hasValidTorsoOffsets));
    }

}

void PostProcessing::computeAndAddAnglesHead(float currentGain) {

    currentOrientationAngleXHeadDeg = glm::degrees(angleAroundLocalX(glm::mat3(realHeadTransform)));
    assert(!isnanf(currentOrientationAngleXHeadDeg));
    currentOrientationAngleYHeadDeg = glm::degrees(angleAroundGlobalY(glm::mat3(realHeadTransform)));
    assert(!isnanf(currentOrientationAngleYHeadDeg));
    currentOrientationAngleYHeadVirtualDeg = glm::degrees(angleAroundGlobalY(glm::mat3(virtualHeadTransform)));
    assert(!isnanf(currentOrientationAngleYHeadVirtualDeg));

    if (frames > 0) {
        glm::mat4 relativeHeadTransformFrame = glm::inverse(realHeadTransformLast)
                                                   * realHeadTransform;

        /* glm::mat4 relativeHeadTransformFrameVirtual = virtualHeadTransform *
                                                   glm::inverse(virtualHeadTransformLast);*/

        angleYHeadCurrentFrameDeg = glm::degrees(angleAroundGlobalY(glm::mat3(relativeHeadTransformFrame)));
        accumulatedAngleYHeadDeg += angleYHeadCurrentFrameDeg;
        accumulatedAbsAngleYHeadDeg += fabs(angleYHeadCurrentFrameDeg);

        //angleYHeadCurrentFrameVirtualDeg = glm::degrees(angleAroundGlobalY(glm::mat3(relativeHeadTransformFrameVirtual))); //problem for resets because of jumps
        angleYHeadCurrentFrameVirtualDeg = currentGain * angleYHeadCurrentFrameDeg;
        accumulatedAngleYHeadVirtualDeg += angleYHeadCurrentFrameVirtualDeg;
        accumulatedAbsAngleYHeadVirtualDeg += fabs(angleYHeadCurrentFrameVirtualDeg);

        angleXHeadCurrentFrameDeg = glm::degrees(angleAroundLocalX(glm::mat3(relativeHeadTransformFrame)));
        assert(!isnanf(angleXHeadCurrentFrameDeg));

        accumulatedAbsAngleXHeadDeg += fabs(angleXHeadCurrentFrameDeg);

    } else {
        angleXHeadCurrentFrameDeg = 0.;
        angleYHeadCurrentFrameDeg = 0.;
        angleYHeadCurrentFrameVirtualDeg = 0.;
    }

}

void PostProcessing::computeAndAddAngleYTorso() {
    currentOrientationAngleYTorsoString = "UNDEFINED";
    angleYTorsoCurrentFrameDeg = 0.;
    if (frames > 0 && hasValidTorsoTransform) {
        if (lastTorsoTransformValid) {
            glm::mat4 relativeTorsoTransformFrame = glm::inverse(realTorsoTransformLast)
                                                    * realTorsoTransform;

            angleYTorsoCurrentFrameDeg = glm::degrees(
                angleAroundGlobalY(glm::mat3(relativeTorsoTransformFrame)));
            accumulatedAngleYTorsoDeg += angleYTorsoCurrentFrameDeg;
            accumulatedAbsAngleYTorsoDeg += fabs(angleYTorsoCurrentFrameDeg);
        }

        float currentOrientationAngleYTorsoDeg = glm::degrees(angleAroundGlobalY(glm::mat3(realTorsoTransform)));
        currentOrientationAngleYTorsoString = R2S(currentOrientationAngleYTorsoDeg);
    }
}

//void PostProcessing::computeAndAddAngleYTorsoVirtual() {
//
//    if (hasValidTorsoTransform && hasValidHeadTransform) {
//        accumulatedAngleYTorsoVirtualDeg = accumulatedAngleYHeadVirtualDeg - relativeAngleYHeadTorsoDeg;
//    }
//
//}

void PostProcessing::computePositionStrings() {
    positionHeadRealString = "UNDEFINED";
    positionHeadVirtualString = "UNDEFINED";
    if (hasValidHeadTransform) {
        positionHeadRealString = getMatlabStringPositionXnegativeZWithOffsetCorrection(realHeadTransform);
        positionHeadVirtualString = getMatlabStringPositionXnegativeZWithOffsetCorrection(virtualHeadTransform);
    }
}


void PostProcessing::checkForValidTransforms() {

    hasValidHeadTransform = false;
    if(frames > 1)
    {
        if(realHeadTransform[3] != realHeadTransformLast[3] ||
           realHeadTransform[3] != realHeadTransformLastLast[3]) {
            hasValidHeadTransform = true;
        }
    }

    hasBodyTorsoFront = false;
    hasBodyTorsoBack = false;

    if(realFrontTransform != realFrontTransformLast)
        hasBodyTorsoFront = true;

    if(realBackTransform != realBackTransformLast)
        hasBodyTorsoBack = true;

    // instead skip first line in main loop?
    if(frames < 2) {
        hasBodyTorsoFront = false;
        hasBodyTorsoBack = false;
    } else {

        hasValidTorsoTransform = (
                (hasBodyTorsoBack && hasBodyTorsoFront) ||
                ((hasBodyTorsoBack || hasBodyTorsoFront) && hasValidTorsoOffsets)
        );
    }




    //hasValidHeadTransform = true;
    //hasBodyTorsoFront = true;
    //hasBodyTorsoBack = true;
    //hasValidTorsoTransform = true;

}

void PostProcessing::fixAccumulatedValue(float *lastValue, float *currentValue, float *currentSum) {
    if (frames > 0) {
        float increment;
        if (*currentValue < *lastValue) {
            increment = *currentValue;
        } else {
            increment = *currentValue - *lastValue;
        }
        *currentSum += increment;
    } else {
        *currentSum = 0.f;
    }
    *lastValue = *currentValue;
}

std::string PostProcessing::GetCleanedStringFromDataLine(std::string line) {
    std::istringstream ssLine(line);

    std::string token;
    std::getline(ssLine, token, ','); //"KINEMATIC"
    std::string eventString = token;

    std::getline(ssLine, token, ','); //framenumber

    std::getline(ssLine, token, ','); //timestamp
    std::string stringTimestamp = token;

    std::getline(ssLine, token, ','); //gain
    std::string stringGain = token;
    float gain = ReadFloat(token);

    std::getline(ssLine, token, ','); //matrix (real head)
    realHeadTransform = ReadMat4(token);

    std::getline(ssLine, token, ','); //matrix (virtual head)
    virtualHeadTransform = ReadMat4(token);

    std::getline(ssLine, token, ','); //nothing

    std::getline(ssLine, token, ','); //matrix (real front)
    realFrontTransform = ReadMat4(token);
    realFrontTransform = correctBodyTransform(realFrontTransform);

    std::getline(ssLine, token, ','); //matrix (real back)
    realBackTransform = ReadMat4(token);
    realBackTransform = correctBodyTransform(realBackTransform);

    std::getline(ssLine, token, ',');
    float readTotalAngleYRealRad = ReadFloat(token);
    fixAccumulatedValue(&lastReadTotalAngleYRealRad, &readTotalAngleYRealRad, &totalAngleYRealRadFixed);
    std::string stringTotalAngleYReal = R2S(totalAngleYRealRadFixed*(180.0f/3.141592f));

    std::getline(ssLine, token, ',');
    float readTotalAngleYVirtualRad = ReadFloat(token);
    fixAccumulatedValue(&lastReadTotalAngleYVirtualRad, &readTotalAngleYVirtualRad, &totalAngleYVirtualRadFixed);
    std::string stringTotalAngleYVirtual = R2S(totalAngleYVirtualRadFixed*(180.0f/3.141592f));

    std::getline(ssLine, token, ',');
    float readTotalDistance = ReadFloat(token);
    //LogError("readDist: " + R2S(readTotalDistance));
    //LogError("lastReadDist: " + R2S(lastReadTotalDistance));
    fixAccumulatedValue(&lastReadTotalDistance, &readTotalDistance, &totalDistanceFixed);
    std::string stringTotalDistance = R2S(totalDistanceFixed);

    checkForValidTransforms();

    computePositionStrings();

    computeTorsoTransform();
    computeRelativeAngleYHeadToTorsoString();

    computeAndAddAnglesHead(gain);
    computeAndAddAngleYTorso();


#if DEBUG_OUTPUT
        std::cout << "+++++++++++++++++++++++++++++++" << std::endl;
        std::cout << "realHeadTransform" << std::endl;
        std::cout << glm::to_string(realHeadTransform) << std::endl;

        std::cout << "realFrontTransform" << std::endl;
        std::cout << glm::to_string(realFrontTransform) << std::endl;

        std::cout << "realBackTransform" << std::endl;
        std::cout << glm::to_string(realBackTransform) << std::endl;
#endif


#if DEBUG_OUTPUT
        std::cout << "realTorsoTransform" << std::endl;
        std::cout << glm::to_string(realTorsoTransform) << std::endl;
#endif


#if DEBUG_OUTPUT
        std::cout << "relativeAngleHeadTorso" << std::endl;
        std::cout << relativeAngleHeadTorso << std::endl;
#endif


    std::string lineOut = "";

//    bool test = hasValidHeadTransform && hasValidTorsoTransform;
//    if (test) {
//        lineOut += "VALIDTRANSFORMS ";
//    } else {
//        lineOut += "INVTRANSFORMS ";
//    }

    lineOut =
            //stringTimestamp + "," + //OK
            //stringGain + "," + //OK

            //positionHeadRealString + "," + //OK
            //positionHeadVirtualString + "," + //OK
            //stringTotalDistance + "," +                             // !!!! erste matrix ist weit weg von der 0 matrix, die anfangs da steht (ABER: auch in echten daten?)

            //R2S(currentOrientationAngleXHeadDeg) + "," +
            //R2S(accumulatedAbsAngleXHeadDeg) + "," +

            //R2S(currentOrientationAngleYHeadDeg) + "," +
            //R2S(accumulatedAngleYHeadDeg) + "," +
            //stringTotalAngleYReal + "," +
            R2S(accumulatedAbsAngleYHeadDeg) + "," +              //ersetzt obigen Wert (unnötig)

            //currentOrientationAngleYTorsoString + "," +
            //R2S(accumulatedAngleYTorsoDeg) + "," +
            R2S(accumulatedAbsAngleYTorsoDeg) + "," +

            //R2S(currentOrientationAngleYHeadVirtualDeg) + "," +
            //R2S(accumulatedAngleYHeadVirtualDeg) + "," +
            //stringTotalAngleYVirtual + "," +
            R2S(accumulatedAbsAngleYHeadVirtualDeg) + "," +       //ersetzt obigen Wert (unnötig)

            relativeAngleYString;                                 //checken, ob Winkel tracker/handy ein Mal aligned wurden !!!!!

    frames ++;
    lastTorsoTransformValid = hasValidTorsoTransform;
    lastHeadTransformValid = hasValidHeadTransform;
    realHeadTransformLastLast = realHeadTransformLast;
    realHeadTransformLast = realHeadTransform;
    virtualHeadTransformLast = virtualHeadTransform;
    realFrontTransformLast = realFrontTransform;
    realBackTransformLast = realBackTransform;
    realTorsoTransformLast = realTorsoTransform;

    return lineOut;
}


std::string PostProcessing::getMatlabString(glm::mat4 v) {
    std::stringstream stringStream;
    stringStream << std::setprecision(std::numeric_limits<double>::digits10 + 1)
                 << "[ "
                 << v[0][0] << " " << v[1][0] << " " << v[2][0] << " " << v[3][0] << "; "
                 << v[0][1] << " " << v[1][1] << " " << v[2][1] << " " << v[3][1] << "; "
                 << v[0][2] << " " << v[1][2] << " " << v[2][2] << " " << v[3][2] << "; "
                 << v[0][3] << " " << v[1][3] << " " << v[2][3] << " " << v[3][3] << "]";
    return stringStream.str();
}

std::string PostProcessing::getMatlabStringPositionXnegativeZWithOffsetCorrection(glm::mat4 v) {
    std::stringstream stringStream;
    stringStream << std::setprecision(std::numeric_limits<double>::digits10 + 1)
                 << "[ "
                 << v[3][0] - 4.0f << "; "
                 << -v[3][2] - 4.0f << "] ";
    return stringStream.str();
    //offset is (-4.f, 0.f, 4.f);
}

void PostProcessing::computeTorsoTransform() {
    glm::vec3 realTorsoPosition;
    glm::mat4 realTorsoOrientation;

    if (hasBodyTorsoBack && hasBodyTorsoFront) {
        hasValidTorsoOffsets = true;
        offsetTorsoFrontToCenter =
                glm::transpose(glm::mat3(realFrontTransform)) *
                (realTorsoPosition - glm::vec3(realFrontTransform[3]));

        offsetTorsoBackToCenter =
                glm::transpose(glm::mat3(realBackTransform)) *
                (realTorsoPosition - glm::vec3(realBackTransform[3]));
    }

#if DEBUG_OUTPUT
    std::cout << std::boolalpha
              << "hasValidTorsoTransform: " << hasValidTorsoTransform << std::endl;
#endif

    //Note that one the following three conditions is true iff hasValidTorsoTransform is true.
    if(!hasBodyTorsoBack && hasBodyTorsoFront && hasValidTorsoOffsets) {
        handleBodyFront(realTorsoPosition, realTorsoOrientation);
    }
    else if(!hasBodyTorsoFront && hasBodyTorsoBack && hasValidTorsoOffsets) {
        handleBodyBack(realTorsoPosition, realTorsoOrientation);
    }
    else if(hasBodyTorsoFront && hasBodyTorsoBack) {
        handleBodyBoth(realTorsoPosition, realTorsoOrientation);
    }

    //torsoTransformString = "UNDEFINED";
    if (hasValidTorsoTransform) {
        //If no torso body is tracked for the current frame, skip the calculation.

#if DEBUG_OUTPUT
        std::cout << std::boolalpha
                  << "hasBodyTorsoFront: " << hasBodyTorsoFront << std::endl;
        std::cout << std::boolalpha
                  << "hasBodyTorsoBack: " << hasBodyTorsoBack << std::endl;

        std::cout << "offsetTorsoFrontToCenter: "
                  << glm::to_string(offsetTorsoFrontToCenter) << std::endl;
        std::cout << "offsetTorsoBackToCenter: "
                  << glm::to_string(offsetTorsoBackToCenter) << std::endl;

        std::cout << "realTorsoPosition: "
                  << glm::to_string(realTorsoPosition) << std::endl;
#endif

        realTorsoTransform =
                glm::translate(glm::mat4(1.0f), realTorsoPosition) *
                realTorsoOrientation;

        for(auto & element : realTorsoTransform)
            assert(glm::isnan(element) == false);

        //torsoTransformString = getMatlabString(realTorsoTransform);

    }

}

void PostProcessing::handleBodyFront(glm::vec3 & realTorsoPosition,
                     glm::mat4 & realTorsoOrientation) {
    realTorsoPosition =
            realFrontTransform *
            glm::vec4(offsetTorsoFrontToCenter, 1.);

    realTorsoOrientation = glm::mat3(realFrontTransform);
}


void PostProcessing::handleBodyBack(glm::vec3 & realTorsoPosition,
                    glm::mat4 & realTorsoOrientation) {
    realTorsoPosition =
            realBackTransform *
            glm::vec4(offsetTorsoBackToCenter, 1.);

    realTorsoOrientation = glm::mat3(realBackTransform);
}

void PostProcessing::handleBodyBoth(glm::vec3 & realTorsoPosition,
                    glm::mat4 & realTorsoOrientation) {
    realTorsoPosition = 0.5f *
                        (realFrontTransform[3] + realBackTransform[3]);

    glm::quat torsoFrontOrientationQuat =
            glm::quat_cast(realFrontTransform);
    glm::quat torsoBackOrientationQuat =
            glm::quat_cast(realBackTransform);

    if(glm::dot(torsoFrontOrientationQuat,
                torsoBackOrientationQuat) < 0.f)
        torsoFrontOrientationQuat = -torsoFrontOrientationQuat;

    glm::quat realTorsoOrientationQuat = glm::mix(
            torsoFrontOrientationQuat, torsoBackOrientationQuat, 0.5f);

    realTorsoOrientation = glm::mat4_cast(realTorsoOrientationQuat);
}

float PostProcessing::angleAroundGlobalY(glm::mat3 transform) {
    glm::vec3 xdir(1, 0, 0);
    glm::vec3 znegdir(0, 0, -1);

    glm::vec3 xdirTrans = transform * xdir;
    glm::vec3 znegdirTrans = transform * znegdir;

    float angleYaw = 0;
    if(glm::length(xdirTrans) > 0.001) {
        angleYaw = atan2(-xdirTrans.z, xdirTrans.x);
    }
    else {
        angleYaw = atan2(-znegdirTrans.x, -znegdirTrans.z);
    }

    return angleYaw;
}

float PostProcessing::angleAroundLocalX(glm::mat3 transform) {
    glm::vec3 ydir(0, 1, 0);
    glm::vec3 znegdir(0, 0, -1);
    glm::vec3 znegdirTrans = transform * znegdir;

    float angleYaw = angleAroundGlobalY(transform);

    glm::vec3 cleanYaw = glm::rotateY(znegdir, angleYaw);

    float a = glm::dot(cleanYaw, znegdirTrans);
    float b = glm::dot(ydir, znegdirTrans);

    return(atan2(b, a));
}

glm::mat4 PostProcessing::correctBodyTransform(glm::mat4 transform) {
    for(int entry = 0 ; entry < 3 ; ++entry) {
        transform[3][entry] /= 1000.0f;
    }
    transform[3] += glm::vec4(4, 0, -4, 0);

    return transform;
}

glm::mat4 PostProcessing::cleanMatrix(glm::mat4 matrix) {
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(matrix,
                   scale, rotation, translation, skew, perspective);

    glm::mat4 result = glm::toMat4(rotation);
    result[3] = glm::vec4(translation, 1);

    for(int col = 0 ; col < 3 ; ++col) {
        result[col] = glm::normalize(result[col]);
    }

    return result;
}

glm::mat4 PostProcessing::ReadMat4(std::string strMatrix) {
    glm::mat4 mat;

    // remove [ ] brackets
    strMatrix.erase(0, 1);
    strMatrix.erase(strMatrix.length() - 1, 1);

    std::istringstream ssMatrix(strMatrix);
    std::string strRow;
    for(int row = 0 ; row < 4 ; ++row) {
        std::getline(ssMatrix, strRow, ';');
        strRow.erase(0, 1);

        std::string strEntry;
        std::istringstream ssRow(strRow);
        for(int col = 0 ; col < 4 ; ++col) {
            ssRow >> std::setprecision(std::numeric_limits<double>::digits10 + 1)
                  >> mat[col][row];
        }
    }

    return mat;
}

float PostProcessing::ReadFloat(std::string strFloat) {
    float f;

    std::istringstream ssFloat(strFloat);
    ssFloat >> std::setprecision(std::numeric_limits<double>::digits10 + 1)
                  >> f;

    return f;
}
