#version 300 es

precision highp float;
precision highp sampler2D;

uniform sampler2D uColorTexture;

in vec2 vPosition;

out vec3 fColor;

void main() {
    vec4 color = texelFetch(uColorTexture, ivec2(gl_FragCoord.xy), 0);
    if (color.a < 0.01) discard;
    fColor = color.rgb / color.a;
}