#version 300 es

in vec3 aPosition;
in vec3 aNormal;
in float aRadius;
in vec3 aColor;

out vec3 vColor;
out vec4 vEyeNormal;
out vec3 vEyePosition;
out float vRadius;

uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

uniform vec4 vc;

void main()
{
    // Transform center to eye coords
    vEyePosition = (uViewMatrix * vec4( aPosition, 1.0 )).xyz;

    // pass normal
    vec3 normal = aNormal;

    // Compute radius from length of normal and scale radius according to global scaling factor
    float radius = min(aRadius * vc.z, -64.0*vEyePosition.z/vc.x);
    vRadius = radius;

    // compute square to save a root operation in fragment shader
    vRadius = vRadius*vRadius;

    // Transform normal to eye coords
    vec3 n = mat3(uViewMatrix) * normal;
    vEyeNormal = vec4( n, dot( vEyePosition,n ) );

    // Transform center to NDC
    gl_Position = uProjectionMatrix * vec4(vEyePosition, 1.0 );

    // Compute point size: at least vc.w pixels
    gl_PointSize = max(vc.x * radius / (-vEyePosition.z), vc.w);

    vColor = aColor;
}
