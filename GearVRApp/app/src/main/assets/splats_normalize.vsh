#version 300 es

in vec2 aPosition;

out vec2 vPosition;

void main() {
    vPosition = aPosition;
    vec2 pos = (aPosition - 0.5) * 2.0;
    gl_Position = vec4(pos, 0.0, 1.0);
}
