precision mediump float;
precision mediump sampler2D;

varying vec3 Normal;
varying vec2 TexCoord0;
varying vec2 TexCoord1;

uniform sampler2D Texture;
uniform sampler2D Texture2; //Lightmap
uniform vec4 uColor;

void main()
{
    vec3 normal = normalize(Normal);
    vec3 lightDir = vec3(1.0, -1.0, -1.0);
    lightDir = normalize(lightDir);
    vec3 lightColor = vec3(0.5, 0.5, 0.5);

    vec3 light = vec3(0.2,0.2,0.2); //ambient
    vec3 diffuse = max(0.0, dot(-lightDir, normal)) * lightColor;
    light += diffuse;
    gl_FragColor =  texture2D(Texture, TexCoord0) * vec4(uColor.rgb * light, uColor.a);
}