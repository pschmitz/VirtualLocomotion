precision mediump float;
precision mediump sampler2D;

varying vec3 Normal;
varying vec2 TexCoord0;
varying vec2 TexCoord1;

uniform sampler2D Texture;
uniform sampler2D Texture2; //Lightmap

void main()
{
    gl_FragColor = vec4(texture2D(Texture, TexCoord1).xyz,1);
    gl_FragColor.rgb *= texture2D(Texture2, TexCoord0).rgb;
}