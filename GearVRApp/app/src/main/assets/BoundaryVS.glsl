attribute vec3 position;
attribute vec3 normal;
attribute vec2 texcoord;

varying vec3 perPixelNormal;
varying vec2 perPixelTexCoord;
varying vec3 perPixelWorldPosition;

uniform mat4 uTransformMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;
uniform vec3 uCameraPosition;

void main()
{
	perPixelNormal = (uTransformMatrix * vec4(normal,0)).xyz;
	perPixelTexCoord = texcoord;
	gl_Position = vec4(position,1);

	gl_Position = uTransformMatrix * gl_Position;
	perPixelWorldPosition = gl_Position.xyz;
	gl_Position = uViewMatrix * gl_Position;
	gl_Position = uProjectionMatrix * gl_Position;
}