#version 330

#include "include/lighting.glsl"
#include "include/camera.glsl"
uniform bool uRenderForCube;


const vec3 v3InvWavelength  = vec3(1.0 / pow(0.750, 4.0), 1.0 / pow(0.570, 4.0), 1.0 / pow(0.475, 4.0));

const float fOuterRadius = 6452.103598913;
const float fInnerRadius = 6356.7523142;
const float fKrESun = 0.00045 * 20.0;
const float fKmESun = 0.0015 * 20.0;
const float fScale  = 1.0 / (fOuterRadius - fInnerRadius);
const float fScaleDepth = 0.25;
const int nSamples = 4;

in vec3 vPosition;

out vec3 fColor;

float scale(float fCos)
{
    float x = 1.0 - fCos;
    return fScaleDepth * exp(-0.00287 + x*(0.459 + x*(3.83 + x*(-6.80 + x*5.25))));
}

void main (void)
{
    float fOuterRadius2 = fOuterRadius * fOuterRadius;
    float fInnerRadius2 = fInnerRadius * fInnerRadius;
    float fKr4PI = fKrESun * 4.0 * 3.141592653;
    float fKm4PI = fKmESun * 4.0 * 3.141592653;
    float fScaleOverScaleDepth = fScale / fScaleDepth;

    vec3 camera   = uCameraPosition / 1000 +  vec3(0,0, fInnerRadius);
    vec3 V = normalize(vPosition);

    float d = dot(V, camera);
    vec3 projected = camera - d * V;
    float projLen2 = dot(projected, projected);

    float fFar;
    vec3 v3FrontColor = vec3(0.0, 0.0, 0.0);
    if (fInnerRadius2 > projLen2 && d < 0.0 && uRenderForCube) {
        // Ray hits the ground
        vec3 onGround = projected - V * sqrt(fInnerRadius2 - projLen2);
        fFar = length(camera - onGround);
        //v3FrontColor = uSunColor * vec3(0.55, 0.7, 0.07) * uSunDirection.z;
    } else {
        // Ray leaves the atmosphere
        vec3 onAtmo = projected + V * sqrt(fOuterRadius2 - projLen2);
        fFar = length(camera - onAtmo);
    }

    vec3 v3Start = camera;
    float fHeight = length(camera);
    float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fHeight));
    float fStartAngle = dot(V, v3Start) / fHeight;
    float fStartOffset = fDepth*scale(fStartAngle);

    float fSampleLength = fFar / float(nSamples);
    float fScaledLength = fSampleLength * fScale;
    vec3 v3SampleRay = V * fSampleLength;
    vec3 v3SamplePoint = v3Start + v3SampleRay * 0.5;

    for(int i=0; i<nSamples; i++)
    {
        float fHeight = length(v3SamplePoint);
        float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fHeight));
        float fLightAngle = dot(uSunDirection, v3SamplePoint) / fHeight;
        float fCameraAngle = abs(dot(V, v3SamplePoint) / fHeight);
        float fScatter = fStartOffset + fDepth*(scale(fLightAngle) - scale(fCameraAngle));
        vec3 v3Attenuate = exp(-fScatter * (v3InvWavelength * fKr4PI + fKm4PI));
        v3FrontColor += v3Attenuate * (fDepth * fScaledLength);
        v3SamplePoint += v3SampleRay;
    }

    fColor = v3InvWavelength * v3FrontColor * fKmESun;

    float g = 0.990;
    float g2 = g * g;
	float fCos = dot(uSunDirection, V);
	float fMiePhase = 1.5 * ((1.0 - g2) / (2.0 + g2)) * (1.0 + fCos*fCos) / pow(1.0 + g2 - 2.0*g*fCos, 1.5);

    if (!uRenderForCube)
    {
	    fColor += fMiePhase * v3FrontColor * fKmESun;
    } else {
        fColor /= 30;
    }
}
