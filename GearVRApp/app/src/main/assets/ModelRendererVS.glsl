
attribute vec3 VertexPosition;
attribute vec3 VertexNormal;
attribute vec2 VertexTexCoord0;
attribute vec2 VertexTexCoord1;

varying vec3 Normal;
varying vec2 TexCoord0;
varying vec2 TexCoord1;

uniform mat4 uMvpMatrix;
uniform mat4 uNormalMatrix;

void main()
{
    gl_Position = uMvpMatrix * vec4(VertexPosition,1);
    Normal = (uNormalMatrix * vec4(VertexNormal, 0)).xyz;
    TexCoord0 = VertexTexCoord0;
    TexCoord1 = VertexTexCoord1;
}