#version 300 es

precision highp float;
precision highp sampler2D;

in vec3  vColor;
in vec4  vEyeNormal;
in vec3  vEyePosition;
in float vRadius;

layout(location=0) out vec4 fColor;

//out vec4 FragDataNormal;
//out vec4 FragDataColor;
//out vec4 FragDataDepth;

uniform float uNearPlane;
uniform vec4 uPix2Ray;

void main( void )
{
    // Viewing ray to fragment
    vec3 R0 = vec3(uPix2Ray.x * gl_FragCoord.x + uPix2Ray.y, uPix2Ray.z * gl_FragCoord.y + uPix2Ray.w, -1.0);

    // pass normal & color
    //if ( dot( R0, vEyeNormal.xyz ) > 0.0 )
    //    FragDataNormal.xyz = -vEyeNormal.xyz;
    //else
    //    FragDataNormal.xyz = vEyeNormal.xyz;

    // Get intersection with splat: R0
    R0 = R0 * vEyeNormal.w / dot(vEyeNormal.xyz, R0);

    // Depth correction (w-buffer)
    // Beware: far plane at infinity
    float depth = 1.0 + uNearPlane / R0.z;
    //FragDataDepth.xyz = vec3(fzb.y * R0.z + fzb.x);
    gl_FragDepth = depth;


    // discard fragment if outside radius
    vec3 axis = R0 - vEyePosition;
    float r2 = dot( axis,axis );
    if ( r2 > vRadius )
        discard;

    float opacity = 1.0 - r2 / vRadius + 0.01;
    fColor = vec4(vColor, opacity);
}
