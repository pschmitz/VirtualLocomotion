precision mediump float;

varying vec2 TexCoord;
varying vec4 Color;
varying float TextureId;

uniform sampler2D Textures[8];

void main()
{
	for(int i = 0; i < 8; i++)
	{
		if(TextureId == float(i))
		{
			gl_FragData[0] = texture2D(Textures[i], TexCoord) * Color;
			//gl_FragData[0] = vec4(TexCoord,0,1);
			return;
		}
	}

	gl_FragData[0] = Color;
}