precision mediump float;
precision mediump sampler2D;

varying vec3 Normal;
varying vec2 TexCoord0;
varying vec2 TexCoord1;

uniform sampler2D Texture;
uniform sampler2D Texture2;

void main()
{
    vec4 color = texture2D(Texture, TexCoord0);
    if(color.a < 0.5)
    {
        discard;
    }
    gl_FragColor = vec4(color.rgb,1);
}