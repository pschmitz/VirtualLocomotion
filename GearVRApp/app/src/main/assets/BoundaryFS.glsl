precision highp float;
precision highp sampler2D;

varying vec3 perPixelNormal;
varying vec2 perPixelTexCoord;
varying vec3 perPixelWorldPosition;

uniform vec3 uCameraPosition;
uniform vec3 uColor;

uniform sampler2D tex;
uniform bool uBoundaryDebug;

void main()
{
    vec3 diff = perPixelWorldPosition - uCameraPosition;
    float distance = length(diff);

    if (uBoundaryDebug) {
        gl_FragColor.a = 1.0;
    } else {
        gl_FragColor.a = clamp(1.0 - distance/0.5, 0.0, 1.0);
    }

    gl_FragColor.rgb = texture2D(tex, perPixelTexCoord).rgb;
    gl_FragColor.rgb *= uColor;
}