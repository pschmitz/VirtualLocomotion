attribute vec2 VertexPosition;
attribute vec2 VertexTexCoord;
attribute vec4 VertexColor;
attribute float VertexTextureId;

uniform vec2 ViewportSize;

varying vec2 TexCoord;
varying vec4 Color;
varying float TextureId;

vec2 Projection(vec2 position)
{
	position.x /= ViewportSize.x / 2.0;
	position.y /= ViewportSize.y / 2.0;
	position.y *= -1.0;
	position.xy += vec2(-1, 1);
	return position;
}

void main()
{
	gl_Position = vec4(Projection(VertexPosition),0,1);
	TexCoord = VertexTexCoord;
	Color = VertexColor;
	TextureId = VertexTextureId;
}