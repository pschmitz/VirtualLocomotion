//
// Created by Benni on 07.12.2016.
//

#include "Parser.h"

void ParseMessage(void *buffer, int length, unsigned &frameNr,
                  std::vector<body> &bodies, std::vector<marker> &markers)
{
    char *bufferReader = (char*)buffer;

    frameNr = *(unsigned*)bufferReader;
    bufferReader += sizeof(unsigned);

    int numberOfBodies = *(int*)bufferReader;
    bufferReader += sizeof(int);

    for (int i = 0; i < numberOfBodies; ++i)
    {
        body readBody = *(body*)bufferReader;
        bufferReader += sizeof(body);
        bodies.push_back(readBody);
    }

    /*
    int numberOfMarkers = *(int*)bufferReader;
    bufferReader += sizeof(int);

    for (int i = 0; i < numberOfMarkers; ++i)
    {
        marker readMarker = *(marker*)bufferReader;
        bufferReader += sizeof(marker);
        markers.push_back(readMarker);
    }*/
}
