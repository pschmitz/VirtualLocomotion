#include <sys/socket.h>
#include <unistd.h>

#include <array>
#include <vector>
#include <iostream>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "UserKinematic.h"

#include "Parser.h"

#include "UdpReceiver.h"
#include "TcpReceiver.h"
#include "UdpSender.h"
#include "ViewerUDPStreamer.h"

#include "DataCaptureManager.h"


//=== === === === === === == === === === === === ==== === === === === === == === === === === === ==== === === === === === == === === === === === =
//===

#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include <vector>

#include <glm/ext.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#include "datareader.h"

#define DEBUG_OUTPUT 0

std::string getCleanedStringFromDataLine(std::string line);

//===
//=== === === === === === == === === === === === ==== === === === === === == === === === === === ==== === === === === === == === === === === === =


std::array<char, 0x10000> buffer;

const glm::vec3 sceneOffsetOrigin(-4.f, 0.f, 4.f);

glm::vec3 currentTrackingPosition = glm::vec3(0, 0, 0);
glm::mat4 currentTrackingOrientation = glm::mat4(1.0f);
unsigned lastFrameNr = 0;
bool hasCompleteBody = false;

glm::mat4 torsoTransforms[2] = {glm::mat4(1.0f), glm::mat4(1.0f)};

UserKinematic userKinematic;
bool lastIsInitialized = false;
glm::mat4 additionalSceneRotation = glm::mat4(1.0f);

void processTrackingPackage(void *buffer, int length)
{
    int header0 = *((int*)buffer);
    int header1 = *(((int*)buffer) + 1);
    int header2 = *(((int*)buffer) + 2);
    if(header0 == 42 && header1 == 0 && header2 == 42)
    {
        // DebugControlMessage *messagePtr = ((DebugControlMessage *)(((char*)buffer) + sizeof(int)*3));
        // DebugControlMessage message = messagePtr[0];
//        adjustableEyeOffset = message.adjustableEyeOffset;
//        rotationCenterOffset = message.rotationCenterOffset;
    }

    unsigned frameNr;
    std::vector<body> bodies;
    std::vector<marker> markers;
    ParseMessage(buffer, length, frameNr, bodies, markers);

    if(lastFrameNr < frameNr || (lastFrameNr - frameNr) > 1000)
    {
        hasCompleteBody = false;

        if (bodies.size() >= 1)
        {
            for(auto & body : bodies) {
//                std::cout << "received tracking body " << body.id << std::endl;
                if(body.id == 0) {
                    currentTrackingPosition.x =
                        body.loc[0] / 1000.0f - sceneOffsetOrigin.x;
                    // correct for 43mm tracking 0 offset
                    currentTrackingPosition.y =
                        (body.loc[1] - 43) / 1000.0f - sceneOffsetOrigin.y;
                    currentTrackingPosition.z =
                        body.loc[2] / 1000.0f - sceneOffsetOrigin.z;

                    //Set columes
                    currentTrackingOrientation[0]= glm::vec4(body.rot[0], body.rot[1], body.rot[2], 0);
                    currentTrackingOrientation[1]= glm::vec4(body.rot[3], body.rot[4], body.rot[5], 0);
                    currentTrackingOrientation[2]= glm::vec4(body.rot[6], body.rot[7], body.rot[8], 0);
                } else if (body.id <= 2) {
                    // One of 2 Torso Trackers

                    auto& trans = torsoTransforms[body.id - 1];
                    trans = glm::mat4(0.0f);
                    trans[0] = glm::vec4(body.rot[0], body.rot[1], body.rot[2], 0);
                    trans[1] = glm::vec4(body.rot[3], body.rot[4], body.rot[5], 0);
                    trans[2] = glm::vec4(body.rot[6], body.rot[7], body.rot[8], 0);
                    trans[3] = glm::vec4(body.loc[0], body.loc[1], body.loc[2], 1);
                }
            }

            hasCompleteBody = true;
        }

        lastFrameNr = frameNr;
    }
}


//void computeKinematic()
//{
//    glm::vec3 realHeadPositionOld = userKinematic.getRealHeadPosition();
//    glm::vec3 realHeadPositionOffset =
//        currentTrackingPosition - realHeadPositionOld;

////    userKinematic.setAngularVelocityHead(

//    glm::vec3 oldDirection =
//            glm::vec3(userKinematic.getRealHeadTransform() *
//                      glm::vec4(1, 0, 0, 0));

//    userKinematic.setRealHeadTransform(
//        glm::translate(glm::mat4(1.0f),
//                       currentTrackingPosition) *
//        currentTrackingOrientation);

//    // std::cout << "realHeadTransform: "
//    //           << glm::to_string(userKinematic.getRealHeadTransform())
//    //           << std::endl;

//    if (lastIsInitialized)
//    {
//        glm::vec3 newDirection =
//                glm::vec3(userKinematic.getRealHeadTransform() *
//                          glm::vec4(1, 0, 0, 0));

//        // std::cout << "oldDirection: " << glm::to_string(oldDirection)
//        //           << std::endl;
//        // std::cout << "newDirection: " << glm::to_string(newDirection)
//        //           << std::endl;

//        oldDirection.y = 0.f;
//        newDirection.y = 0.f;
//        oldDirection = glm::normalize(oldDirection);
//        newDirection = glm::normalize(newDirection);

//        float angleOld = atan2(-oldDirection.z, oldDirection.x);
//        float angleNew = atan2(-newDirection.z, newDirection.x);

//        // float angle =
//        //     acos(glm::clamp(glm::dot(oldDirection, newDirection), -1.f, 1.f));
////        float angle = 0.f;

//        float angle = angleNew - angleOld;
//        // std::cout << "angle: " << angle << std::endl;

//        float additionalRotationAngle =
////            userKinematic.getAngularVelocityHead().y * (frameTime /
////            1000.0) *
//            angle * (userKinematic.getRotationGain() - 1.f);

////        std::cout << "angle: " << angle << std::endl;

//        additionalSceneRotation =
//            additionalSceneRotation *
//            glm::rotate(glm::mat4(1.0f),
//                        additionalRotationAngle, glm::vec3(0, 1, 0));
//    }

//    glm::vec3 virtualHeadPositionOld =
//        glm::vec3(userKinematic.getVirtualHeadTransform() *
//                  glm::vec4(0, 0, 0, 1));
//    glm::vec3 virtualHeadPositionOffset =
//        glm::vec3(additionalSceneRotation *
//                  glm::vec4(realHeadPositionOffset, 1));

//    userKinematic.setVirtualHeadTransform(
//        glm::translate(glm::mat4(1.f),
//                       virtualHeadPositionOld + virtualHeadPositionOffset) *
//        additionalSceneRotation * currentTrackingOrientation);

//    lastIsInitialized = true;
//}

void computeKinematic(double frameTime)
{
    IVrRenderInterface *vrRenderInterface =
        this->App->GetVrRenderInterface();
    VrapiRenderInterface *vrapiRenderInterface =
        dynamic_cast<VrapiRenderInterface*>(vrRenderInterface);

    const ovrHeadModelParms headModelParms = vrapi_DefaultHeadModelParms();
    ovrTracking *tracking = vrapiRenderInterface->GetOvrTracking();

    glm::vec3 realHeadPositionOld =
        userKinematic.getRealHeadTransform() * glm::vec4(0, 0, 0, 1);
    glm::vec3 realHeadPositionOffset =
        currentTrackingPosition - realHeadPositionOld;

    userKinematic.setMovementVelocityHead(
        (realHeadPositionOffset) / float(frameTime / 1000.0));
    userKinematic.setAngularVelocityHead(
        glm::mat3(glm::toMat4(toGlm(tracking->HeadPose.Pose.Orientation))) *
        toGlm(tracking->HeadPose.AngularVelocity));

    glm::mat4 realHeadOrientation =
        lastRecenterPose *
        glm::toMat4(toGlm(tracking->HeadPose.Pose.Orientation));
    userKinematic.setRealHeadTransform(
        glm::translate(glm::mat4(1.0f),
                       currentTrackingPosition) *
        realHeadOrientation);

    if (lastIsInitialized)
    {
        float realAngle = userKinematic.getAngularVelocityHead().y * (frameTime / 1000.0);
        float additionalRotationAngle = realAngle * (userKinematic.getRotationGain() - 1.f);

        totalRealRotation += std::abs(realAngle);
        totalVirtualRotation += std::abs(realAngle * userKinematic.getRotationGain());
        totalWalkedDistance += glm::length(realHeadPositionOffset);

        additionalSceneRotation =
            additionalSceneRotation *
            glm::rotate(glm::mat4(1.0f),
                        additionalRotationAngle, glm::vec3(0, 1, 0));
    }

    glm::vec3 virtualHeadPositionOld =
        userKinematic.getVirtualHeadTransform() * glm::vec4(0, 0, 0, 1);
    glm::vec3 virtualHeadPositionOffset =
        additionalSceneRotation *
        glm::vec4(realHeadPositionOffset, 1);

    userKinematic.setVirtualHeadTransform(
        glm::translate(glm::mat4(1.f),
                       virtualHeadPositionOld + virtualHeadPositionOffset) *
        additionalSceneRotation * realHeadOrientation);

    lastIsInitialized = true;
}


int main() {
    UdpReceiver receiverTracking(1235);
    TcpReceiver receiverTransform(4200);
    // ViewerUDPStreamer viewerToBlender;

    // viewerToBlender.Connect("localhost", 1337);

    UdpSender senderBlender("coudenhove", 1337);
    UdpSender senderOpenAL("coudenhove", 1338);

//    int fdTcp = accept(receiverTransform.mSocket, NULL, NULL);
    int fdTcp = -1;

    std::cout << "receiverTracking.mSocket: "
              << receiverTracking.mSocket << std::endl;
    std::cout << "fdTcp: " << fdTcp << std::endl;

    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(receiverTracking.mSocket, &rfds);
    FD_SET(fdTcp, &rfds);

    DataCaptureManager dataCaptureMgr;

    while(true) {
        fd_set dup = rfds;
        select(FD_SETSIZE, &dup, NULL, NULL, NULL);

        if(FD_ISSET(receiverTracking.mSocket, &dup)) {
            auto bytes_received = read(receiverTracking.mSocket,
                                       buffer.data(), sizeof(buffer));
            if (bytes_received > 0) {
//                std::cout << "tracking data received" << std::endl;
                processTrackingPackage(buffer.data(), bytes_received);
                computeKinematic(1.0);

                // ===

                std::string dataAsString = dataCaptureMgr.FormattedStringForUserKinematicData(
                            lastFrameNr,
                            userKinematic,
                            torsoTransforms,
                            0.0f,
                            0.0f,
                            0.0f);


                std::cout << getCleanedStringFromDataLine(dataAsString) << std::endl;

                // ===

//                glm::mat4 sendTransform =
//                    userKinematic.getVirtualHeadTransform();
//                ssize_t bytes =
//                    send(senderOpenAL.fd, glm::value_ptr(sendTransform), 64, 0);

//                glm::mat4 rotX90 = glm::axisAngleMatrix(glm::vec3(1, 0, 0),
//                                                        glm::radians(90.0f));
//                sendTransform =
//                    rotX90 * userKinematic.getVirtualHeadTransform();
//                bytes =
//                    send(senderBlender.fd, glm::value_ptr(sendTransform), 64, 0);

                // if(bytes < 0) {
                //     perror("write: ");
                // }
                // else
                //     std::cout << bytes << " written" << std::endl;
            }
        }
//        if(FD_ISSET(fdTcp, &dup)) {
//            auto bytes_received = read(fdTcp, buffer.data(), sizeof(buffer));
//            if (bytes_received > 0) {
//                if(bytes_received == 4) {
//                    userKinematic.setRotationGain(*(float*)(buffer.data()));
//                    std::cout << "gain: " << userKinematic.getRotationGain()
//                              << std::endl;
//                }
//                if(bytes_received == 64) {
//                    glm::mat4 virtualHeadTransform;
//                    memcpy(glm::value_ptr(virtualHeadTransform),
//                           buffer.data(), 64);
//                    std::cout << "recv: "
//                              << glm::to_string(virtualHeadTransform)
//                              << std::endl;
//                    std::cout << "comp: "
//                              << glm::to_string(userKinematic.getVirtualHeadTransform())
//                              << std::endl;
//                    std::cout << "diff: "
//                              << glm::to_string(virtualHeadTransform -
//                                                userKinematic.getVirtualHeadTransform())
//                              << std::endl;
//                    userKinematic.setVirtualHeadTransform(virtualHeadTransform);

//                    additionalSceneRotation =
//                        glm::inverse(userKinematic.getRealHeadTransform()) *
//                        virtualHeadTransform;
//                    additionalSceneRotation[3] = glm::vec4(0, 0, 0, 1);
//                }
//            }
//        }
    }
}




//=== === === === === === == === === === === === ==== === === === === === == === === === === === ==== === === === === === == === === === === === =
//=== === === === === === == === === === === === ==== === === === === === == === === === === === ==== === === === === === == === === === === === =
//=== === === === === === == === === === === === ==== === === === === === == === === === === === ==== === === === === === == === === === === === =
//=== === === === === === == === === === === === ==== === === === === === == === === === === === ==== === === === === === == === === === === === =
//=== === === === === === == === === === === === ==== === === === === === == === === === === === ==== === === === === === == === === === === === =

glm::mat4 realHeadTransform;
glm::mat4 realFrontTransform;
glm::mat4 realBackTransform;
glm::mat4 realTorsoTransform;

glm::mat4 realHeadTransformLast;
glm::mat4 realFrontTransformLast;
glm::mat4 realBackTransformLast;

glm::vec3 offsetTorsoFrontToCenter;
glm::vec3 offsetTorsoBackToCenter;

glm::mat4 correctBodyTransform(glm::mat4 transform);
glm::mat4 cleanMatrix(glm::mat4 matrix);


DataReader reader;

bool hasValidTorsoTransform; //false, if torso transformation is UNDEFINED

float relativeAngleHeadTorso;


void cutOffLinesAfterLastTargetCollected();

void insertTorsoTransformAndRelativeAngleAndCleanData();

void computeTorsoTransform();
void handleBodyFront(glm::vec3 & realTorsoPosition,
                     glm::mat4 & realTorsoOrientation);
void handleBodyBack(glm::vec3 & realTorsoPosition,
                     glm::mat4 & realTorsoOrientation);
void handleBodyBoth(glm::vec3 & realTorsoPosition,
                     glm::mat4 & realTorsoOrientation);

float angleAroundGlobalY(glm::mat3 transform);

std::string getMatlabString(glm::mat4 v);

//int main() {

//    //cutOffLinesAfterLastTargetCollected();
//    insertTorsoTransformAndRelativeAngleAndCleanData();

//    return 0;
//}

void cutOffLinesAfterLastTargetCollected() {
    std::ifstream file( "testfile2" );
    std::ofstream fileOut( "testfile2.out" );

    if(file) {

        std::vector<std::string> allLines;

        std::string line;
        while(std::getline(file, line)) {
            allLines.push_back(line);
            //std::cout << line << std::endl;
        }
        file.close();

        std::string currentLastLine = allLines.back();
        while (currentLastLine.substr(0, 16).compare("TARGET_COLLECTED") != 0) {
            allLines.pop_back();
            currentLastLine = allLines.back();
//            std::cout << currentLastLine<< std::endl;
        }

//        allLines.pop_back();
//        std::cout << allLines.back() << std::endl;
//        allLines.pop_back();
//        std::cout << allLines.back() << std::endl;


        for (std::vector<std::string>::iterator it = allLines.begin() ; it != allLines.end(); ++it) {
            fileOut << *it << std::endl;
       }

        fileOut.close();
    } else {
        std::cout << "Record file not found." << std::endl;
    }
}



void computeRelativeAngleHeadToTorso() {

    if (hasValidTorsoTransform) {
        glm::mat4 relativeTransformation = realHeadTransform
                * glm::inverse(realTorsoTransform);

        relativeAngleHeadTorso = angleAroundGlobalY(glm::mat3(relativeTransformation));
    }

}

std::string getCleanedStringFromDataLine(std::string line) {
    std::istringstream ssLine(line);

    std::string token;
    std::getline(ssLine, token, ',');
    std::getline(ssLine, token, ',');
    std::getline(ssLine, token, ',');
    std::getline(ssLine, token, ',');
    std::getline(ssLine, token, ',');

    realHeadTransform = reader.ReadMat4(token);

    if(realHeadTransform[3] == realHeadTransformLast[3])
        return "";

    std::getline(ssLine, token, ',');
    std::getline(ssLine, token, ',');
    std::getline(ssLine, token, ',');

    realFrontTransform = reader.ReadMat4(token);
    realFrontTransform = correctBodyTransform(realFrontTransform);

    std::getline(ssLine, token, ',');

    realBackTransform = reader.ReadMat4(token);
    realBackTransform = correctBodyTransform(realBackTransform);

#if DEBUG_OUTPUT
    std::cout << "+++++++++++++++++++++++++++++++" << std::endl;
    std::cout << "realHeadTransform" << std::endl;
    std::cout << glm::to_string(realHeadTransform) << std::endl;

    std::cout << "realFrontTransform" << std::endl;
    std::cout << glm::to_string(realFrontTransform) << std::endl;

    std::cout << "realBackTransform" << std::endl;
    std::cout << glm::to_string(realBackTransform) << std::endl;
#endif

    computeTorsoTransform();

#if DEBUG_OUTPUT
    std::cout << "realTorsoTransform" << std::endl;
    std::cout << glm::to_string(realTorsoTransform) << std::endl;
#endif

    computeRelativeAngleHeadToTorso();

#if DEBUG_OUTPUT
    std::cout << "relativeAngleHeadTorso" << std::endl;
    std::cout << relativeAngleHeadTorso << std::endl;
#endif

    realHeadTransformLast = realHeadTransform;
    realFrontTransformLast = realFrontTransform;
    realBackTransformLast = realBackTransform;

    std::string torsoTransformString = "UNDEFINED";
    std::string relativeAngleString = "UNDEFINED";
    if(hasValidTorsoTransform) {
        torsoTransformString = getMatlabString(realTorsoTransform);
        relativeAngleString = std::to_string(relativeAngleHeadTorso);
    }

    std::string lineOut =
        getMatlabString(realHeadTransform) + "," +
        getMatlabString(realFrontTransform) + "," +
        getMatlabString(realBackTransform) + "," +
        torsoTransformString + "," +
        relativeAngleString + ",";

    return lineOut;
}


void insertTorsoTransformAndRelativeAngleAndCleanData()
{
    std::ifstream file( "testfile" );
    std::ofstream fileOut( "testfile.out" );

    if(file) {

        std::string line;
        while(std::getline(file, line)) {

            std::string lineOut = getCleanedStringFromDataLine(line);
            if (lineOut.length() > 0) {
                fileOut << lineOut << std::endl;
            }
        }

        file.close();
    } else {
        std::cout << "Record file not found." << std::endl;
    }
}

std::string getMatlabString(glm::mat4 v) {
    std::stringstream stringStream;
    stringStream << std::setprecision(std::numeric_limits<double>::digits10 + 1)
                 << "[ "
                 << v[0][0] << " " << v[1][0] << " " << v[2][0] << " " << v[3][0] << "; "
                 << v[0][1] << " " << v[1][1] << " " << v[2][1] << " " << v[3][1] << "; "
                 << v[0][2] << " " << v[1][2] << " " << v[2][2] << " " << v[3][2] << "; "
                 << v[0][3] << " " << v[1][3] << " " << v[2][3] << " " << v[3][3] << "]";
    return stringStream.str();
}

void computeTorsoTransform() {
    glm::vec3 realTorsoPosition;
    glm::mat4 realTorsoOrientation;

    bool hasBodyTorsoFront = false;
    bool hasBodyTorsoBack = false;

    if(realFrontTransform != realFrontTransformLast)
        hasBodyTorsoFront = true;

    if(realBackTransform != realBackTransformLast)
        hasBodyTorsoBack = true;

    // instead skip first line in main loop?
    static unsigned int skippedFrames = 0;
    if(skippedFrames < 2) {
        hasBodyTorsoFront = false;
        hasBodyTorsoBack = false;
        skippedFrames ++;
    }

    hasValidTorsoTransform = (hasBodyTorsoBack || hasBodyTorsoFront);

#if DEBUG_OUTPUT
    std::cout << std::boolalpha
              << "hasValidTorsoTransform: " << hasValidTorsoTransform << std::endl;
#endif

    if (hasValidTorsoTransform) {
        //If no torso body is tracked for the current frame, skip the calculation.

        if(!hasBodyTorsoBack && hasBodyTorsoFront) {
            handleBodyFront(realTorsoPosition, realTorsoOrientation);
        }
        else if(!hasBodyTorsoFront && hasBodyTorsoBack) {
            handleBodyBack(realTorsoPosition, realTorsoOrientation);
        }
        else if(hasBodyTorsoFront && hasBodyTorsoBack) {
            handleBodyBoth(realTorsoPosition, realTorsoOrientation);
        }

        if (hasBodyTorsoFront) {
            offsetTorsoFrontToCenter =
                glm::transpose(glm::mat3(realFrontTransform)) *
                (realTorsoPosition - glm::vec3(realFrontTransform[3]));
        }

        if (hasBodyTorsoBack) {
            offsetTorsoBackToCenter =
                glm::transpose(glm::mat3(realBackTransform)) *
                (realTorsoPosition - glm::vec3(realBackTransform[3]));
        }

#if DEBUG_OUTPUT
        std::cout << std::boolalpha
                  << "hasBodyTorsoFront: " << hasBodyTorsoFront << std::endl;
        std::cout << std::boolalpha
                  << "hasBodyTorsoBack: " << hasBodyTorsoBack << std::endl;

        std::cout << "offsetTorsoFrontToCenter: "
                  << glm::to_string(offsetTorsoFrontToCenter) << std::endl;
        std::cout << "offsetTorsoBackToCenter: "
                  << glm::to_string(offsetTorsoBackToCenter) << std::endl;

        std::cout << "realTorsoPosition: "
                  << glm::to_string(realTorsoPosition) << std::endl;
#endif

        realTorsoTransform =
            glm::translate(glm::mat4(1.0f), realTorsoPosition) *
            realTorsoOrientation;

        for(auto & element : realTorsoTransform)
            assert(glm::isnan(element) == false);

    }

}

void handleBodyFront(glm::vec3 & realTorsoPosition,
                     glm::mat4 & realTorsoOrientation) {
    realTorsoPosition =
        realFrontTransform *
        glm::vec4(offsetTorsoFrontToCenter, 1.);

    realTorsoOrientation = glm::mat3(realFrontTransform);
}


void handleBodyBack(glm::vec3 & realTorsoPosition,
                    glm::mat4 & realTorsoOrientation) {
    realTorsoPosition =
        realBackTransform *
        glm::vec4(offsetTorsoBackToCenter, 1.);

    realTorsoOrientation = glm::mat3(realBackTransform);
}

void handleBodyBoth(glm::vec3 & realTorsoPosition,
                    glm::mat4 & realTorsoOrientation) {
    realTorsoPosition = 0.5f *
        (realFrontTransform[3] + realBackTransform[3]);

    glm::quat torsoFrontOrientationQuat =
        glm::quat_cast(realFrontTransform);
    glm::quat torsoBackOrientationQuat =
        glm::quat_cast(realBackTransform);

    if(glm::dot(torsoFrontOrientationQuat,
                torsoBackOrientationQuat) < 0.f)
        torsoFrontOrientationQuat = -torsoFrontOrientationQuat;

    glm::quat realTorsoOrientationQuat = glm::mix(
        torsoFrontOrientationQuat, torsoBackOrientationQuat, 0.5f);

    realTorsoOrientation = glm::mat4_cast(realTorsoOrientationQuat);
}

float angleAroundGlobalY(glm::mat3 transform) {
    glm::vec3 xdir(1, 0, 0);
    glm::vec3 znegdir(0, 0, -1);

    glm::vec3 xdirTrans = transform * xdir;
    glm::vec3 znegdirTrans = transform * znegdir;

    float angleYaw = 0;
    if(glm::length(xdirTrans) > 0.001) {
        angleYaw = atan2(-xdirTrans.z, xdirTrans.x);
    }
    else {
        angleYaw = atan2(-znegdirTrans.x, -znegdirTrans.z);
    }

    return angleYaw;
}

float angleAroundLocalX(glm::mat3 transform) {
    glm::vec3 ydir(0, 1, 0);
    glm::vec3 znegdir(0, 0, -1);
    glm::vec3 znegdirTrans = transform * znegdir;

    float angleYaw = angleAroundGlobalY(transform);

    glm::vec3 cleanYaw = glm::rotateY(znegdir, angleYaw);

    float a = glm::dot(cleanYaw, znegdirTrans);
    float b = glm::dot(ydir, znegdirTrans);

    return(atan2(b, a));
}

glm::mat4 correctBodyTransform(glm::mat4 transform) {
    for(int entry = 0 ; entry < 3 ; ++entry) {
        transform[3][entry] /= 1000.0f;
    }
    transform[3] += glm::vec4(4, 0, -4, 0);

    return transform;
}

glm::mat4 cleanMatrix(glm::mat4 matrix) {
    glm::vec3 scale;
    glm::quat rotation;
    glm::vec3 translation;
    glm::vec3 skew;
    glm::vec4 perspective;
    glm::decompose(matrix,
                   scale, rotation, translation, skew, perspective);

    glm::mat4 result = glm::toMat4(rotation);
    result[3] = glm::vec4(translation, 1);

    for(int col = 0 ; col < 3 ; ++col) {
        result[col] = glm::normalize(result[col]);
    }

    return result;
}
