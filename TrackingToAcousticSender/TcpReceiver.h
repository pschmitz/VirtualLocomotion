#pragma once

#include <cstddef>
#include <cstdint>
#include <thread>
#include <netinet/in.h>

class TcpReceiver {
public:
    TcpReceiver(uint16_t port);
    ~TcpReceiver();

    int mSocket;
    sockaddr_in mAddress = {};
};
