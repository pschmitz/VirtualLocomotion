#pragma once

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <strings.h>
#include <endian.h>

#include <iostream>

class UdpSender {
public:
    UdpSender() {}
    UdpSender(const char* host, short port)
    {
        if(fd != -1) {
            close(fd);
        }

        struct sockaddr_in serv_addr;
        struct hostent *server;

        fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (fd < 0)
            std::cerr << "UdpSender: failed to create socket"
                      << std::endl;
        server = gethostbyname(host);
        if (server == NULL) {
            std::cerr << "UdpSender: no such host" << std::endl;
        }

        bzero((char *) &serv_addr, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        bcopy((char *)server->h_addr,
              (char *)&serv_addr.sin_addr.s_addr,
              server->h_length);
        serv_addr.sin_port = htons(port);

        if (connect(fd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
            std::cerr << "UdpSender: connect error" << std::endl;

        std::cerr << "UdpSender: connected. fd: " << fd << std::endl;
    }

    int fd = -1;
};
