//
// Created by pschmitz on 3/20/17.
//

#ifndef GEARVRAPP_USERKINEMATIC_H
#define GEARVRAPP_USERKINEMATIC_H

#include <glm/glm.hpp>

class UserKinematic
{
public:
    float getRotationGain();
    void setRotationGain(float gain);

    glm::vec3 getRealHeadPosition();
    glm::mat4 getRealHeadTransform();
    void setRealHeadTransform(glm::mat4 transform);

    glm::vec3 getVirtualHeadPosition();
    glm::mat4 getVirtualHeadTransform();
    void setVirtualHeadTransform(glm::mat4 transform);

    void setMovementVelocityHead(glm::vec3 velocity);

    glm::vec3 getAngularVelocityHead();
    void setAngularVelocityHead(glm::vec3 velocity);

private:
    float translationGain = 1.f;
    float rotationGain = 1.f;

    glm::vec3 movementVelocityHead;
    glm::vec3 angularVelocityHead;

    glm::mat4 realHeadTransform;
    glm::mat4 realTorsoTransform;

    glm::mat4 virtualHeadTransform;
};

#endif //GEARVRAPP_USERKINEMATIC_H
