#include <unistd.h>
#include <sys/socket.h>
#include <endian.h>
#include <netinet/in.h>

#include <iostream>

#include "TcpReceiver.h"

TcpReceiver::TcpReceiver(uint16_t port) {
    mSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (mSocket == -1) {
        std::cerr<< "Could not create socket!" << std::endl;
        return;
    }
    mAddress.sin_family = AF_INET;
    mAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    mAddress.sin_port = htons(port);

    if (bind(mSocket, (struct sockaddr*)(&mAddress), sizeof(mAddress)) < 0) {
        std::cerr << "Could not bind TCP Receiver to address." << std::endl;
        return;
    }

    listen(mSocket, 1);
}

TcpReceiver::~TcpReceiver()
{
    close(mSocket);
}
