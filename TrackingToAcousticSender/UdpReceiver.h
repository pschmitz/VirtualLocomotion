#pragma once

#include <netinet/in.h>

#include <cstddef>
#include <cstdint>
#include <array>
#include <thread>


class UdpReceiver {
public:
    // 2312 == MTU of WiFi == biggest UDP packet receivable
    static constexpr size_t buffer_length = 2312;

    UdpReceiver(uint16_t port);

    int mSocket;
    sockaddr_in mAddress = {};
};
