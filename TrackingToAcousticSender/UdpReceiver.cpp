#include <unistd.h>
#include <sys/socket.h>
#include <endian.h>
#include <netinet/in.h>

#include <iostream>

#include "UdpReceiver.h"

UdpReceiver::UdpReceiver(uint16_t port) {
    mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (mSocket == -1) {
        std::cerr << "Could not create socket!" << std::endl;
        return;
    }
    mAddress.sin_family = AF_INET;
    mAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    mAddress.sin_port = htons(port);

    if (bind(mSocket, (struct sockaddr*)(&mAddress), sizeof(mAddress)) < 0) {
        std::cerr << "Could not bind UDP Receiver to address." << std::endl;
        return;
    }
}
