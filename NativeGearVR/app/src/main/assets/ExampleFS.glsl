#version 300 es
precision highp float;
precision highp sampler2D;

in vec3 perPixelNormal;
in vec2 perPixelTexCoord;

out vec4 finalColor;

uniform vec3 uLineColor;

uniform sampler2D tex;

void main()
{
    vec3 lightDirection = vec3(-0.8,-0.7f,0.6f);
    lightDirection = normalize(lightDirection);
    vec3 normal = normalize(perPixelNormal);


    finalColor = texture(tex, perPixelTexCoord);
    finalColor.rgb *= clamp(clamp(dot(normal, -lightDirection),0.0f,1.0f) + 0.3f, 0.0f, 1.0f);

	/*if(mod(perPixelTexCoord.x, 5.0f) < 0.1f || mod(perPixelTexCoord.y, 5.0f) < 0.1f)
	{
		finalColor = vec4(uLineColor,1);
	}
	else
	{
		finalColor = vec4(perPixelNormal,1);
	}*/
}