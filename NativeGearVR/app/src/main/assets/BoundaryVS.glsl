#version 300 es
precision highp float;

in vec3 position;
in vec3 normal;
in vec2 texcoord;

out vec3 perPixelNormal;
out vec2 perPixelTexCoord;
out vec3 perPixelWorldPosition;

uniform mat4x4 uTransformMatrix;
uniform mat4x4 uViewMatrix;
uniform mat4x4 uProjectionMatrix;

void main()
{
	perPixelNormal = (uTransformMatrix * vec4(normal,0)).xyz;
	perPixelTexCoord = texcoord;
	gl_Position = vec4(position,1);

	gl_Position = uTransformMatrix * gl_Position;
	perPixelWorldPosition = gl_Position.xyz;
	gl_Position = uViewMatrix * gl_Position;
	gl_Position = uProjectionMatrix * gl_Position;
}