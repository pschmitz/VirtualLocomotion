#version 300 es
precision highp float;
precision highp sampler2D;

in vec3 perPixelNormal;
in vec2 perPixelTexCoord;

out vec4 finalColor;

void main()
{
	vec3 lightDir = vec3(-1,-1,-1);
	vec3 normal = normalize(perPixelNormal);

	finalColor = vec4(normal,1);
}