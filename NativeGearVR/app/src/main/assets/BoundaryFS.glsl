#version 300 es
precision highp float;
precision highp sampler2D;

in vec3 perPixelNormal;
in vec2 perPixelTexCoord;
in vec3 perPixelWorldPosition;

uniform vec3 uCameraPosition;
uniform vec3 uColor;

out vec4 finalColor;

uniform sampler2D tex;

void main()
{
    //vec3 lightDirection = vec3(-0.8,-0.7f,0.6f);
    //lightDirection = normalize(lightDirection);
    //vec3 normal = normalize(perPixelNormal);

    vec3 diff = perPixelWorldPosition - uCameraPosition;
    float distance = length(diff);

    finalColor.a = clamp(1.0f - distance/1.0f, 0.0f, 1.0f);
    finalColor.a = clamp(finalColor.a, 0.0f, 1.0f);

    finalColor.rgb = texture(tex, perPixelTexCoord).rgb;
    finalColor.rgb *= uColor;
    //finalColor.rgb *= clamp(clamp(dot(normal, -lightDirection),0.0f,1.0f) + 0.5f, 0.0f, 1.0f);
}