package nativegearvr;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 * Created by Benni on 01.09.2016.
 */
public class NativeToJava
{
    Context context;

    public NativeToJava(Context context)
    {
        this.context = context;
    }

    public void SetDebugText(String text)
    {
        //Log.d("SetDebugText", text);
        GL2JNIActivity.setDebugText(text);
    }

    public void SaveToInternalFile(String filename, String text)
    {
        //TODO: implement
        //getFilesDir()
        //getCacheDir()

        Log.d("SaveToFile", filename);

        File file = new File(context.getFilesDir(), filename);
        FileOutputStream outputStream;

        try
        {
            outputStream = context.openFileOutput(file.getAbsolutePath(), Context.MODE_PRIVATE);
            outputStream.write(text.getBytes());
            outputStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void SaveToInternalFile(String filename, byte[] data)
    {
        //TODO: implement
    }

    public String LoadFromInternalFile(String filename)
    {
        File file = new File(context.getFilesDir(), filename);
        byte[] buffer = new byte[(int)file.length()];
        FileInputStream inputStream;

        try
        {
            inputStream = context.openFileInput(file.getAbsolutePath());
            inputStream.read(buffer);
            inputStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return new String(buffer);
    }

    public void SaveToExternalFile(String filename, String text)
    {
        //TODO: implement
        //getFilesDir()
        //getCacheDir()
        try
        {
            File path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS);
            File file = new File(path, filename);
            Log.d("SaveToFile", file.getPath());

            path.mkdirs();

            if (!file.exists())
            {
                file.createNewFile();
            }

            OutputStream outputStream = new FileOutputStream(file);

            outputStream.write(text.getBytes());
            outputStream.close();

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(context,
                    new String[] { file.toString() }, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String LoadFromExternalFile(String filename)
    {
        FileInputStream inputStream;
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS);
        File file = new File(path, filename);
        byte[] buffer = new byte[(int)file.length()];

        try
        {
            inputStream = new FileInputStream(file);
            inputStream.read(buffer);
            inputStream.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return new String(buffer);
    }

    public TextureData LoadTextureData(String filename)
    {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;   // No pre-scaling

        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try
        {
            istr = assetManager.open(filename);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e)
        {
            // handle exception
        }

        // Read in the resource
        //final Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fd);
        //final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId, options);

        TextureData textureData = new TextureData();

        int bytesize = bitmap.getRowBytes() * bitmap.getHeight(); // or bitmap.ByteCount if target is  api 12 or later
        ByteBuffer buffer = ByteBuffer.allocate(bytesize);
        bitmap.copyPixelsToBuffer(buffer);
        buffer.rewind();
        textureData.bytes = new byte[bytesize];
        buffer.get(textureData.bytes);

        textureData.width = bitmap.getWidth();
        textureData.height = bitmap.getHeight();

        // Recycle the bitmap, since its data has been loaded into OpenGL.
        bitmap.recycle();
        return textureData;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }
}
