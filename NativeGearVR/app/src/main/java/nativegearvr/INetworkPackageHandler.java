package nativegearvr;

/**
 * Created by Benni on 05.07.2016.
 */
public interface INetworkPackageHandler
{
    void handlePackage(byte[] data);
}
