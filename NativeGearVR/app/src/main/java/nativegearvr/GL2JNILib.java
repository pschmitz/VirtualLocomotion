
package nativegearvr;

import android.app.Activity;
import android.content.res.AssetManager;
import android.view.Surface;

// Wrapper for native library

public class GL2JNILib
{
    public static native void testFunction( long handle, float value );
    public static native void nativeReceivedPackage( byte[] bytes, long handle );

    // Activity lifecycle
    public static native long onCreate(Activity obj, AssetManager assetManager, NativeToJava nativeToJava);
    public static native void onStart( long handle );
    public static native void onResume( long handle );
    public static native void onPause( long handle );
    public static native void onStop( long handle );
    public static native void onDestroy( long handle );

    // Surface lifecycle
    public static native void onSurfaceCreated( long handle, Surface s );
    public static native void onSurfaceChanged( long handle, Surface s );
    public static native void onSurfaceDestroyed( long handle );

    // Input
    public static native void onKeyEvent( long handle, int keyCode, int action );
    public static native void onTouchEvent( long handle, int action, float x, float y );
}
