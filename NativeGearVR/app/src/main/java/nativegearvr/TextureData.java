package nativegearvr;

/**
 * Created by Benni on 30.09.2016.
 */
public class TextureData
{
    public byte[] bytes;
    public int width;
    public int height;
}
