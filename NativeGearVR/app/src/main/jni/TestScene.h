//
// Created by Benni on 07.07.2016.
//

#ifndef NATIVEGEARVR_TESTSCENE_H
#define NATIVEGEARVR_TESTSCENE_H

#include <VrApi.h>
#include <VrApi_Helpers.h>

#include "Framework/Scene.h"
#include "Ovr/OvrFramebuffer.h"

#include "Render/RoomRenderer.h"
#include "Render/CubeRenderer.h"
#include "Render/BoundaryRenderer.h"
#include "DataCaptureManager.h"

#include <glm/glm.hpp>
#include <vector>

using namespace glm;

#define DRAW_BOUNDARY

struct DebugControlMessage
{
    float adjustableEyeOffset;
    float rotationCenterOffset;
};

struct UserKinematic
{
    vec3 RealWalkDifference;
    float RealkWalkingSpeed;
    vec3 RealPosition;
    vec3 VirtualPosition;
    vec3 AngularVelocity;

};

class TestScene : public Scene
{
private: //Settings
    vec2 sceneMax = vec2(2.3,2.6);
    vec2 sceneMin = vec2(-2,-1.75);
    //length of scene diagonal sqrt(4.3² + 4.35²) = ~6m

    bool allowCapturing = false;
    int framesWithoutPackageTillLag = 4; //4 * 1/60s

    float adjustableEyeOffset = -0.05f; //-0.05f or 0.0f
    float rotationCenterOffset = 0.1;

    UserKinematic userKinematic;
    double frameTime;
private:
    RoomRenderer roomRenderer;
    CubeRenderer cubeRenderer;
    BoundaryRenderer boundaryRenderer;
    vec3 cubePosition;
    mat4 cubeRotation;

    vec3 approxStartPosition = vec3(-1.5f,0,2.2f);
    vector<vec3> virtualTargetPositions;
    vector<vec3> realTargetPositions;
    int currentTargetIndex = 0;

    vec3 getCurrentRealTargetPosition() { return realTargetPositions[currentTargetIndex]; }
    vec3 getCurrentVirtualTargetPosition() { return virtualTargetPositions[currentTargetIndex]; }

    mat4 lastVirtualViewMatrix = mat4(1.0f);
    mat4 lastRealViewMatrix = mat4(1.0f);
    vec3 virtualToRealPosition(vec3 virtualPosition);

    mat4 additionalSceneRotation = mat4(1.0f);

    bool lastIsInitialized = false;

    unsigned long currentFrame = 0;
    unsigned long localFrameTimeStamp = 0;

    unsigned long lastFrameNr = 0; //Tracking system frame
    vec3 currentTrackingPosition = vec3(0, 0, 0);
    bool hasCompleteBody = false;

    vec3 oldTrackingPosition = vec3(0,0,0);
    vec3 virtualPosition = vec3(0,0,0);

    DataCaptureManager dataCaptureManager;
public:
    float testFloat    = 0;

    void Init() override;
    void Load() override;
    void Update(ovrMobile *ovr, double predictedDisplayTime, double predictedDisplayTimeDelta, bool isTouching) override;
    void DrawEye(int eye, const ovrTracking *tracking, ovrHeadModelParms &headModelParms, ovrTracking &updatedTracking) override;
    void Destroy() override;

    void ReceivePackage(void *buffer, int length, JNIEnv *env) override;

private:
    void computeManipulation(const ovrTracking *&tracking, mat4 realViewMatrix);
};


#endif //NATIVEGEARVR_TESTSCENE_H
