//
// Created by Benni on 30.09.2016.
//

#include "Model.h"
#include "../Framework/ObjLoader.h"
#include "../Framework/FileReader.h"


struct vertex
{
    vec3 Position = vec3(0,0,0);
    vec3 Normal = vec3(0,1,0);
    vec2 TexCoord = vec2(0,0);

    vertex()
    {

    }

    vertex(vec3 pos, vec3 normal, vec2 tex)
    {
        Position = pos;
        Normal = normal;
        TexCoord = tex;
    }

    vertex(float x, float y, float z, float nx, float ny, float nz, float tx, float ty)
    {
        Position = vec3(x,y,z);
        Normal = vec3(nx,ny,nz);
        TexCoord = vec2(tx,ty);
    }
};

Model LoadModel(JavaRuntime &javaRuntime, string modelPath, string texturePath, vec3 meshOffset, vec3 meshScaling)
{
    Model model;

    ObjLoader objLoader;
    string objFileContent = FileReader::readTextFromAssetFile(javaRuntime, modelPath);
    vector<glm::vec3> vpositions;
    vector<glm::vec3> vnormals;
    vector<glm::vec2> vtexcoords;
    vector<unsigned int> vindices;
    objLoader.loadObjFile(objFileContent, vpositions, vnormals, vtexcoords, vindices);

    model.VerticesCount = vpositions.size();
    model.IndicesCount = vindices.size();
    vertex *vertices = new vertex[model.VerticesCount];
    unsigned int *indices = &vindices[0];

    for (int j = 0; j < vpositions.size(); ++j)
    {
        vpositions[j] += vec3(4,0,0);

        vertices[j] = vertex(
                vpositions[j],
                vnormals[j],
                vtexcoords[j]);

        //LogError(to_string(vpositions[j].x) + ", " + to_string(vpositions[j].y) + ", " + to_string(vpositions[j].z));
    }

    //invert backface
    for (int j = 0; j < model.IndicesCount; j += 3)
    {
        uint temp = indices[j];
        indices[j] = indices[j+2];
        indices[j+2] = temp;
    }

    glGenBuffers(1, &model.VertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, model.VertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex) * model.VerticesCount, vertices, GL_STATIC_DRAW);
    glBindBuffer (GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &model.IndexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.IndexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * model.IndicesCount, indices, GL_STATIC_DRAW);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

    //InputLayout
    glGenVertexArrays(1, &model.VertexBufferObject);
    glBindVertexArray(model.VertexBufferObject);

    //TODO: this is dependent to the vertex shader ?

    /*GLint tempint = glGetAttribLocation(program, "position");
    GLint tempint2 = glGetAttribLocation(program, "normal");
    GLint tempint3 = glGetAttribLocation(program, "texcoord");

    glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
    glEnableVertexAttribArray(tempint);
    glVertexAttribPointer (tempint, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), NULL);
    glEnableVertexAttribArray(tempint2);
    glVertexAttribPointer (tempint2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid *)sizeof(vec3));
    glEnableVertexAttribArray(tempint3);
    glVertexAttribPointer (tempint3, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid *)(sizeof(vec3) * 2));
    */

    //create texture
    /*glGenTextures(1, &roomTexture);

    //TODO: get texture data

    glBindTexture(GL_TEXTURE_2D, roomTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 256, 256, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
    glGenerateMipmap(GL_TEXTURE_2D);*/

    return model;
}
