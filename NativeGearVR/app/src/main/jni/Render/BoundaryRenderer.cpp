//
// Created by Benni on 01.09.2016.
//

#include "BoundaryRenderer.h"
#include "../Framework/FileReader.h"
#include "../Framework/Shader.h"
#include "../Framework/ObjLoader.h"
#include "../Framework/Log.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;


struct vertex
{
    vec3 Position = vec3(0,0,0);
    vec3 Normal = vec3(0,1,0);
    vec2 TexCoord = vec2(0,0);

    vertex()
    {

    }

    vertex(vec3 pos, vec3 normal, vec2 tex)
    {
        Position = pos;
        Normal = normal;
        TexCoord = tex;
    }

    vertex(float x, float y, float z, float nx, float ny, float nz, float tx, float ty)
    {
        Position = vec3(x,y,z);
        Normal = vec3(nx,ny,nz);
        TexCoord = vec2(tx,ty);
    }
};

const int indexCount = 36 - 12;

void BoundaryRenderer::Load(JavaRuntime &javaRuntime)
{
    const int vertexCount = 24;
    vertex vertices[vertexCount]
            {
                    vertex(0,0,0, 0,-1,0, 0,0),
                    vertex(1,0,0, 0,-1,0, 1,0),
                    vertex(0,0,1, 0,-1,0, 0,1),
                    vertex(1,0,1, 0,-1,0, 1,1),

                    vertex(0,0,0, 0,0,-1, 0,0),
                    vertex(1,0,0, 0,0,-1, 1,0),
                    vertex(0,1,0, 0,0,-1, 0,1),
                    vertex(1,1,0, 0,0,-1, 1,1),

                    vertex(1,0,0, 1,0,0, 0,0),
                    vertex(1,0,1, 1,0,0, 1,0),
                    vertex(1,1,0, 1,0,0, 0,1),
                    vertex(1,1,1, 1,0,0, 1,1),

                    vertex(1,0,1, 0,0,1, 0,0),
                    vertex(0,0,1, 0,0,1, 1,0),
                    vertex(1,1,1, 0,0,1, 0,1),
                    vertex(0,1,1, 0,0,1, 1,1),

                    vertex(0,0,1, -1,0,0, 0,0),
                    vertex(0,0,0, -1,0,0, 1,0),
                    vertex(0,1,1, -1,0,0, 0,1),
                    vertex(0,1,0, -1,0,0, 1,1),

                    vertex(0,1,0, 0,1,0, 0,0),
                    vertex(1,1,0, 0,1,0, 1,0),
                    vertex(0,1,1, 0,1,0, 0,1),
                    vertex(1,1,1, 0,1,0, 1,1),
            };

    for (int i = 0; i < vertexCount; ++i)
    {
        //make [1,1,1]
        vertices[i].Position.x = (vertices[i].Position.x - 0.5f);
        vertices[i].Position.y = (vertices[i].Position.y - 0.5f);
        vertices[i].Position.z = (vertices[i].Position.z - 0.5f);

        //from d3d texcoords to openGL texcoords
        vertices[i].TexCoord.x = 1 - vertices[i].TexCoord.x;
        vertices[i].TexCoord.y = 1 - vertices[i].TexCoord.y;

        vertices[i].TexCoord.x *= 10;
        vertices[i].TexCoord.y *= 5;
    }

    uint indices[indexCount]
            {
                    //0, 1, 3, //Bottom
                    //0, 3, 2,

                    4, 6, 5, //Front
                    5, 6, 7,

                    8, 10, 11, //Right
                    8, 11, 9,

                    12, 15, 13, //Back //TODO: Fix Texcoords
                    12, 14, 15,

                    16, 19, 17, //Left
                    16, 18, 19,

                    //20, 22, 21, //Top
                    //21, 22, 23,
            };

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex) * vertexCount, vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * indexCount, indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    //Create shader
    GLuint vshader = Shader::compileShader(javaRuntime, "BoundaryVS.glsl", GL_VERTEX_SHADER);
    GLuint fshader = Shader::compileShader(javaRuntime, "BoundaryFS.glsl", GL_FRAGMENT_SHADER);

    program = glCreateProgram();
    glAttachShader(program, vshader);
    glAttachShader(program, fshader);
    glLinkProgram(program);

    glDeleteShader(vshader);
    glDeleteShader(fshader);

    uViewMatrixLocation = glGetUniformLocation(program, "uViewMatrix");
    uProjectionMatrixLocation = glGetUniformLocation(program, "uProjectionMatrix");
    uTransformationMatrixLocation = glGetUniformLocation(program, "uTransformMatrix");
    uCameraPositionLocation = glGetUniformLocation(program, "uCameraPosition");
    uColorLocation = glGetUniformLocation(program, "uColor");

    //InputLayout
    glGenVertexArrays(1, &vertexBufferObject);
    glBindVertexArray(vertexBufferObject);

    GLint tempint = glGetAttribLocation(program, "position");
    GLint tempint2 = glGetAttribLocation(program, "normal");
    GLint tempint3 = glGetAttribLocation(program, "texcoord");

    glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
    glEnableVertexAttribArray(tempint);
    glVertexAttribPointer (tempint, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), NULL);
    glEnableVertexAttribArray(tempint2);
    glVertexAttribPointer (tempint2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid *)sizeof(vec3));
    glEnableVertexAttribArray(tempint3);
    glVertexAttribPointer (tempint3, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid *)(sizeof(vec3) * 2));


    //create texture
    glGenTextures(1, &texture);

    const int textureSize = 128;
    unsigned char textureData[textureSize * textureSize * 4];

    for (int k = 0; k < textureSize; ++k)
    {
        for (int i = 0; i < textureSize; ++i)
        {
            textureData[k * textureSize * 4 + i * 4] = 255;
            textureData[k * textureSize * 4 + i * 4 + 1] = 255;
            textureData[k * textureSize * 4 + i * 4 + 2] = 255;
            textureData[k * textureSize * 4 + i * 4 + 3] = 255;
        }
    }

    for (int l = 0; l < textureSize; ++l)
    {
        textureData[l * 4] = 50;
        textureData[l * 4 + 1] = 50;
        textureData[l * 4 + 2] = 50;
        textureData[l * 4 + 3] = 255;
    }

    for (int l = 0; l < textureSize; ++l)
    {
        textureData[l * textureSize * 4 + 0 * 4] = 50;
        textureData[l * textureSize * 4 + 0 * 4 + 1] = 50;
        textureData[l * textureSize * 4 + 0 * 4 + 2] = 50;
        textureData[l * textureSize * 4 + 0 * 4 + 3] = 255;
    }

    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureSize, textureSize, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);
    glGenerateMipmap(GL_TEXTURE_2D);
}


void BoundaryRenderer::Render(vec3 cameraPosition, vec3 color, mat4 eyeViewMatrix, mat4 projectionMatrix, float x, float y, float width, float height)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glBindVertexArray(vertexBufferObject);
    glUseProgram(program);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

    vec3 center = vec3(x + width/2, 1.5f, y + height/2);

    mat4 transformation = glm::translate(mat4(1.0f), center) * glm::scale(mat4(1.0f), vec3(width,0.2f,height));

    glUniformMatrix4fv(uTransformationMatrixLocation, 1, GL_FALSE, glm::value_ptr(transformation));
    glUniformMatrix4fv(uViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(eyeViewMatrix));
    glUniformMatrix4fv(uProjectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
    glUniform3f(uCameraPositionLocation, cameraPosition.x, cameraPosition.y, cameraPosition.z);
    glUniform3f(uColorLocation, color.x, color.y, color.z);


    glBindTexture(GL_TEXTURE_2D, texture);
    glActiveTexture(GL_TEXTURE0);

    //glDrawArrays(GL_TRIANGLES, 0, 6);
    glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, NULL);
}

void BoundaryRenderer::Unload()
{
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteVertexArrays(1, &vertexBufferObject);
    glDeleteProgram(program);
}