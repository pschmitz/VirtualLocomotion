//
// Created by Benni on 07.07.2016.
//

#include "RoomRenderer.h"
#include "../Framework/FileReader.h"
#include "../Framework/Shader.h"
#include "../Framework/ObjLoader.h"
#include "../Framework/Log.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace glm;

struct vertex
{
    vec3 Position = vec3(0,0,0);
    vec3 Normal = vec3(0,1,0);
    vec2 TexCoord = vec2(0,0);

    vertex()
    {

    }

    vertex(vec3 pos, vec3 normal, vec2 tex)
    {
        Position = pos;
        Normal = normal;
        TexCoord = tex;
    }

    vertex(float x, float y, float z, float nx, float ny, float nz, float tx, float ty)
    {
        Position = vec3(x,y,z);
        Normal = vec3(nx,ny,nz);
        TexCoord = vec2(tx,ty);
    }
};

int indexCount = 36;

void RoomRenderer::Load(JavaRuntime &javaRuntime)
{
    ObjLoader objLoader;
    string objFileContent = FileReader::readTextFromAssetFile(javaRuntime, "testRoom01.obj");
    vector<glm::vec3> vpositions;
    vector<glm::vec3> vnormals;
    vector<glm::vec2> vtexcoords;
    vector<unsigned int> vindices;
    objLoader.loadObjFile(objFileContent, vpositions, vnormals, vtexcoords, vindices);

    /*int vertexCount = vpositions.size();
    indexCount = vindices.size();
    vertex *vertices = new vertex[vertexCount];
    unsigned int *indices = &vindices[0];

    for (int j = 0; j < vpositions.size(); ++j)
    {
        vpositions[j] += vec3(4,0,0);

        vertices[j] = vertex(
                vpositions[j],
                vnormals[j],
                vtexcoords[j] * 0.25f);

        //LogError(to_string(vpositions[j].x) + ", " + to_string(vpositions[j].y) + ", " + to_string(vpositions[j].z));
    }*/

    //Create vertexBuffer
    /*Vector3 vertices[6] =
    {
            Vector3(0,0,0),
            Vector3(1,0,0),
            Vector3(1,0,1),

            Vector3(1,0,1),
            Vector3(0,0,0),
            Vector3(0,0,1),
    };*/
    const int vertexCount = 24;
    vertex vertices[vertexCount]
    {
        vertex(0,0,0, 0,-1,0, 0,0),
        vertex(1,0,0, 0,-1,0, 1,0),
        vertex(0,0,1, 0,-1,0, 0,1),
        vertex(1,0,1, 0,-1,0, 1,1),

        vertex(0,0,0, 0,0,-1, 0,0),
        vertex(1,0,0, 0,0,-1, 1,0),
        vertex(0,1,0, 0,0,-1, 0,1),
        vertex(1,1,0, 0,0,-1, 1,1),

        vertex(1,0,0, 1,0,0, 0,0),
        vertex(1,0,1, 1,0,0, 1,0),
        vertex(1,1,0, 1,0,0, 0,1),
        vertex(1,1,1, 1,0,0, 1,1),

        vertex(1,0,1, 0,0,1, 0,0),
        vertex(0,0,1, 0,0,1, 1,0),
        vertex(1,1,1, 0,0,1, 0,1),
        vertex(0,1,1, 0,0,1, 1,1),

        vertex(0,0,1, -1,0,0, 0,0),
        vertex(0,0,0, -1,0,0, 1,0),
        vertex(0,1,1, -1,0,0, 0,1),
        vertex(0,1,0, -1,0,0, 1,1),

        vertex(0,1,0, 0,1,0, 0,0),
        vertex(1,1,0, 0,1,0, 1,0),
        vertex(0,1,1, 0,1,0, 0,1),
        vertex(1,1,1, 0,1,0, 1,1),
    };

    float min = FLT_MAX;
    float max = FLT_MIN;

    for (int i = 0; i < vertexCount; ++i)
    {
        //make [1,1,1]
        vertices[i].Position.x = (vertices[i].Position.x - 0.5f)*2;
        vertices[i].Position.y = (vertices[i].Position.y - 0.5f)*2;
        vertices[i].Position.z = (vertices[i].Position.z - 0.5f)*2;

        //scale
        vertices[i].Position.x = vertices[i].Position.x * 8;
        vertices[i].Position.z = vertices[i].Position.z * 8;
        vertices[i].Position.y = vertices[i].Position.y * 1.5f + 1.5f;

        //from d3d texcoords to openGL texcoords
        vertices[i].TexCoord.x = 1 - vertices[i].TexCoord.x;
        vertices[i].TexCoord.y = 1 - vertices[i].TexCoord.y;

        vertices[i].TexCoord *= 0.1f;

        //invert normal
        vertices[i].Normal.x *= -1;
        vertices[i].Normal.y *= -1;
        vertices[i].Normal.z *= -1;

        min = vertices[i].Position.y < min ? vertices[i].Position.y : min;
        max = vertices[i].Position.y > max ? vertices[i].Position.y : max;
    }

    const int indexCount = 36;
    uint indices[indexCount]
    {
            0, 1, 3, //Bottom
            0, 3, 2,

            4, 6, 5, //Front
            5, 6, 7,

            8, 10, 11, //Right
            8, 11, 9,

            12, 15, 13, //Back //TODO: Fix Texcoords
            12, 14, 15,

            16, 19, 17, //Left
            16, 18, 19,

            20, 22, 21, //Top
            21, 22, 23,
    };

    //invert backface
    for (int j = 0; j < indexCount; j += 3)
    {
        uint temp = indices[j];
        indices[j] = indices[j+2];
        indices[j+2] = temp;
    }

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex) * vertexCount, vertices, GL_STATIC_DRAW);
    glBindBuffer (GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint) * indexCount, indices, GL_STATIC_DRAW);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

    //Create shader
    GLuint vshader = Shader::compileShader(javaRuntime, "ExampleVS.glsl", GL_VERTEX_SHADER);
    GLuint fshader = Shader::compileShader(javaRuntime, "ExampleFS.glsl", GL_FRAGMENT_SHADER);

    program = glCreateProgram();
    glAttachShader(program, vshader);
    glAttachShader(program, fshader);
    glLinkProgram(program);

    glDeleteShader(vshader);
    glDeleteShader(fshader);

    uViewMatrixLocation = glGetUniformLocation(program, "uViewMatrix");
    uProjectionMatrixLocation = glGetUniformLocation(program, "uProjectionMatrix");

    uTransformationMatrixLocation = glGetUniformLocation(program, "uTransformMatrix");
    uLineColorLocation = glGetUniformLocation(program, "uLineColor");

    //InputLayout
    glGenVertexArrays(1, &vertexBufferObject);
    glBindVertexArray(vertexBufferObject);

    GLint tempint = glGetAttribLocation(program, "position");
    GLint tempint2 = glGetAttribLocation(program, "normal");
    GLint tempint3 = glGetAttribLocation(program, "texcoord");

    glBindBuffer (GL_ARRAY_BUFFER, vertexBuffer);
    glEnableVertexAttribArray(tempint);
    glVertexAttribPointer (tempint, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), NULL);
    glEnableVertexAttribArray(tempint2);
    glVertexAttribPointer (tempint2, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid *)sizeof(vec3));
    glEnableVertexAttribArray(tempint3);
    glVertexAttribPointer (tempint3, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), (const GLvoid *)(sizeof(vec3) * 2));

    //create texture
    glGenTextures(1, &roomTexture);

    unsigned char textureData[256 * 256 * 4];

    for (int k = 0; k < 256; ++k)
    {
        for (int i = 0; i < 256; ++i)
        {
            textureData[k * 256 * 4 + i * 4] = 200;
            textureData[k * 256 * 4 + i * 4 + 1] = 200;
            textureData[k * 256 * 4 + i * 4 + 2] = 200;
            textureData[k * 256 * 4 + i * 4 + 3] = 255;
        }
    }

    for (int l = 0; l < 256; ++l)
    {
        textureData[l * 4] = 50;
        textureData[l * 4 + 1] = 50;
        textureData[l * 4 + 2] = 50;
        textureData[l * 4 + 3] = 255;
    }

    for (int l = 0; l < 256; ++l)
    {
        textureData[l * 256 * 4 + 0 * 4] = 50;
        textureData[l * 256 * 4 + 0 * 4 + 1] = 50;
        textureData[l * 256 * 4 + 0 * 4 + 2] = 50;
        textureData[l * 256 * 4 + 0 * 4 + 3] = 255;
    }

    TextureData textureData1 = javaRuntime.loadTextureData("CinderblockClean0003_1_S.jpg");

    glBindTexture(GL_TEXTURE_2D, roomTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureData1.width, textureData1.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData1.data);
    glGenerateMipmap(GL_TEXTURE_2D);
}


void RoomRenderer::Render(mat4 transformation, mat4 eyeViewMatrix, mat4 projectionMatrix, vec3 lineColor)
{
    glBindVertexArray(vertexBufferObject);
    glUseProgram(program);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

    glUniformMatrix4fv(uTransformationMatrixLocation, 1, GL_FALSE, glm::value_ptr(transformation));
    glUniformMatrix4fv(uViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(eyeViewMatrix));
    glUniformMatrix4fv(uProjectionMatrixLocation, 1, GL_FALSE, glm::value_ptr(projectionMatrix * eyeViewMatrix));

    glUniform3f(uLineColorLocation, lineColor.x, lineColor.y, lineColor.z);

    glBindTexture(GL_TEXTURE_2D, roomTexture);
    glActiveTexture(GL_TEXTURE0);

    //glDrawArrays(GL_TRIANGLES, 0, 6);
    glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, NULL);
}

void RoomRenderer::Unload()
{
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteVertexArrays(1, &vertexBufferObject);
    glDeleteProgram(program);
}