//
// Created by Benni on 01.09.2016.
//

#ifndef NATIVEGEARVR_BOUNDARYRENDERER_H
#define NATIVEGEARVR_BOUNDARYRENDERER_H

#include <GLES3/gl3.h>
#include "../Framework/JavaRuntime.h"

#include <glm/glm.hpp>
#include "Math.h"
using namespace glm;

class BoundaryRenderer
{
private:
    GLuint vertexBuffer;
    GLuint indexBuffer;
    GLuint vertexBufferObject;
    GLuint program;

    GLint uViewMatrixLocation;
    GLint uProjectionMatrixLocation;
    GLint uTransformationMatrixLocation;
    GLint uCameraPositionLocation;
    GLint uColorLocation;

    GLuint texture;

public:
    void Load(JavaRuntime &javaRuntime);
    void Render(vec3 cameraPosition, vec3 color, mat4 eyeViewMatrix, mat4 projectionMatrix, float x, float y, float width, float height);
    void Unload();
};

#endif //NATIVEGEARVR_BOUNDARYRENDERER_H
