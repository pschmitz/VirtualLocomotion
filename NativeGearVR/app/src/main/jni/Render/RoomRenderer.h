//
// Created by Benni on 07.07.2016.
//

#ifndef NATIVEGEARVR_ROOMRENDERER_H
#define NATIVEGEARVR_ROOMRENDERER_H


#include <GLES3/gl3.h>
#include "../Framework/JavaRuntime.h"

#include <glm/glm.hpp>
using namespace glm;

class RoomRenderer
{
private:
    GLuint vertexBuffer;
    GLuint indexBuffer;
    GLuint vertexBufferObject;
    GLuint program;

    GLuint roomTexture;

    GLint uViewMatrixLocation;
    GLint uProjectionMatrixLocation;
    GLint uTransformationMatrixLocation;
    GLint uLineColorLocation;

public:
    void Load(JavaRuntime &javaRuntime);
    void Render(mat4 transformation, mat4 eyeViewMatrix, mat4 projectionMatrix, vec3 lineColor);
    void Unload();
};


#endif //NATIVEGEARVR_ROOMRENDERER_H
