//
// Created by Benni on 30.09.2016.
//

#ifndef NATIVEGEARVR_MODELRENDERER_H
#define NATIVEGEARVR_MODELRENDERER_H



#include <GLES3/gl3.h>
#include "../Framework/JavaRuntime.h"

#include <glm/glm.hpp>
#include <vector>
using namespace glm;

class ModelRenderer
{
private:
    GLuint program;

    GLint uViewMatrixLocation;
    GLint uProjectionMatrixLocation;
    GLint uTransformationMatrixLocation;
public:
    void Load(JavaRuntime &javaRuntime);
    void Render(mat4 eyeViewMatrix, mat4 projectionMatrix, vec3 lineColor);
    void Unload();
};


#endif //NATIVEGEARVR_MODELRENDERER_H
