//
// Created by Benni on 22.07.2016.
//

#ifndef NATIVEGEARVR_CUBERENDERER_H
#define NATIVEGEARVR_CUBERENDERER_H


#include <GLES3/gl3.h>
#include "../Framework/JavaRuntime.h"

#include <glm/glm.hpp>
using namespace glm;

class CubeRenderer
{
private:
    GLuint vertexBuffer;
    GLuint indexBuffer;
    GLuint vertexBufferObject;
    GLuint program;

    GLint uViewMatrixLocation;
    GLint uProjectionMatrixLocation;
    GLint uTransformationMatrixLocation;

public:
    void Load(JavaRuntime &javaRuntime);
    void Render(mat4 transformation, mat4 eyeViewMatrix, mat4 projectionMatrix);
    void Unload();
};

#endif //NATIVEGEARVR_CUBERENDERER_H
