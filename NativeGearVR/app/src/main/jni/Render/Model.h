//
// Created by Benni on 30.09.2016.
//

#ifndef NATIVEGEARVR_MODEL_H
#define NATIVEGEARVR_MODEL_H

#include <GLES3/gl3.h>
#include <string>
#include <glm/glm.hpp>
#include "Math.h"
#include "../Framework/JavaRuntime.h"

using namespace std;
using namespace glm;

struct Model
{
    GLuint VertexBuffer;
    GLuint IndexBuffer;
    GLuint VertexBufferObject;
    GLuint Texture;
    int VerticesCount;
    int IndicesCount;

    void Release()
    {
        glDeleteBuffers(1, &VertexBuffer);
        glDeleteBuffers(1, &IndexBuffer);
        glDeleteTextures(1, &Texture);

        glDeleteVertexArrays(1, &VertexBufferObject);
    }
};

Model LoadModel(JavaRuntime &javaRuntime, string modelPath, string texturePath, vec3 meshOffset, vec3 meshScaling);


#endif //NATIVEGEARVR_MODEL_H
