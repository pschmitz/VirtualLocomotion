//
// Created by Benni on 07.07.2016.
//

#include "TestScene.h"
#include "Parser.h"

#include <android/log.h>
#include <VrApi_Types.h>
#include "Framework/Log.h"
#include "Framework/Math.h"

void TestScene::Init()
{
    float virtualSceneDistance = 6.5f;
    virtualTargetPositions.push_back(approxStartPosition + vec3(0,0,0)); //Start position
    virtualTargetPositions.push_back(approxStartPosition + vec3(virtualSceneDistance,0,0));
    virtualTargetPositions.push_back(approxStartPosition + vec3(virtualSceneDistance,0,-virtualSceneDistance));
    virtualTargetPositions.push_back(approxStartPosition + vec3(0,0,-virtualSceneDistance));
    virtualTargetPositions.push_back(approxStartPosition + vec3(0,0,0));

    realTargetPositions.push_back(vec3(approxStartPosition)); //Start position
    realTargetPositions.push_back(vec3(2.2,0,-1.7f));
    realTargetPositions.push_back(vec3(approxStartPosition));
    realTargetPositions.push_back(vec3(2.2,0,-1.7f));
    realTargetPositions.push_back(vec3(approxStartPosition));
}

void TestScene::Load()
{
    dataCaptureManager.Init(&javaRuntime);
    roomRenderer.Load(javaRuntime);
    cubeRenderer.Load(javaRuntime);
    boundaryRenderer.Load(javaRuntime);

    if(allowCapturing)
    {
        this->javaRuntime.setDebugText("Ready");
    }
    else
    {
        this->javaRuntime.setDebugText("Capturing is disabled");
    }

    currentTrackingPosition = vec3(0, 1.8f, 0);

    //Testing
    //->javaRuntime.saveTextToFile("test.txt", "Random text content");
    //string str = javaRuntime.loadTextFromFile("test.txt");
    //this->javaRuntime.setDebugText(str);
}

float rotationFactorMax = -FLT_MAX;

float lastSpeed = 0;
void TestScene::Update(ovrMobile *ovr, double predictedDisplayTime, double predictedDisplayTimeDelta, bool isTouching)
{
    frameTime = predictedDisplayTimeDelta;

    if(isTouching)
    {
        vrapi_RecenterPose(ovr);
        lastIsInitialized = false;
        additionalSceneRotation = mat4(1.0f);
        virtualPosition = currentTrackingPosition;
        oldTrackingPosition = currentTrackingPosition;
        rotationFactorMax = -FLT_MAX;
        currentTargetIndex = 0;
    }

    if(allowCapturing)
    {
        if (isTouching)
        {
            //Capture 60s
            dataCaptureManager.Start(60);
        }

        dataCaptureManager.Update(frameTime);
    }
    else
    {
        //javaRuntime.setDebugText(to_string(userKinematic.RealkWalkingSpeed));
        if(lastSpeed != userKinematic.RealkWalkingSpeed)
        {
            Log("Speed", to_string(userKinematic.RealkWalkingSpeed));
        }
        lastSpeed = userKinematic.RealkWalkingSpeed;
    }

    vec3 temp1 = getCurrentVirtualTargetPosition();
    vec3 temp2 = userKinematic.VirtualPosition;
    vec2 temp1xz = vec2(temp1.x, temp1.z);
    vec2 temp2xz = vec2(temp2.x, temp2.z);
    //javaRuntime.setDebugText(to_string(glm::length(temp1xz - temp2xz)));

    if(glm::length(temp1xz - temp2xz) < 1.0f) //1m
    {
        currentTargetIndex++;
        if(currentTargetIndex == virtualTargetPositions.size())
        {
            currentTargetIndex = 1; //Skip start position(0) on loop
        }
    }

    //javaRuntime.setDebugText("CurrentPosition: \n" + to_string(userKinematic.RealPosition));
}

vec3 TestScene::virtualToRealPosition(vec3 virtualPosition)
{
    mat4 inverse = glm::inverse(lastRealViewMatrix);
    vec4 temp = inverse * (lastVirtualViewMatrix * vec4(virtualPosition, 1));
    return vec3(temp.x, temp.y, temp.z);
}

void TestScene::computeManipulation(const ovrTracking *&tracking, mat4 realViewMatrix)
{
    //Compute direction, up, right
    mat4 inverseView = glm::inverse(realViewMatrix);
    vec4 result = (inverseView * vec4( vec3(0,0,-1), 0));
    vec3 direction = vec3(result.x, result.y, result.z);
/*
    result = (realViewMatrix * vec4( vec3(0,1,0), 0));
    vec3 up = vec3(result.x, result.y, result.z);
    vec3 right = glm::cross(direction, up);
*/
    vec3 upGlobal = vec3(0,1,0);
    vec3 right = glm::cross(direction, upGlobal);
    vec3 up = glm::cross(right, direction);

    direction = normalize(direction);
    right = normalize(right);
    up = normalize(up);

    //Compute userKinematic
    userKinematic.AngularVelocity = toGlm(tracking->HeadPose.AngularVelocity);

    vec3 oldRealPosition = userKinematic.RealPosition;
    userKinematic.RealPosition = currentTrackingPosition;
    userKinematic.RealWalkDifference = userKinematic.RealPosition - oldRealPosition;

    //TODO: Speed should be time dependent (frametime)
    userKinematic.RealkWalkingSpeed = glm::length(userKinematic.RealWalkDifference);

    float distanceToVirtualTarget = glm::length(getCurrentVirtualTargetPosition() - userKinematic.VirtualPosition);
    float distanceToRealTarget = glm::length(getCurrentRealTargetPosition() - userKinematic.RealPosition);
    float translationGainFactor = distanceToVirtualTarget / distanceToRealTarget;
    translationGainFactor = glm::clamp(translationGainFactor, 0.75f, 1.25f);

    //The virtual position is the sum of the movement between 2 frames relative to the virtual rotation.
    vec4 temp = (glm::transpose(additionalSceneRotation) * vec4(userKinematic.RealWalkDifference, 0));
    virtualPosition += vec3(temp.x, temp.y,temp.z) * translationGainFactor; //apply translation gain here
    userKinematic.VirtualPosition = virtualPosition;
    oldTrackingPosition = currentTrackingPosition;

    //Try to correct RealWalingSpeed for state detection
    vec3 angularDiff = userKinematic.AngularVelocity * (float)frameTime;
    vec4 tempResult = glm::rotate(mat4(1.0f), angularDiff.y, vec3(0,1,0)) * vec4(0,0,rotationCenterOffset, 0);
    float rotatedDistance = glm::length(vec3(0,0,rotationCenterOffset) - vec3(tempResult.x,tempResult.y,tempResult.z));
    //Log("rotatedDistance", to_string(rotatedDistance));

    if(rotatedDistance < userKinematic.RealkWalkingSpeed)
    {
        userKinematic.RealkWalkingSpeed -= rotatedDistance;
    }
    //javaRuntime.setDebugText("Rotated distance:" + to_string(rotatedDistance) + " WalkSpeed: " + to_string(userKinematic.RealkWalkingSpeed));

    bool walking = false;
    bool moving = false;
    string stateString = "Still";

    if(glm::abs(userKinematic.AngularVelocity.y) > 0.08f)
    {
        moving = true;
        stateString = "Looking";
    }
    else
    {
        stateString = "Still";
    }

    if(userKinematic.RealkWalkingSpeed > 0.004)
    {
        walking = true;
        moving = true;
        stateString += "Walking";
    }

    stateString += "\n AngularVelocityY" + to_string(userKinematic.AngularVelocity.y);
    stateString += "\n WalkingSpeed" + to_string(userKinematic.RealkWalkingSpeed);

    //javaRuntime.setDebugText(stateString);

    if (lastIsInitialized)
    {
        //Set this value to 0 to have only the real world rotation.
        float rotationFactor = 0; //additional to the normal rotation (0 is normal rotation only)
        vec3 toRealTarget = getCurrentRealTargetPosition() - userKinematic.RealPosition;

        vec3 toVirtualTarget = virtualToRealPosition(getCurrentVirtualTargetPosition()) - virtualToRealPosition(userKinematic.VirtualPosition);
        vec3 toRealTargetXZ = vec3(toRealTarget.x, 0, toRealTarget.z);
        vec3 toVirtualTargetXZ = vec3(toVirtualTarget.x, 0, toVirtualTarget.z);


        float tempClampResult = glm::dot(toVirtualTargetXZ, toRealTargetXZ) / (glm::length(toVirtualTargetXZ) * glm::length(toRealTargetXZ));
        tempClampResult = glm::clamp(tempClampResult, -1.0f, 1.0f);
        float angleToTarget = glm::acos(tempClampResult);
        float sign = glm::sign(glm::cross(toVirtualTargetXZ, toRealTargetXZ).y);

        float walkSpeedFactor = userKinematic.RealkWalkingSpeed * 100; //map [0, 0.01] to [0,1]
        float angularVelocityFactor = glm::abs(userKinematic.AngularVelocity.y) * 0.5f; //map [0, ~2] to [0,1]

        //Log("ANGLE", to_string(angleToTarget));
        //javaRuntime.setDebugText("Angle v/r: " + to_string(glm::degrees(angleToTarget)));

        if(angleToTarget > glm::radians(5.0f))
        {
            rotationFactor = 0.02f * frameTime * sign; //Stand still
            rotationFactor += 0.08f * frameTime * walkSpeedFactor * sign;
            rotationFactor += 0.14f * frameTime * angularVelocityFactor * sign;

            if(angleToTarget >= glm::radians(30.0f))
            {
                rotationFactor += 0.3f * frameTime * angleToTarget * angularVelocityFactor * sign;
            }
        }


        javaRuntime.setDebugText("Trans. gain: " + to_string(translationGainFactor) + "\nRot. gain: "
                                 + to_string(rotationFactor));

        rotationFactorMax = glm::max(rotationFactorMax, rotationFactor);
        //javaRuntime.setDebugText(to_string(rotationFactorMax));

        additionalSceneRotation = additionalSceneRotation * glm::rotate(mat4(1.0f), rotationFactor, vec3(0, 1, 0));
    }

    lastIsInitialized = true;
}

void TestScene::DrawEye(int eye, const ovrTracking *tracking, ovrHeadModelParms &headModelParms, ovrTracking &updatedTracking)
{
    glm::quat rotation = toGlm(updatedTracking.HeadPose.Pose.Orientation);
    const ovrVector3f centerEyeOffset = tracking->HeadPose.Pose.Position;

    mat4 manualViewMatrix = glm::translate(mat4(1.0f), vec3(0,0,adjustableEyeOffset)) * glm::inverse(glm::mat4_cast(rotation)); //head rotation
    const float eyeOffset = ( eye ? -0.5f : 0.5f ) * headModelParms.InterpupillaryDistance;
    manualViewMatrix = glm::translate(mat4(1.0f), vec3(eyeOffset,0,0)) * manualViewMatrix;

    // The real view matrix contains the HMDs Rotation and the eye offset
    mat4 realViewMatrix = manualViewMatrix * glm::rotate(mat4(1.0f), 3.14159265359f/2.0f, vec3(0,1,0));

    //Do these calculations only once per frame. eye == 0
    if(eye == 0)
    {
        computeManipulation(tracking, realViewMatrix);
    }

    mat4 virtualViewMatrix = realViewMatrix * additionalSceneRotation * glm::translate(mat4(1.0f), -virtualPosition);
    //For the final rendering the realViewMatrix contains the currentTrackingPosition to draw object at there real position.
    realViewMatrix = realViewMatrix * glm::translate(mat4(1.0f), -currentTrackingPosition);

    roomRenderer.Render(mat4(1.0f), virtualViewMatrix, projectionMatrix, hasCompleteBody ? vec3(1, 1, 1) : vec3(1, 0, 0));

    cubePosition = virtualTargetPositions[currentTargetIndex] + vec3(0,0.125f,0);
    mat4 cubeTranslation = glm::translate(mat4(1.0f), cubePosition);
    cubeRenderer.Render(cubeTranslation * cubeRotation, virtualViewMatrix, projectionMatrix);


    bool drawRealWorldTarget = false;
    if(drawRealWorldTarget)
    {
        mat4 realTarget = glm::translate(mat4(1.0f), realTargetPositions[currentTargetIndex] + vec3(0,0.125f,0));
        cubeRenderer.Render(realTarget, realViewMatrix, projectionMatrix);
    }

    //Set the boundary color for different app states.
    vec3 color = vec3(1,1,0);
    bool lagging = (currentFrame - localFrameTimeStamp) > framesWithoutPackageTillLag;
    if(lagging)
    {
        color = vec3(1,0,0);
    }
    else if(!hasCompleteBody)
    {
        color = vec3(1,0.4f,0);
    }
    else if(dataCaptureManager.IsCapturing())
    {
        color = vec3(0,1,0);
    }

#ifdef DRAW_BOUNDARY
    boundaryRenderer.Render(currentTrackingPosition, color, realViewMatrix, projectionMatrix,
                            sceneMin.x, sceneMin.y, sceneMax.x - sceneMin.x, sceneMax.y - sceneMin.y);
#endif

    currentFrame++;
    lastVirtualViewMatrix = virtualViewMatrix;
    lastRealViewMatrix = realViewMatrix;
}

void TestScene::ReceivePackage(void *buffer, int length, JNIEnv *env)
{
    int header0 = *((int*)buffer);
    int header1 = *(((int*)buffer) + 1);
    int header2 = *(((int*)buffer) + 2);

    if(header0 == 42 && header1 == 0 && header2 == 42)
    {
        DebugControlMessage *messagePtr = ((DebugControlMessage *)(((char*)buffer) + sizeof(int)*3));
        DebugControlMessage message = messagePtr[0];
        adjustableEyeOffset = message.adjustableEyeOffset;
        rotationCenterOffset = message.rotationCenterOffset;
    }

    unsigned long frameNr;
    vector<body> bodies;
    vector<marker> markers;
    ParseMessage(buffer, length, frameNr, bodies, markers);

    if(lastFrameNr < frameNr || (lastFrameNr - frameNr) > 1000)
    {
        hasCompleteBody = false;

        /*if(markers.size() > 0)
        {
            vec3 sum = vec3(0,0,0);

            for (int i = 0; i < markers.size() ; ++i)
            {
                sum += vec3(markers[i].loc[0] / 1000.0f, markers[i].loc[1] / 1000.0f, markers[i].loc[2] / 1000.0f);
            }

            sum *= 1.0f / markers.size();

            currentTrackingPosition = sum;

            LogError("Marker!");
        }*/

        if(bodies.size() > 0)
        {
            for (int i = 0; i < bodies.size(); ++i)
            {
                body body = bodies[i];
                if(body.id == 9)
                {
                    //Head body
                    currentTrackingPosition.x = body.loc[0] / 1000.0f;
                    currentTrackingPosition.y = body.loc[1] / 1000.0f;
                    currentTrackingPosition.z = body.loc[2] / 1000.0f;

                    __android_log_print(ANDROID_LOG_ERROR, "Input" , "%f %f %f", currentTrackingPosition.x, currentTrackingPosition.y, currentTrackingPosition.z);
                    hasCompleteBody = true;

                    if(dataCaptureManager.IsCapturing())
                    {
                        Capturedata data;
                        data.x = currentTrackingPosition.x;
                        data.y = currentTrackingPosition.z;
                        data.frameNr = frameNr;

                        dataCaptureManager.AddData(data);
                    }
                }
                else if(body.id == 2)
                {
                    cubePosition.x = body.loc[0] / 1000.0f;
                    cubePosition.y = body.loc[1] / 1000.0f;
                    cubePosition.z = body.loc[2] / 1000.0f;

                    cubeRotation = mat4x4(
                            body.rot[0],body.rot[1],body.rot[2],0,
                            body.rot[3],body.rot[4],body.rot[5],0,
                            body.rot[6],body.rot[7],body.rot[8],0,
                            0,          0,          0,          1
                    );
                }
            }
        }

        localFrameTimeStamp = currentFrame;
        lastFrameNr = frameNr;
    }
}

void TestScene::Destroy()
{
    roomRenderer.Unload();
    cubeRenderer.Unload();
}