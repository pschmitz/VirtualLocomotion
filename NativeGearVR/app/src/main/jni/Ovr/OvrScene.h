//
// Created by Benni on 07.10.2016.
//

#ifndef NATIVEGEARVR_OVRSCENE_H
#define NATIVEGEARVR_OVRSCENE_H

#include <VrApi.h>
#include <VrApi_Helpers.h>
#include "OvrFramebuffer.h"

#include "../Framework/JavaRuntime.h"
#include "../Framework/Scene.h"

#define NUM_MULTI_SAMPLES    4

class OvrScene
{
private:
    OvrFramebuffer FrameBuffer[VRAPI_FRAME_LAYER_EYE_MAX];
    ovrMatrix4f ProjectionMatrix;
    ovrMatrix4f TexCoordsTanAnglesMatrix;
    double oldPredictedDisplayTime = 0;

public:
    Scene *scene;

    void Init(Scene *scene);

    void Load(const ovrJava *java);

    void Update(ovrMobile *ovr, double predictedDisplayTime, bool isTouching);

    ovrFrameParms DrawEyes(const ovrJava *java,
                           long long frameIndex, int minimumVsyncs,
                           const ovrPerformanceParms *perfParms,
                           const ovrTracking *tracking, ovrMobile *ovr);

    void Destroy();
    bool IsCreated();
};


#endif //NATIVEGEARVR_OVRSCENE_H
