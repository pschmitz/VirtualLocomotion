//
// Created by Benni on 07.10.2016.
//

#define REDUCED_LATENCY

#include "OvrScene.h"
#include "../TestScene.h"
#include "../Framework/Math.h"

void OvrScene::Init(Scene *scene)
{
    for (int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++)
    {
        this->FrameBuffer[eye].Clear();
    }
    this->ProjectionMatrix = ovrMatrix4f_CreateIdentity();
    this->TexCoordsTanAnglesMatrix = ovrMatrix4f_CreateIdentity();
    this->scene = scene;
    this->scene ->Init();
}

void OvrScene::Load(const ovrJava *java)
{
    // Create the frame buffers.
    for (int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++)
    {
        this->FrameBuffer[eye].Create(
                VRAPI_TEXTURE_FORMAT_8888,
                vrapi_GetSystemPropertyInt(java,
                                           VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_WIDTH),
                vrapi_GetSystemPropertyInt(java,
                                           VRAPI_SYS_PROP_SUGGESTED_EYE_TEXTURE_HEIGHT),
                NUM_MULTI_SAMPLES);
    }

    // Setup the projection matrix.
    this->ProjectionMatrix = ovrMatrix4f_CreateProjectionFov(
            vrapi_GetSystemPropertyFloat(java, VRAPI_SYS_PROP_SUGGESTED_EYE_FOV_DEGREES_X),
            vrapi_GetSystemPropertyFloat(java, VRAPI_SYS_PROP_SUGGESTED_EYE_FOV_DEGREES_Y),
            0.0f, 0.0f, 0.001f, 100.0f);
    this->TexCoordsTanAnglesMatrix = ovrMatrix4f_TanAngleMatrixFromProjection(
            &this->ProjectionMatrix);

    scene->projectionMatrix = toGlm(ProjectionMatrix);
    scene->Load();
}


void OvrScene::Update(ovrMobile *ovr, double predictedDisplayTime, bool isTouching)
{
    if(oldPredictedDisplayTime == 0)
    {
        oldPredictedDisplayTime = predictedDisplayTime;
    }

    double predictedDisplayTimeDelta = predictedDisplayTime - oldPredictedDisplayTime;
    scene->Update(ovr, predictedDisplayTime, predictedDisplayTimeDelta, isTouching);
    oldPredictedDisplayTime = predictedDisplayTime;
}

ovrFrameParms OvrScene::DrawEyes(const ovrJava *java,
                       long long frameIndex, int minimumVsyncs,
                       const ovrPerformanceParms *perfParms,
                       const ovrTracking *tracking, ovrMobile *ovr)
{
    ovrFrameParms parms = vrapi_DefaultFrameParms(java, VRAPI_FRAME_INIT_DEFAULT,
                                                  vrapi_GetTimeInSeconds(), NULL);
    parms.FrameIndex = frameIndex;
    parms.MinimumVsyncs = minimumVsyncs;
    parms.PerformanceParms = *perfParms;
    parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Flags |= VRAPI_FRAME_LAYER_FLAG_CHROMATIC_ABERRATION_CORRECTION;

    // Calculate the center view matrix.
    /*const */ovrHeadModelParms headModelParms = vrapi_DefaultHeadModelParms();

    // Render the eye images.
    for (int eye = 0; eye < VRAPI_FRAME_LAYER_EYE_MAX; eye++)
    {
        OvrFramebuffer *frameBuffer = &this->FrameBuffer[eye];
        frameBuffer->SetCurrent();

        GL(glEnable(GL_SCISSOR_TEST));
        GL(glDepthMask(GL_TRUE));
        GL(glEnable(GL_DEPTH_TEST));
        GL(glDepthFunc(GL_LEQUAL));
        GL(glViewport(0, 0, frameBuffer->Width, frameBuffer->Height));
        GL(glScissor(0, 0, frameBuffer->Width, frameBuffer->Height));
        GL(glClearColor(0.125f, 0.0f, 0.125f, 1.0f));
        GL(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));


        // updated sensor prediction for each eye (updates orientation, not position)
#ifdef REDUCED_LATENCY
        ovrTracking updatedTracking = vrapi_GetPredictedTracking( ovr, tracking->HeadPose.TimeInSeconds );
        updatedTracking.HeadPose.Pose.Position = tracking->HeadPose.Pose.Position;
#else
        ovrTracking updatedTracking = *tracking;
#endif

        //TODO: draw per eye
        scene->DrawEye(eye, tracking, headModelParms, updatedTracking);

        GL(glBindVertexArray(0));
        GL(glUseProgram(0));

        // Explicitly clear the border texels to black because OpenGL-ES does not support GL_CLAMP_TO_BORDER.
        {
            // Clear to fully opaque black.
            GL(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
            // bottom
            GL(glScissor(0, 0, frameBuffer->Width, 1));
            GL(glClear(GL_COLOR_BUFFER_BIT));
            // top
            GL(glScissor(0, frameBuffer->Height - 1, frameBuffer->Width, 1));
            GL(glClear(GL_COLOR_BUFFER_BIT));
            // left
            GL(glScissor(0, 0, 1, frameBuffer->Height));
            GL(glClear(GL_COLOR_BUFFER_BIT));
            // right
            GL(glScissor(frameBuffer->Width - 1, 0, 1, frameBuffer->Height));
            GL(glClear(GL_COLOR_BUFFER_BIT));
        }

        frameBuffer->Resolve();

        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].ColorTextureSwapChain = frameBuffer->ColorTextureSwapChain;
        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].TextureSwapChainIndex = frameBuffer->TextureSwapChainIndex;
        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].TexCoordsFromTanAngles = this->TexCoordsTanAnglesMatrix;
        parms.Layers[VRAPI_FRAME_LAYER_TYPE_WORLD].Textures[eye].HeadPose = updatedTracking.HeadPose;

        frameBuffer->Advance();
    }

    OvrFramebuffer::SetNone();
    return parms;
}


void OvrScene::Destroy()
{
    scene->Destroy();
    delete scene;
    scene = nullptr;
}

bool OvrScene::IsCreated()
{
    return true;
}