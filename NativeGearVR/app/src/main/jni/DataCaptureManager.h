//
// Created by Benni on 14.10.2016.
//

#ifndef NATIVEGEARVR_DATACAPTUREMANAGER_H
#define NATIVEGEARVR_DATACAPTUREMANAGER_H

#include <vector>
#include "Framework/JavaRuntime.h"

using namespace std;

struct Capturedata
{
    unsigned long frameNr;
    float x;
    float y;

    //float walkingSpeed;
};

class DataCaptureManager
{
private:
    bool capturing = false;
    bool finishedCapturing = true;
    float currentCaptureTime = 0;
    float targetTime;

    int numberOfCaptureSessions = 0;

    vector<Capturedata> capturedData;
    JavaRuntime *javaRuntime;
public:
    void Init(JavaRuntime *javaRuntime) { this->javaRuntime = javaRuntime; }

    void Start(float time);
    void Update(float frameTime);

    void AddData(Capturedata &data);

    bool IsCapturing() { return capturing; }
private:
    void saveData();
};


#endif //NATIVEGEARVR_DATACAPTUREMANAGER_H
