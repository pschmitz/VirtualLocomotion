//
// Created by Benni on 14.10.2016.
//

#include "DataCaptureManager.h"
#include "Framework/Log.h"

void DataCaptureManager::AddData(Capturedata &data)
{
    if(capturing)
    {
        capturedData.push_back(data);
    }
}

void DataCaptureManager::Start(float time)
{
    if (!capturing && finishedCapturing)
    {
        currentCaptureTime = 0;
    }
}

void DataCaptureManager::Update(float frameTime)
{
    if (capturing)
    {
        javaRuntime->setDebugText("Capturing" + to_string(capturedData.size()));
    }

    if (capturing && currentCaptureTime >= targetTime + 0.5f)
    {
        saveData();
        capturedData.clear();
        finishedCapturing = true;
        capturing = false;
    }
    else if (capturing && currentCaptureTime >= targetTime)
    {
        javaRuntime->setDebugText("Stopped Capturing ... saving data");
        finishedCapturing = false;
        capturing = false;
    }
}

void DataCaptureManager::saveData()
{
    //wait 5 seconds to avoid threading issues
    LogError("Save capture");

    string output;

    for (int i = 0; i < capturedData.size(); ++i)
    {
        output.append(
                to_string(capturedData[i].frameNr) + " " + to_string(capturedData[i].x) +
                " " + to_string(capturedData[i].y) + "\n");
    }

    this->javaRuntime->saveTextToFile(
            string("CapturedData") + to_string(numberOfCaptureSessions) + ".txt", output);

    numberOfCaptureSessions++;
    javaRuntime->setDebugText("Ready");
}