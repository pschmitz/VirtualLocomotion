//
// Created by Benni on 29.07.2016.
//

#include "Math.h"
#include <glm/glm.hpp>
#include <glm/gtx/matrix_decompose.hpp>

string to_string(mat4 matrix)
{
    return to_string(matrix[0].x) + to_string(matrix[1].x) + to_string(matrix[2].x)+ to_string(matrix[3].x) + "\n" +
    to_string(matrix[0].y) + to_string(matrix[1].y) + to_string(matrix[2].y)+ to_string(matrix[3].y) + "\n" +
    to_string(matrix[0].z) + to_string(matrix[1].z) + to_string(matrix[2].z)+ to_string(matrix[3].z) + "\n" +
    to_string(matrix[0].w) + to_string(matrix[1].w) + to_string(matrix[2].w)+ to_string(matrix[3].w);
}

string to_string(vec3 vector)
{
    return to_string(vector.x) + ", " + to_string(vector.y) + ", " + to_string(vector.z);
}


mat4 extractRotation(mat4 matrix)
{
    vec3 scale;
    quat orientation;
    vec3 translation;
    vec3 skew;
    vec4 perspective;

    glm::decompose(matrix, scale, orientation, translation, skew, perspective);
    return glm::mat4_cast(orientation);
}


glm::quat toGlm(const ovrQuatf quat)
{
    glm::quat quaternion;
    quaternion.x = quat.x;
    quaternion.y = quat.y;
    quaternion.z = quat.z;
    quaternion.w = quat.w;
    return quaternion;
}


vec3 toGlm(ovrVector3f vector)
{
    return vec3(vector.x, vector.y, vector.z);
}

mat4 toGlm(const ovrMatrix4f matrix)
{
    glm::mat4 glmMatrix;
    /*glmMatrix[0][0] = matrix.M[0][0];
    glmMatrix[0][1] = matrix.M[0][1];
    glmMatrix[0][2] = matrix.M[0][2];
    glmMatrix[0][3] = matrix.M[0][3];

    glmMatrix[1][0] = matrix.M[1][0];
    glmMatrix[1][1] = matrix.M[1][1];
    glmMatrix[1][2] = matrix.M[1][2];
    glmMatrix[1][3] = matrix.M[1][3];

    glmMatrix[2][0] = matrix.M[2][0];
    glmMatrix[2][1] = matrix.M[2][1];
    glmMatrix[2][2] = matrix.M[2][2];
    glmMatrix[2][3] = matrix.M[2][3];

    glmMatrix[3][0] = matrix.M[3][0];
    glmMatrix[3][1] = matrix.M[3][1];
    glmMatrix[3][2] = matrix.M[3][2];
    glmMatrix[3][3] = matrix.M[3][3];*/

    glmMatrix[0][0] = matrix.M[0][0];
    glmMatrix[0][1] = matrix.M[1][0];
    glmMatrix[0][2] = matrix.M[2][0];
    glmMatrix[0][3] = matrix.M[3][0];

    glmMatrix[1][0] = matrix.M[0][1];
    glmMatrix[1][1] = matrix.M[1][1];
    glmMatrix[1][2] = matrix.M[2][1];
    glmMatrix[1][3] = matrix.M[3][1];

    glmMatrix[2][0] = matrix.M[0][2];
    glmMatrix[2][1] = matrix.M[1][2];
    glmMatrix[2][2] = matrix.M[2][2];
    glmMatrix[2][3] = matrix.M[3][2];

    glmMatrix[3][0] = matrix.M[0][3];
    glmMatrix[3][1] = matrix.M[1][3];
    glmMatrix[3][2] = matrix.M[2][3];
    glmMatrix[3][3] = matrix.M[3][3];
    return glmMatrix;
}
