//
// Created by Benni on 07.07.2016.
//

#include "FileReader.h"
#include <android/asset_manager_jni.h>

namespace FileReader
{
    string readTextFromAssetFile(JavaRuntime &javaRuntime, string filename)
    {
        AAsset* asset = AAssetManager_open(javaRuntime.assetManager, (const char *) filename.c_str(), AASSET_MODE_UNKNOWN);
        if (NULL == asset)
        {
            //__android_log_print(ANDROID_LOG_ERROR, NF_LOG_TAG, "_ASSET_NOT_FOUND_");
            return JNI_FALSE;
        }
        long size = AAsset_getLength(asset);
        size += 1;
        char* buffer = (char*) malloc (sizeof(char)*size);

        buffer[size-1] = '\0';

        AAsset_read (asset,buffer,size);
        //__android_log_print(ANDROID_LOG_ERROR, NF_LOG_TAG, buffer);
        AAsset_close(asset);
        string tempStr = string(buffer);
        free(buffer);
        return tempStr;
    }
};