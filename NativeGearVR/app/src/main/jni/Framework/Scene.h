//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_SCENE_H
#define NATIVEGEARVR_SCENE_H

#include <VrApi.h>
#include <VrApi_Helpers.h>
#include <glm/glm.hpp>

#include "JavaRuntime.h"

using namespace glm;

/*abstract*/ class Scene
{
public:
    JavaRuntime javaRuntime;
    mat4 projectionMatrix;

public:
    virtual ~Scene() {}

    virtual void Init() = 0;
    virtual void Load() = 0;
    virtual void Update(ovrMobile *ovr, double predictedDisplayTime, double predictedDisplayTimeDelta, bool isTouching) = 0;
    virtual void DrawEye(int eye, const ovrTracking *tracking, ovrHeadModelParms &headModelParms, ovrTracking &updatedTracking) = 0;
    virtual void Destroy() = 0;

    virtual void ReceivePackage(void *buffer, int length, JNIEnv *env) { }
};

#endif //NATIVEGEARVR_SCENE_H
