//
// Created by Benni on 07.07.2016.
//

#ifndef GEARVRFRAMEWORK_FILEREADER_H
#define GEARVRFRAMEWORK_FILEREADER_H

#include <string>
#include <android/asset_manager_jni.h>
#include "JavaRuntime.h"

using namespace std;

namespace FileReader
{
    string readTextFromAssetFile(JavaRuntime &javaRuntime, string filename);
};


#endif //GEARVRFRAMEWORK_FILEREADER_H
