//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_LOG_H
#define NATIVEGEARVR_LOG_H

#include <android/log.h>
#include <string>
using namespace std;

#define DEFAULT_LOG_TAG "NativeGearVr"
#define DEFAULT_ERROR_LOG_TAG "NativeGearVr"

#define ALOGE(...) __android_log_print( ANDROID_LOG_ERROR, DEFAULT_LOG_TAG, __VA_ARGS__ )
#if DEBUG
#define ALOGV(...) __android_log_print( ANDROID_LOG_VERBOSE, DEFAULT_LOG_TAG, __VA_ARGS__ )
#else
#define ALOGV(...)
#endif

void LogError(string str);
void LogError(string logTag, string str);
void Log(string str);
void Log(string logTag, string str);

#endif //NATIVEGEARVR_LOG_H
