//
// Created by Benni on 07.07.2016.
//

#ifndef NATIVEGEARVR_JAVARUNTIME_H
#define NATIVEGEARVR_JAVARUNTIME_H

#include <VrApi.h>
#include <VrApi_Helpers.h>
#include <android/asset_manager_jni.h>
#include <string>
#include "TextureData.h"

using namespace std;

/*
Type Signature

Java Type
Z

boolean
B

byte
C

char
S

short
I

int
J

long
F

float
D

double

L fully-qualified-class ;
fully-qualified-class

[ type
type[]

( arg-types ) ret-type
method type

For example, the Java method:

long f (int n, String s, int[] arr);

has the following type signature:

(ILjava/lang/String;[I)J

 */

struct JavaRuntime
{
    JNIEnv *env;
    AAssetManager *assetManager;
    jobject nativeToJava;

    void setDebugText(std::string text)
    {
        jclass clazz = env->GetObjectClass(nativeToJava);
        // Get the method that you want to call
        jmethodID methodId = env->GetMethodID(clazz, "SetDebugText", "(Ljava/lang/String;)V");
        jstring javaString = env->NewStringUTF(std::string(text).c_str());
        env->CallVoidMethod(nativeToJava, methodId, javaString);

        env->DeleteLocalRef(javaString);
        env->DeleteLocalRef(clazz);
    }

    void saveTextToFile(string filename, string text)
    {
        jclass clazz = env->GetObjectClass(nativeToJava);
        // Get the method that you want to call
        jmethodID methodId = env->GetMethodID(clazz, "SaveToExternalFile", "(Ljava/lang/String;Ljava/lang/String;)V");
        jstring javaString = env->NewStringUTF(std::string(filename).c_str());
        jstring javaString2 = env->NewStringUTF(std::string(text).c_str());
        env->CallVoidMethod(nativeToJava, methodId, javaString, javaString2);

        env->DeleteLocalRef(javaString);
        env->DeleteLocalRef(javaString2);
        env->DeleteLocalRef(clazz);
    }

    string loadTextFromFile(string filename)
    {
        jclass clazz = env->GetObjectClass(nativeToJava);
        // Get the method that you want to call
        jmethodID methodId = env->GetMethodID(clazz, "LoadFromExternalFile", "(Ljava/lang/String;)Ljava/lang/String;");

        jstring javaString = env->NewStringUTF(std::string(filename).c_str());
        jstring returnedString = (jstring)env->CallObjectMethod(nativeToJava, methodId, javaString);

        const char *nativeString = env->GetStringUTFChars(returnedString, 0);

        // use your string
        string result = string(nativeString);
        env->ReleaseStringUTFChars(javaString, nativeString);
        return result;
    }

    TextureData loadTextureData(string filename)
    {
        TextureData textureData;

        //TODO: test if you have to use DeleteLocalRef on all used jobjects
        //env->DeleteLocalRef()

        jclass clazz = env->GetObjectClass(nativeToJava);
        // Get the method that you want to call
        jmethodID methodId = env->GetMethodID(clazz, "LoadTextureData", "(Ljava/lang/String;)Lnativegearvr/TextureData;");
        jstring javaString = env->NewStringUTF(std::string(filename).c_str());

        jobject javaTextureData = env->CallObjectMethod(nativeToJava, methodId, javaString);
        jclass javaTextureClass = env->GetObjectClass(javaTextureData);

        jfieldID widthField = env->GetFieldID(javaTextureClass, "width", "I");
        jfieldID heightField = env->GetFieldID(javaTextureClass, "height", "I");
        jfieldID bytesField = env->GetFieldID(javaTextureClass, "bytes", "[B");

        textureData.width = env->GetIntField(javaTextureData, widthField);
        textureData.height = env->GetIntField(javaTextureData, heightField);
        jbyteArray javabytearray = (jbyteArray)env->GetObjectField(javaTextureData, bytesField);
        textureData.dataLength = env->GetArrayLength(javabytearray);

        jbyte *javaBytes = env->GetByteArrayElements(javabytearray, nullptr);
        textureData.data = new byte[textureData.dataLength];
        for (int i = 0; i < textureData.dataLength; ++i)
        {
            textureData.data[i] = javaBytes[i];
        }
        env->ReleaseByteArrayElements(javabytearray, javaBytes, 0);

        return textureData;
    }
};

#endif //NATIVEGEARVR_JAVARUNTIME_H