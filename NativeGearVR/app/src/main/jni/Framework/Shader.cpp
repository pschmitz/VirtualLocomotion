//
// Created by Benni on 22.07.2016.
//

#include "Shader.h"
#include "FileReader.h"

#include <exception>
#include <android/log.h>

namespace Shader
{
    GLuint compileShader(JavaRuntime &javaRuntime, string filename, GLenum shaderType)
    {
        string vscontent = FileReader::readTextFromAssetFile(javaRuntime, filename);

        GLuint shader = glCreateShader(shaderType);
        auto temp = vscontent.c_str();
        int length = vscontent.size();

        glShaderSource(shader, 1, &temp, &length);

        glCompileShader(shader);
        int logLength;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        if (logLength > 0)
        {
            GLchar *log = (GLchar *) malloc(logLength);
            glGetShaderInfoLog(shader, logLength, &logLength, log);
            if (strcmp(log, "") != 0)
            {
                __android_log_print(ANDROID_LOG_ERROR, "Shader" ,"Shader compilation failed: %s",log);
                //throw std::exception();
            }
            free(log);
        }

        return shader;
    }
}