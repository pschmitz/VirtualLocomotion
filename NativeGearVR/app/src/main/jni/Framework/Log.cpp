//
// Created by Benni on 29.07.2016.
//

#include "Log.h"

void LogError(string str)
{
    __android_log_print( ANDROID_LOG_ERROR, DEFAULT_ERROR_LOG_TAG,str.c_str() );
}

void Log(string str)
{
    __android_log_print( ANDROID_LOG_DEBUG, DEFAULT_LOG_TAG,str.c_str() );
}

void LogError(string logTag, string str)
{
    __android_log_print( ANDROID_LOG_ERROR, logTag.c_str(),str.c_str() );
}

void Log(string logTag, string str)
{
    __android_log_print( ANDROID_LOG_DEBUG, logTag.c_str(),str.c_str() );
}