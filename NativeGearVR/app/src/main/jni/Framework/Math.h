//
// Created by Benni on 29.07.2016.
//

#ifndef NATIVEGEARVR_MATH_H
#define NATIVEGEARVR_MATH_H

#include "VrApi_Types.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
using namespace std;
using namespace glm;

vec3 toGlm(ovrVector3f vector);
mat4 toGlm(const ovrMatrix4f matrix);
glm::quat toGlm(const ovrQuatf quat);

mat4 extractRotation(mat4 matrix);

string to_string(mat4 matrix);
string to_string(vec3 vector);

#endif //NATIVEGEARVR_MATH_H
