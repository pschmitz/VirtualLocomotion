//
// Created by Benni on 30.09.2016.
//

#ifndef NATIVEGEARVR_TEXTUREDATA_H
#define NATIVEGEARVR_TEXTUREDATA_H

#define byte unsigned char

struct TextureData
{
    byte *data;
    int dataLength;
    int width;
    int height;

    void Release()
    {
        delete[] data;
        width = 0;
        height = 0;
        dataLength = 0;
    }
};

#endif //NATIVEGEARVR_TEXTUREDATA_H
