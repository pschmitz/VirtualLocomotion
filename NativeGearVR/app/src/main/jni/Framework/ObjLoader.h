//
// Created by Benni on 29.07.2016.
//

#ifndef NATIVEGEARVR_OBJLOADER_H
#define NATIVEGEARVR_OBJLOADER_H

#include <string>
#include <vector>
#include <stdio.h>
#include <glm/glm.hpp>

using namespace std;

/*!
 * An object loader loads the vertex data from a given file
 */
class ObjLoader
{
private:
    vector<glm::vec3> positions;
    vector<glm::vec3> normals;
    vector<glm::vec2> texCoords;
    vector<int> positionIndices;
    vector<int> normalIndices;
    vector<int> texCoordIndices;
    int vertexCount;
public:
    /*!
     * Loads an object from a file. It gives back the positions, normals, texCoords and indices if there
     * @param filename Input: the path to the obj file
     * @param vertexCount Output: The number of vertices in the file
     * @param positionData Output: The position values of the vertices
     * @param normalData Output: The normal values of the vertices
     * @param texCoordData Output: The texture coordinate values of the vertices
     * @param indices Output: the indices
     */
    void loadObjFile(string &objFileContent, vector<glm::vec3> &positions, vector<glm::vec3> &normals, vector<glm::vec2> &texcoords, vector<unsigned int> &indices);

private:
    void reset();
    void handleLine(string &line);

    void addPosition(string &line);
    void addNormal(string &line);
    void addTexCoord(string &line);
    void addIndices(string &line);

    float* stringToFloatArray(string str, uint8_t assertSize);
    int* faceIndexStringToIndexArray(string str);
};


#endif //NATIVEGEARVR_OBJLOADER_H
