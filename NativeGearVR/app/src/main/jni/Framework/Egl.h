//
// Created by Benni on 23.06.2016.
//

#ifndef NATIVEGEARVR_EGL_H
#define NATIVEGEARVR_EGL_H

#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "Log.h"

static const char *EglErrorString(const EGLint error)
{
    switch (error)
    {
        case EGL_SUCCESS:
            return "EGL_SUCCESS";
        case EGL_NOT_INITIALIZED:
            return "EGL_NOT_INITIALIZED";
        case EGL_BAD_ACCESS:
            return "EGL_BAD_ACCESS";
        case EGL_BAD_ALLOC:
            return "EGL_BAD_ALLOC";
        case EGL_BAD_ATTRIBUTE:
            return "EGL_BAD_ATTRIBUTE";
        case EGL_BAD_CONTEXT:
            return "EGL_BAD_CONTEXT";
        case EGL_BAD_CONFIG:
            return "EGL_BAD_CONFIG";
        case EGL_BAD_CURRENT_SURFACE:
            return "EGL_BAD_CURRENT_SURFACE";
        case EGL_BAD_DISPLAY:
            return "EGL_BAD_DISPLAY";
        case EGL_BAD_SURFACE:
            return "EGL_BAD_SURFACE";
        case EGL_BAD_MATCH:
            return "EGL_BAD_MATCH";
        case EGL_BAD_PARAMETER:
            return "EGL_BAD_PARAMETER";
        case EGL_BAD_NATIVE_PIXMAP:
            return "EGL_BAD_NATIVE_PIXMAP";
        case EGL_BAD_NATIVE_WINDOW:
            return "EGL_BAD_NATIVE_WINDOW";
        case EGL_CONTEXT_LOST:
            return "EGL_CONTEXT_LOST";
        default:
            return "unknown";
    }
}

class ovrEgl
{
public:
    EGLint MajorVersion;
    EGLint MinorVersion;
    EGLDisplay Display;
    EGLConfig Config;
    EGLSurface TinySurface;
    EGLSurface MainSurface;
    EGLContext Context;

    void Clear()
    {
        this->MajorVersion = 0;
        this->MinorVersion = 0;
        this->Display = 0;
        this->Config = 0;
        this->TinySurface = EGL_NO_SURFACE;
        this->MainSurface = EGL_NO_SURFACE;
        this->Context = EGL_NO_CONTEXT;
    }

    void CreateContext(const ovrEgl *shareEgl)
    {
        if (this->Display != 0)
        {
            return;
        }

        this->Display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
        ALOGV("        eglInitialize( Display, &MajorVersion, &MinorVersion )");
        eglInitialize(this->Display, &this->MajorVersion, &this->MinorVersion);
        // Do NOT use eglChooseConfig, because the Android EGL code pushes in multisample
        // flags in eglChooseConfig if the user has selected the "force 4x MSAA" option in
        // settings, and that is completely wasted for our warp target.
        const int MAX_CONFIGS = 1024;
        EGLConfig configs[MAX_CONFIGS];
        EGLint numConfigs = 0;
        if (eglGetConfigs(this->Display, configs, MAX_CONFIGS, &numConfigs) == EGL_FALSE)
        {
            ALOGE("        eglGetConfigs() failed: %s", EglErrorString(eglGetError()));
            return;
        }
        const EGLint configAttribs[] =
                {
                        EGL_ALPHA_SIZE, 8, // need alpha for the multi-pass timewarp compositor
                        EGL_BLUE_SIZE, 8,
                        EGL_GREEN_SIZE, 8,
                        EGL_RED_SIZE, 8,
                        EGL_DEPTH_SIZE, 0,
                        EGL_SAMPLES, 0,
                        EGL_NONE
                };
        this->Config = 0;
        for (int i = 0; i < numConfigs; i++)
        {
            EGLint value = 0;

            eglGetConfigAttrib(this->Display, configs[i], EGL_RENDERABLE_TYPE, &value);
            if ((value & EGL_OPENGL_ES3_BIT_KHR) != EGL_OPENGL_ES3_BIT_KHR)
            {
                continue;
            }

            // The pbuffer config also needs to be compatible with normal window rendering
            // so it can share textures with the window context.
            eglGetConfigAttrib(this->Display, configs[i], EGL_SURFACE_TYPE, &value);
            if ((value & (EGL_WINDOW_BIT | EGL_PBUFFER_BIT)) != (EGL_WINDOW_BIT | EGL_PBUFFER_BIT))
            {
                continue;
            }

            int j = 0;
            for (; configAttribs[j] != EGL_NONE; j += 2)
            {
                eglGetConfigAttrib(this->Display, configs[i], configAttribs[j], &value);
                if (value != configAttribs[j + 1])
                {
                    break;
                }
            }
            if (configAttribs[j] == EGL_NONE)
            {
                this->Config = configs[i];
                break;
            }
        }
        if (this->Config == 0)
        {
            ALOGE("        eglChooseConfig() failed: %s", EglErrorString(eglGetError()));
            return;
        }
        EGLint contextAttribs[] =
                {
                        EGL_CONTEXT_CLIENT_VERSION, 3,
                        EGL_NONE
                };
        ALOGV("        Context = eglCreateContext( Display, Config, EGL_NO_CONTEXT, contextAttribs )");
        this->Context = eglCreateContext(this->Display, this->Config,
                                         (shareEgl != NULL) ? shareEgl->Context : EGL_NO_CONTEXT,
                                         contextAttribs);
        if (this->Context == EGL_NO_CONTEXT)
        {
            ALOGE("        eglCreateContext() failed: %s", EglErrorString(eglGetError()));
            return;
        }
        const EGLint surfaceAttribs[] =
                {
                        EGL_WIDTH, 16,
                        EGL_HEIGHT, 16,
                        EGL_NONE
                };
        ALOGV("        TinySurface = eglCreatePbufferSurface( Display, Config, surfaceAttribs )");
        this->TinySurface = eglCreatePbufferSurface(this->Display, this->Config, surfaceAttribs);
        if (this->TinySurface == EGL_NO_SURFACE)
        {
            ALOGE("        eglCreatePbufferSurface() failed: %s", EglErrorString(eglGetError()));
            eglDestroyContext(this->Display, this->Context);
            this->Context = EGL_NO_CONTEXT;
            return;
        }
        ALOGV("        eglMakeCurrent( Display, TinySurface, TinySurface, Context )");
        if (eglMakeCurrent(this->Display, this->TinySurface, this->TinySurface, this->Context) == EGL_FALSE)
        {
            ALOGE("        eglMakeCurrent() failed: %s", EglErrorString(eglGetError()));
            eglDestroySurface(this->Display, this->TinySurface);
            eglDestroyContext(this->Display, this->Context);
            this->Context = EGL_NO_CONTEXT;
            return;
        }
    }

    void DestroyContext()
    {
        if (this->Display != 0)
        {
            ALOGE("        eglMakeCurrent( Display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT )");
            if (eglMakeCurrent(this->Display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT) ==
                EGL_FALSE)
            {
                ALOGE("        eglMakeCurrent() failed: %s", EglErrorString(eglGetError()));
            }
        }
        if (this->Context != EGL_NO_CONTEXT)
        {
            ALOGE("        eglDestroyContext( Display, Context )");
            if (eglDestroyContext(this->Display, this->Context) == EGL_FALSE)
            {
                ALOGE("        eglDestroyContext() failed: %s", EglErrorString(eglGetError()));
            }
            this->Context = EGL_NO_CONTEXT;
        }
        if (this->TinySurface != EGL_NO_SURFACE)
        {
            ALOGE("        eglDestroySurface( Display, TinySurface )");
            if (eglDestroySurface(this->Display, this->TinySurface) == EGL_FALSE)
            {
                ALOGE("        eglDestroySurface() failed: %s", EglErrorString(eglGetError()));
            }
            this->TinySurface = EGL_NO_SURFACE;
        }
        if (this->Display != 0)
        {
            ALOGE("        eglTerminate( Display )");
            if (eglTerminate(this->Display) == EGL_FALSE)
            {
                ALOGE("        eglTerminate() failed: %s", EglErrorString(eglGetError()));
            }
            this->Display = 0;
        }
    }

    void CreateSurface(ANativeWindow *nativeWindow)
    {
        if (this->MainSurface != EGL_NO_SURFACE)
        {
            return;
        }
        ALOGV("        MainSurface = eglCreateWindowSurface( Display, Config, nativeWindow, attribs )");
        const EGLint surfaceAttribs[] = {EGL_NONE};
        this->MainSurface = eglCreateWindowSurface(this->Display, this->Config, nativeWindow,
                                                   surfaceAttribs);
        if (this->MainSurface == EGL_NO_SURFACE)
        {
            ALOGE("        eglCreateWindowSurface() failed: %s", EglErrorString(eglGetError()));
            return;
        }
#if EXPLICIT_GL_OBJECTS == 0
        ALOGV("        eglMakeCurrent( display, MainSurface, MainSurface, Context )");
        if (eglMakeCurrent(this->Display, this->MainSurface, this->MainSurface, this->Context) == EGL_FALSE)
        {
            ALOGE("        eglMakeCurrent() failed: %s", EglErrorString(eglGetError()));
            return;
        }
#endif
    }

    void DestroySurface()
    {
#if EXPLICIT_GL_OBJECTS == 0
        if (this->Context != EGL_NO_CONTEXT)
        {
            ALOGV("        eglMakeCurrent( display, TinySurface, TinySurface, Context )");
            if (eglMakeCurrent(this->Display, this->TinySurface, this->TinySurface, this->Context) ==
                EGL_FALSE)
            {
                ALOGE("        eglMakeCurrent() failed: %s", EglErrorString(eglGetError()));
            }
        }
#endif
        if (this->MainSurface != EGL_NO_SURFACE)
        {
            ALOGV("        eglDestroySurface( Display, MainSurface )");
            if (eglDestroySurface(this->Display, this->MainSurface) == EGL_FALSE)
            {
                ALOGE("        eglDestroySurface() failed: %s", EglErrorString(eglGetError()));
            }
            this->MainSurface = EGL_NO_SURFACE;
        }
    }
};

#endif //NATIVEGEARVR_EGL_H
