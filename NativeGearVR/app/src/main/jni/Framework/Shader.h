//
// Created by Benni on 22.07.2016.
//

#ifndef NATIVEGEARVR_SHADER_H
#define NATIVEGEARVR_SHADER_H

#include <string>
#include <GLES3/gl3.h>
#include "JavaRuntime.h"

using namespace std;

namespace Shader
{
    GLuint compileShader(JavaRuntime &javaRuntime, string filename, GLenum shaderType);
}

#endif //NATIVEGEARVR_SHADER_H
