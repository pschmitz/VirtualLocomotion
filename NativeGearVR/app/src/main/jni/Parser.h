//
// Created by Benni on 14.07.2016.
//

#ifndef NATIVEGEARVR_PARSER_H
#define NATIVEGEARVR_PARSER_H

#include <vector>
using namespace std;

/*struct
{
    unsigned long framenr;

    int nbodies;
    struct body
    {
        unsigned long id;
        float quality;
        float loc[3];
        float ang[3];
        float rot[9];
    };

    int nmarkers;
    struct marker
    {
        unsigned long id;
        float quality;
        float loc[3];
    };
};*/

struct body
{
    unsigned long id;
    float quality;
    float loc[3];
    float ang[3];
    float rot[9];
};

struct marker
{
    unsigned long id;
    float quality;
    float loc[3];
};


void ParseMessage(void *buffer, int length, unsigned long &frameNr, vector<body> &bodies, vector<marker> &markers);

#endif //NATIVEGEARVR_PARSER_H
