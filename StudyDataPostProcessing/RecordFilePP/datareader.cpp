#include "datareader.h"
#include <sstream>
#include <vector>
#include <iostream>
#include <iomanip>

#include <glm/ext.hpp>

DataReader::DataReader()
{

}

glm::mat4 DataReader::ReadMat4(std::string strMatrix) {
    glm::mat4 mat;

    // remove [ ] brackets
    strMatrix.erase(0, 1);
    strMatrix.erase(strMatrix.length() - 1, 1);

    std::istringstream ssMatrix(strMatrix);
    std::string strRow;
    for(int row = 0 ; row < 4 ; ++row) {
        std::getline(ssMatrix, strRow, ';');
        strRow.erase(0, 1);

        std::string strEntry;
        std::istringstream ssRow(strRow);
        for(int col = 0 ; col < 4 ; ++col) {
            ssRow >> std::setprecision(std::numeric_limits<double>::digits10 + 1)
                  >> mat[col][row];
        }
    }

    return mat;
}

// glm::vec3 DataReader::ReadVec3(std::string str) {
//     return glm::vec3(0.0f, 0.0f, 0.0f);
// }

// float DataReader::ReadFloat(std::string str) {
//     return 0.0f;
// }
