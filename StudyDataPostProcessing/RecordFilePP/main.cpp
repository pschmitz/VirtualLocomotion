#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include <vector>

#include "datareader.h"
#include "PostProcessing.h"



void cutOffLinesAfterLastTargetCollected(std::string fileNameIn, std::string fileNameOut) {
    std::ifstream file( fileNameIn );
    std::ofstream fileOut( fileNameOut );

    if(file) {

        std::vector<std::string> allLines;

        std::string line;
        while(std::getline(file, line)) {
            allLines.push_back(line);
            //std::cout << line << std::endl;
        }
        file.close();

        std::string currentLastLine = allLines.back();
        while (currentLastLine.substr(0, 16).compare("TARGET_COLLECTED") != 0) {
            allLines.pop_back();
            currentLastLine = allLines.back();
//            std::cout << currentLastLine<< std::endl;
        }

//        allLines.pop_back();
//        std::cout << allLines.back() << std::endl;
//        allLines.pop_back();
//        std::cout << allLines.back() << std::endl;


        for (std::vector<std::string>::iterator it = allLines.begin() ; it != allLines.end(); ++it) {
            fileOut << *it << std::endl;
       }

        fileOut.close();
    } else {
        std::cout << "Record file not found. (0)" << std::endl;
    }
}

void removeNonKinematicLines(std::string fileNameIn, std::string fileNameOut) {
    std::ifstream file( fileNameIn );
    std::ofstream fileOut( fileNameOut );

    if(file) {

        std::vector<std::string> allLines;

        std::string line;
        while(std::getline(file, line)) {

            if (line.substr(0, 9).compare("KINEMATIC") == 0) {
                allLines.push_back(line);
            }

        }
        file.close();

        for (std::vector<std::string>::iterator it = allLines.begin() ; it != allLines.end(); ++it) {
            fileOut << *it << std::endl;
       }

        fileOut.close();
    } else {
        std::cout << "Record file not found. (1)" << std::endl;
    }
}





void recomputeDataInFile(std::string fileNameIn, std::string fileNameOut)
{
    std::ifstream file( fileNameIn );
    std::ofstream fileOut( fileNameOut );

    PostProcessing p;

    if(file) {

        std::string line;
        while(std::getline(file, line)) {
            //std::cout << "010" << std::endl;

            if (line.size() > 0) {
                std::string lineOut = p.GetCleanedStringFromDataLine(line);
                //std::cout << "011" << std::endl;
                if (lineOut.length() > 0) {
                    //std::cout << "011b" << std::endl;
                    fileOut << lineOut << std::endl;
                }
                //std::cout << "012" << std::endl;
            } else {
                std::cout << "line of lenght 0: ignored" << std::endl;
            }
        }

        file.close();
    } else {
        std::cout << "Record file not found. (2)" << std::endl;
    }
}


int main() {

    //std::cout << "000" << std::endl;
    //cutOffLinesAfterLastTargetCollected("GearVRRecord_2017Jun08_17-11-13","intermediateFile1");
    //std::cout << "001" << std::endl;
    removeNonKinematicLines("GearVRRecord_2017Jun08_17-07-22", "intermediateFile2");
    //std::cout << "002" << std::endl;
    recomputeDataInFile("intermediateFile2", "GearVRRecord_2017Jun08_17-07-22.out");
    //std::cout << "003" << std::endl;

    return 0;
}






