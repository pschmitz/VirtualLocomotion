#ifndef DATAREADER_H
#define DATAREADER_H

#include <glm/glm.hpp>
#include <string>

class DataReader
{
public:
    DataReader();

    glm::mat4 ReadMat4(std::string str);
    // glm::vec3 ReadVec3(std::string str);
    // float ReadFloat(std::string str);
};

#endif // DATAREADER_H
