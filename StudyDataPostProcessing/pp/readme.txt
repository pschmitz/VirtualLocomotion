Currently there are 3 scripts used for post processing:


=== === === === === === === === === === === === === === === === === === === === === === === === === === === ===
=== processFilesInDir.sh == === === === === === === === === === === === === === === === === === === === === ===

To automatically process all files in a directory, run the 'processFilesInDir.sh' script with the target directory as parameter.

The target directory must have the followng structure:
<target directory>/<subdirectories>/<record files>

The post processed files are stored next to the corresponding record files, with the suffix ".out" appended to the original file name.

--- The output has the following format: ---

timestamp
gain

position head real (X, -Z)
position head virtual (X, -Z)

total distance (meters)

current orientation angle X-axis head real (degree)
accumulated absolute angle X-axis head real (degree)

current orientation angle Y-axis head real (degree)
accumulated angle Y-axis head real (degree)
accumulated absolute angle Y-axis head real (degree)

current orientation angle Y-axis torso real (degree)
accumulated angle Y-axis torso real (degree)
accumulated absolute angle Y-axis torso real (degree)

current orientation angle Y-axis head virtual (degree)
accumulated angle Y-axis head virtual (degree)
accumulated absolute angle Y-axis head virtual (degree)

relative angle head-to-torso real Y-axis (degree)



=== === === === === === === === === === === === === === === === === === === === === === === === === === === ===
=== accumulatedValuesPerSubject.sh  === === === === === === === === === === === === === === === === === === ===


Use the 'accumulatedValuesPerSubject.sh' script with the target directory as parameter (again regard directory structure) to create a file containing all accumulated absolute values for each subject. That file is stored next to the corresponding record files, using the file name "<subject-ID>_accumulated".

--- The file content has the following format: ---

total distance (meters)
accumulated absolute angle X-axis head real (degree)
accumulated absolute angle Y-axis head real (degree)
accumulated absolute angle Y-axis torso real (degree)
accumulated absolute angle Y-axis head virtual (degree)



=== === === === === === === === === === === === === === === === === === === === === === === === === === === ===
===  copyOutputAndNotes.sh  === === === === === === === === === === === === === === === === === === === === ===


Use the 'copyOutputAndNotes.sh' script with the target directory as parameter (again regard directory structure) to copy all post processed files, files with accumulated values and notes files to extra directories.



