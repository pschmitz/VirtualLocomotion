#!/bin/bash

function processFile() {
	notesFileName="$(echo $1 | grep notes)"
	if [ "$1" == "$notesFileName" ];
	then
		echo "Copying note file $1."
		cp "$1" "$notesDirPath/$1"
		return
    fi
    accFileName="$(echo $1 | grep accumulated)"
	if [ "$1" == "$accFileName" ];
	then
		echo "Copying accumulated values file $1."
		cp "$1" "$accDirPath/$1"
		return
    fi
    outFileName="$(echo $1 | grep out)"
	if [ "$1" == "$outFileName" ];
	then
		echo "Copying output file $1."
		cp "$1" "$outputDirPath/$1"
		return
    fi
    echo "Skipping original record file $1."
}

function processDir() {
	if [ -z "$(ls -A $1)" ]; then
   		echo "Skipping empty directory $1."
	else
		cd $1
		echo "Entering directory $1."
		for i in *
		do
			processFile $i
		done
		echo "Exiting directory $1."
		cd ..
	fi
}

outputDirPath="$(pwd)/outputFiles"
notesDirPath="$(pwd)/notesFiles"
accDirPath="$(pwd)/accumulatedFiles"
mkdir $outputDirPath
mkdir $notesDirPath
mkdir $accDirPath
if [ -z "$1" ] || [ ! -d $1 ]
then
    echo No directory given or directory does not exist.
else
	cd $1
	for i in *
		do
        	if [ -d "$i" ]; then
			processDir $i
		fi
	done
fi


