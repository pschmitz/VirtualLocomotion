#!/bin/bash

accumulatedDistance=0
accumulatedAbsXHeadReal=0
accumulatedAbsYHeadReal=0
accumulatedAbsYTorsoReal=0
accumulatedAbsYHeadVirtual=0
targetFileName="thisFileShouldntExist"

function resetVariables() {
    accumulatedDistance=0
    accumulatedAbsXHeadReal=0
    accumulatedAbsYHeadReal=0
    accumulatedAbsYTorsoReal=0
    accumulatedAbsYHeadVirtual=0
}

function processFile() {
	notesFileName="$(echo $1 | grep notes)"
	if [ "$1" == "$notesFileName" ];
	then
		echo "Skipping note file $1."
		return
    fi
    accFileName="$(echo $1 | grep accumulated)"
	if [ "$1" == "$accFileName" ];
	then
		echo "Skipping accumulated values file $1."
		return
    fi
    
    outFileName="$(echo $1 | grep out)"
	if [ "$1" == "$outFileName" ];
	then
        echo "Processing output file $1."
        
        distanceRead="$(tail --lines=1 $1 | cut -d',' -f5)"
        #echo "$accumulatedDistance+$distanceRead"
        accumulatedDistance="$(echo "$distanceRead+$accumulatedDistance" | bc)"
        
        absXHeadRealRead="$(tail --lines=1 $1 | cut -d',' -f7)"
        accumulatedAbsXHeadReal="$(echo "$absXHeadRealRead+$accumulatedAbsXHeadReal" | bc)"
        
        absYHeadRealRead="$(tail --lines=1 $1 | cut -d',' -f10)"
        accumulatedAbsYHeadReal="$(echo "$absYHeadRealRead+$accumulatedAbsYHeadReal" | bc)"
        
        absYTorsoRealRead="$(tail --lines=1 $1 | cut -d',' -f13)"
        accumulatedAbsYTorsoReal="$(echo "$absYTorsoRealRead+$accumulatedAbsYTorsoReal" | bc)"
        
        absYHeadVirtualRead="$(tail --lines=1 $1 | cut -d',' -f16)"
        accumulatedAbsYHeadVirtual="$(echo "$absYHeadVirtualRead+$accumulatedAbsYHeadVirtual" | bc)"
        
		return
    fi
    if [ -f $1 ];
    then
		echo "Skipping original file $1."
    else
        echo "Skipping $1 (not a file)."
    fi
}

function processDir() {
	if [ -z "$(ls -A $1)" ]; then
   		echo "Skipping empty directory $1."
	else
		cd $1
		echo "Entering directory $1."
		targetFileName="$1_accumulated"
        if [ -f $targetFileName ];
        then
            echo "Accumulated values file $targetFileName already present. I remove it now and create a new one."
            rm -f "$targetFileName"
        fi
        touch "$targetFileName"
        
		resetVariables
		for i in *
		do
			processFile $i
		done
		
        echo -n "$accumulatedDistance" >> "$targetFileName"
        echo -n "," >> "$targetFileName"
        echo -n "$accumulatedAbsXHeadReal" >> "$targetFileName"
        echo -n "," >> "$targetFileName"
        echo -n "$accumulatedAbsYHeadReal" >> "$targetFileName"
        echo -n "," >> "$targetFileName"
        echo -n "$accumulatedAbsYTorsoReal" >> "$targetFileName"
        echo -n "," >> "$targetFileName"
        echo -n "$accumulatedAbsYHeadVirtual" >> "$targetFileName"
        
        #echo "$accumulatedDistance"
        #echo "$accumulatedAbsXHeadReal"
        #echo "$accumulatedAbsYHeadReal"
        #echo "$accumulatedAbsYTorsoReal"
        #echo "$accumulatedAbsYHeadVirtual"
        #resetVariables
        #echo "-----RESET-----"
        #echo "$accumulatedDistance"
        #echo "$accumulatedAbsXHeadReal"
        #echo "$accumulatedAbsYHeadReal"
        #echo "$accumulatedAbsYTorsoReal"
        #echo "$accumulatedAbsYHeadVirtual"
		echo "Exiting directory $1."
		cd ..
	fi
}

if [ -z "$1" ] || [ ! -d $1 ]
then
    echo No directory given or directory does not exist.
else
	cd $1
	for i in *
		do
        	if [ -d "$i" ]; then
			processDir $i
		fi
	done
fi
