#!/bin/bash

function processFile() {
	notesFileName="$(echo $1 | grep notes)"
	if [ "$1" == "$notesFileName" ];
	then
		echo "Skipping note file $1."
		return
    fi
    outFileName="$(echo $1 | grep out)"
	if [ "$1" == "$outFileName" ];
	then
		echo "Skipping output file $1."
		return
    fi
	accFileName="$(echo $1 | grep accumulated)"
	if [ "$1" == "$accFileName" ];
	then
		echo "Skipping accumulated values file $1."
		return
    fi
    
    if [ -f $1 ];
    then
        echo "Processing record file $1."
        mkdir pp_tmp_dir
        $ppAppPath "$1"
        rm -r -f pp_tmp_dir
    else
        echo "Skipping $1 (not a file)."
    fi
}

function processDir() {
	if [ -z "$(ls -A $1)" ]; then
   		echo "Skipping empty directory $1."
	else
		cd $1
		echo "Entering directory $1."
		for i in *
		do
			processFile $i
		done
		echo "Exiting directory $1."
		cd ..
	fi
}

ppAppPath="$(pwd)/RecordFilePP.exe"
if [ -z "$1" ] || [ ! -d $1 ]
then
    echo No directory given or directory does not exist.
else
	cd $1
	for i in *
		do
        	if [ -d "$i" ]; then
			processDir $i
		fi
	done
fi


